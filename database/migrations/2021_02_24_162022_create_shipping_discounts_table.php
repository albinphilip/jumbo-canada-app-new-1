<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->unique();
            $table->dateTime('expires_on');
            $table->string('applicable_to');
            $table->string('reduction_type');
            $table->integer('value');
            $table->string('status');
            $table->unsignedBigInteger('applicable_product')->nullable();
            $table->foreign('applicable_product')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->string('category')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_discounts');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_registration extends Model
{
    protected $guarded=[];
    public function store()
    {
        return $this->belongsTo(Store::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function province()
    {
        return $this->hasOne(Province::class,'name','provenance');
    }
}

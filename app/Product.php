<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function productImages()
    {
        return $this->hasMany(Product_image::class);
    }

    public function productVideos()
    {
        return $this->hasMany(Product_video::class);
    }

    public function productSpecifications()
    {
        return $this->hasMany(Product_specification::class);
    }

    public function productVariants()
    {
        return $this->hasMany(Product_variant::class);
    }

    public function getImage()
    {
        return $this->hasOne('App\Product_image')->OrderBy('sort_order');
    }

    public function getVideo()
    {
        return $this->hasOne('App\Product_video');
    }

    public function getVariant()
    {
        return $this->hasOne(Product_variant::class);
    }

    public function productSerialNumbers()
    {
        return $this->hasMany(Product_serialnumber::class);
    }

    public function wishlists()
    {
        return $this->hasMany(Wishlist::class);
    }

    public function order()
    {
        return $this->hasmany(Order::class);
    }

    public function reviews()
    {
        return $this->hasMany(Reviews::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function coupon_code()
    {
        return $this->hasMany(CouponCode::class,'id','applicable_product');
    }
    public function productMetadata()
    {
        return $this->hasOne(Product_metadata::class);
    }
    public function productShipment()
    {
        return $this->hasOne(Product_shipment::class);
    }
    public function shippingDiscounts()
    {
        return $this->hasMany(Shipping_discount::class);
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }


    public static function getProduct($category,$brand)
    {
        return Product::where([['category_id', $category],['brand_id',$brand],['status','!=','Hidden']])->get()->count();
    }
}

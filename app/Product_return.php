<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_return extends Model
{
    protected $guarded = [];

    public function product_registration()
    {
        return $this->belongsTo(Product_registration::class, 'reg_num', 'reg_no');
    }

}

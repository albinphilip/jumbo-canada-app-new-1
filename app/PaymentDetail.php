<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    protected $guarded = [];
    public function order()
    {
        return $this->hasOne(Order::class);
    }
    public function customer()
    {
        return $this->hasOne(Customer::class);
    }
}

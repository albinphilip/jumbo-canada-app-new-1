<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderFee_splitup extends Model
{
    protected $guarded = [];
    public function order()
    {
        return $this->belongsTo(Order::class,'order_id','order_id');
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Auth;


class CustomerAuthenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next ,$guard='customer')
    {
        if (! Auth::guard($guard)->check()) {
            return redirect(route('customerlogin'));
        }

        return $next($request);
    }

}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\Product_image;
use App\Product_registration;
use App\Product_serialnumber;
use App\Product_specification;
use App\Product_variant;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class ProductRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product_registration::all();
        $uniq = $products->unique('safety_code');
        $duplicate = $products->diff($uniq);
        $duplicate = $duplicate->unique('safety_code');
        //dd($duplicate);
        return view('admin.product-registration.index', ['products' => $products,'duplicates'=>$duplicate]);
    }

    public function edit($id)
    {
        $regisered_product = Product_registration::findOrFail($id);

        $products = Product::all();
        $stores = Store::all();
        return view("admin.product-registration.edit", ['products' => $products, 'regisered_product' => $regisered_product, 'stores' => $stores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reg = Product_registration::findOrFail($id);
        $data = $request->validate([
            'product_id' => 'required',
            'serial_number' => 'required',
            'safety_code' => 'required',
            'product_code' => 'nullable',
            'purchase_date' => 'nullable',
            'first_name' => 'nullable',
            'last_name' => 'nullable',
            'provenance' => 'nullable',
            'city' => 'nullable',
            'address' => 'nullable',
            'zipcode' => 'nullable',
            'phone' => 'required',
            'email' => 'required',
            'store_id' => 'required',
            'status' => 'required',
            'reason' => 'nullable',
        ]);
//        $find_product = Product_serialnumber::where([
//            'product_id' => $request->product_id,
//            'serial_number' => $request->serial_number,
//            'safety_code' => $request->safety_code,
//            'product_code' => $request->product_code])->first();
//
//        if (empty($find_product)) {
//            return back()->with('error', 'Given product not found.!')->withInput();
//        }
        $reg->update($data);
        //dd($data);
        //if update status is approve send the email to user
        if ($data['status'] == 'Approved') {
            $product = Product::find($request->product_id);
            $data['first_name'] = $data['first_name'];
            $data['product_name'] = $product->name;
            $data['reg_no']=$reg->reg_no;
            $email = $request->email;
            Mail::send('front.emails.product-reg-approved', ['data' => $data], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product Registration Approved');
            });
        } else if ($data['status'] == 'Rejected') {
            $product = Product::find($request->product_id);

            $msg = 'Your  request to register ' . $product->name . '
                 for warranty is rejected due to the following reason: ' . $request->reason . '. Please contact our support team for further clarification.';
            $email = $request->email;
            Mail::send('front.emails.discount-status-change', ['data' => $data['first_name'], 'msg' => $msg], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product Registration Rejected');
            });
        }
        return redirect(route('product-registration.index'))->with('success', 'Product registration updated successfully');


    }

    public function exportData()
    {
        $registrations = Product_registration::all();
        $data_array = array();
        $filename = 'WarrantyRegistration';
        $delimiter=",";
        $f=fopen($filename.'.csv',"w");//create a file pointer

        // Header row
        $fields = array("Serial Number", "Safety Code", "Product Code", "Date of Purchase", "Place of Purchase", "Customer", "Phone", 'Email', 'Province', 'Address', 'Status');
        fputcsv($f, $fields, $delimiter);
        foreach ($registrations as $registration) {
            $serial = $registration->serial_number;
            $safety = $registration->safety_code;
            $product = $registration->product_code;
            $date = $registration->purchase_date;
            $place = $registration->store->address . ',' . $registration->store->city;
            $name = $registration->first_name . ' ' . $registration->last_name;
            $phone = $registration->phone;
            $email = $registration->email;
            $province = $registration->provenance;
            $address = $registration->city . ',' . $registration->address . ',' . $registration->zipcode;
            $status = $registration->status;

            $data_array = array($serial, $safety, $product, $date, $place, $name, $phone, $email, $province, $address, $status);
            fputcsv($f, $data_array);

        }
        fclose($f);
        // download
        header("Content-Description: File Transfer");
        //header("Content-Disposition: attachment; filename=$filename.'csv'");
        header("Content-Type: application/download; ");
        $path = public_path($filename.'.csv');

        return response()->download($path, $filename.'.csv');
    }
}

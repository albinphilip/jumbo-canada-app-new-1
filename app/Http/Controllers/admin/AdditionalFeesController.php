<?php

namespace App\Http\Controllers\admin;

use App\Additional_fee;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class AdditionalFeesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fees = Additional_fee::all();
        //dd($orders);
        return view('admin.fees.index',['fees'=>$fees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.fees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fees = Additional_fee::where('province',$request->province)->first();
        if($fees != null)
            return back()->withInput()->with('error','Fee For the province already added');
        else{
            $data = $request->validate([
                'shipping_charge' => 'required',
                'province' => 'required',
                'tax' => 'required'
            ]);
            Additional_fee::create($data);
            return redirect(route('fees.index'))->with('success','Additional Fee Added successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fees = Additional_fee::findOrFail($id);
        return view('admin.fees.edit',['fee'=>$fees]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fees = Additional_fee::findOrFail($id);
        $data = $request->validate([
            'shipping_charge' => 'required',
            'province' => 'required',
            'tax' => 'required'
        ]);
        $fees->update($data);
        return redirect(route('fees.index'))->with('success','Additional Fee Details Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fees=Additional_fee::findOrFail($id);
        $fees->delete();
        return back()->with('success','Additional Fee successfully!');
    }
}

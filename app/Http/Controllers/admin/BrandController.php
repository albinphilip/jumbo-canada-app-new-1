<?php

namespace App\Http\Controllers\admin;

use App\Brand;
use App\Category;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cache;

class BrandController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands=Brand::all();
        //dd($brands);
        return view('admin.brand.index',['brands'=>$brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            'name' => 'required|max:255',
            'banner_image'=>'nullable|image|max:2048',
        ]);
        //dd($data);
        //Uploading and saving Cover
        if($request->banner_image) {
            $image_path = request('banner_image')->store('uploads/brands', 'public');
            $naked_path = env('IMAGE_PATH') . $image_path;
            $photo = Image::make($naked_path)->fit(1280,319);
            $photo->save();
            $data['banner_image']=$image_path;
        }
        else{
            $data['banner_image']='';
        }
        //Storing
        Cache::forget('brands');
        Brand::create($data);
        return redirect(route('brand.index'))->with('success','Brand added successfully!');;


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand=Brand::findOrFail($id);
        return view('admin.brand.edit',['brand'=>$brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand=Brand::findOrFail($id);
        $data=$request->validate([
            'name' => 'required|max:255',
            'banner_image'=>'nullable|image|max:500',
        ]);
        //Uploading and saving Cover
        if($request->banner_image) {
            $image_path = request('banner_image')->store('uploads/brands', 'public');
            $naked_path = env('IMAGE_PATH') . $image_path;
            $photo = Image::make($naked_path)->fit(1280,319);
            $photo->save();
            $data['banner_image']=$image_path;
            //removing old image
            $file_path=env('IMAGE_PATH').$brand->banner_image;
            if(file_exists($file_path))
            {
                @unlink($file_path);
            }
        }
        //updating
        $brand->update($data);
        Cache::forget('brands');
        return redirect(route('brand.index'))->with('success','Brand updated successfully!');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand=Brand::findOrFail($id);
        //deleting
        $brand->delete();
        Cache::forget('brands');
        return back()->with('success','Brand deleted successfully!');
    }
}

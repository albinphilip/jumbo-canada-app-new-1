<?php

namespace App\Http\Controllers\admin;

use App\CouponCode;
use App\Customer;
use App\Http\Controllers\AdminController;
use App\Order;
use Cassandra\Custom;
use Illuminate\Http\Request;

class CustomerController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=Customer::all();
        return view('admin.customer.index',['customers'=>$customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $order = Order::where('customer_id',$id)->first();
        //dd($order);
        if($order != null)
            return back()->with('error','Customer who have made an order cannot be deleted');
        else{
            $customer->delete();
            return back()->with('success','Customer Deleted Successfully');
        }
    }

    //export to csv
    public function export(Request $request)
    {
        $from = $request->from;
        $to = $request->to;

        if($from != null && $to != null)
            $customers = Customer::where([[('created_at'),'>=',$from],[('created_at'),'<=',$to.' 23:59:59']])->get();
        else if($from == null && $to != null)
            $customers = Customer::where('created_at','<=',$to.' 23:59:59')->get();
        else if($from != null && $to == null)
            $customers = Customer::where('created_at','>=',$from)->get();
        else
            $customers = Customer::all();

        $order_arr = array();
        $filename = 'customerdetail';
        $delimiter=",";
        $f=fopen('report/'.$filename.'.csv',"w");//create a file pointer
        // Header row
        $fields = array("Date","Name","Email Id","Phone Number","No.of products purchased",'Address');
        fputcsv($f,$fields,$delimiter);
        foreach ($customers->sortByDesc('id') as $customer)
        {


            $date = date_format($customer->created_at,'d-M-Y');
            $name = $customer->firstname.' '.$customer->lastname;
            $email = $customer->email;
            $phone = $customer->telephone;
            $product = $customer->orders()->count();
            $addr= '';
            foreach ($customer->addresses as $address)
            {
                $addr = $addr.$address->firstname.' '.$address->lastname .','.
                    $address->address_1.','.
                    $address->address_2.','.
                    $address->province.','.
                    $address->city .' - '.$address->postcode.','.
                    $address->company.PHP_EOL;
            }

            $order_arr = array($date,$name,$email,$phone,$product,$addr);
            // Write to file
            fputcsv($f,$order_arr);
        }

        fclose($f);

        // download
        header("Content-Description: File Transfer");
        //header("Content-Disposition: attachment; filename=$filename.'csv'");
        header("Content-Type: application/download; ");
        $headers = array(
            'Content-Type' => 'text/csv',
        );
        $path = 'report/'.$filename.'.csv';

        //return response()->download($path, $filename.'.csv')->deleteFileAfterSend(true);
        $path = asset($path);
        return response()->json(['success'=>'success','path'=>$path]);

    }
}

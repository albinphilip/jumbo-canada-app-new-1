<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store=Store::all();
        return view('admin.store.index',['stores'=>$store]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        $data=$request->validate([
            'city' => 'required|max:255',
            'address' =>'required',
            'phone' =>'required',
            'email' =>'required|email',
            'mapLocation' =>'required',
            'website_url'=>'nullable|url',

        ]);
        Store::create($data);
        return redirect(route('store.index'))->with('success','Store added successfully!');;

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store=Store::findOrFail($id);
        return view('admin.store.edit',['store'=>$store]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::findOrFail($id);
        $data=$request->validate([
            'city' => 'required|max:255',
            'address' =>'required',
            'phone' =>'required',
            'email' =>'required|email',
            'mapLocation' =>'required',
            'website_url'=>'nullable|url',
        ]);
        $store->update($data);
        return redirect(route('store.index'))->with('success','Store updated successfully!');;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store=Store::findOrFail($id);
        $store->delete();
        return back()->with('success','Store deleted successfully!');
    }
}

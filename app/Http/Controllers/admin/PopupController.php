<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\AdminController;
use App\Popup;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cache;

class PopupController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $popup = Popup::orderBy('id','desc')->first();
        //dd($popup);
        return view('admin.popup.create',['popup'=>$popup]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $data = $request->validate([
           'title' => 'nullable',
           'body'=>'nullable',
           'image'=>'required|image|max:2048',
           'expiry_date'=>'required',
            'url'=>'nullable',
        ]);
        $popup = Popup::orderBy('id','desc')->first();
        if($popup != null) {
            $file_path=env('IMAGE_PATH').$popup->cover_photo;
            if(file_exists($file_path))
            {
                @unlink($file_path);
            }
            //deleting
            $popup->delete();
        }
        //Uploading and saving Cover
            if($request->image) {
                $image_path = request('image')->store('uploads/popup', 'public');
                $naked_path = env('IMAGE_PATH') . $image_path;
                $photo = Image::make($naked_path);
                $photo->save();
                $data['image']=$image_path;
            }
            else{
                $data['image']='';
            }
            Popup::create($data);
            Cache::forget('popup');
            return redirect(route('popup.index'))->with('success','Popup added successfully!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

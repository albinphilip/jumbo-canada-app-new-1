<?php

namespace App\Http\Controllers\admin;

use App\Address;
use App\CanadaPost;
use App\Http\Controllers\AdminController;
use App\Order;
use App\OrderFee_splitup;
use App\Product_shipment;
use App\Product_variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Response;

class OrderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('order_id','!=',null)->with('customer','product')->get();
        //dd($orders);
        return view('admin.order.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->status;
        $order->save();

        //increase quantity on cancel & refund
        if ($request->status == 'Cancel & refunded') {
            if ($order->variant != null)
                $var = Product_variant::where([['product_id', $order->product_id], ['value', $order->variant]])->first();
            else
                $var = Product_variant::where('product_id', $order->product_id)->first();
            if ($var->quantity > 0) {
                $quantity = $var->quantity + $order->quantity;
                $var->update(array('quantity' => $quantity));
            }

            //reduce cost of the cancelled item from total amount if the re are multiple items in order
            $count = Order::where('order_id', $order->order_id)->count();
            if ($count > 1) {
                $sub_total = str_replace(',', '', $order->orderFeesplitup->sub_total);
                $new_subtotal = $sub_total - $order->product_price ;
                $tax = ($new_subtotal+$order->orderFeesplitup->shipping_charge-$order->orderFeesplitup->reduction)
                        *$order->orderFeesplitup->tax/100;

                $order_total = $new_subtotal + $order->orderFeesplitup->shipping_charge + $tax;
                $order_total = number_format($order_total, 2, '.', ',');
                $new_subtotal = number_format($new_subtotal, 2, '.', ',');
                //update order fee slit up
                $splitup = $order->orderFeesplitup();
                $splitup->update(['sub_total'=>$new_subtotal]);
                //update order table
                Order::where([['order_id', $order->order_id]])->update(['order_total' => $order_total]);
            }

        }

        //sending status update to user
        $msg = '';
        if ($order->status == 'Processing') {
            $msg = 'We are processing your order (' . $order->order_id . ') ' . $order->product->name .' and '.($order->delivery_date ?('estimated delivery date will be '. date('M-d-Y',strtotime($order->delivery_date)).',') : ''). 'will update you once we shipped the order.';
        }
        if ($order->status == 'Delivered') {
            $msg = 'This email is to acknowledge that your order (' . $order->order_id . ') ' . $order->product->name . ' has been delivered.';
        }
        if ($order->status == 'Cancel & refunded') {
            $msg = 'This email is to acknowledge that your order (' . $order->order_id . ') ' . $order->product->name . ' has been cancelled and refund process initiated.';
        }

        if ($msg != '') {
            $email = $order->customer->email;
            Mail::send('front.emails.order-status-change', ['order' => $order, 'msg' => $msg], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Order Status Update');
            });
        }
        //dd('here');

        return response()->json("success");

        // return redirect(route('order.index'))->with('success','Status updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function customer($id)
    {
        $orders = Order::where([['customer_id', $id],['order_id','!=',null]])->get();
        return view('admin.order.index', ['orders' => $orders]);
    }

    public function splitup($id)
    {
        $items = Order::where('order_id', $id)->get();
        //$fees=OrderFee_splitup::where('order_id',$id)->first();
        return view('admin.orderfee.index', ['items' => $items]);
    }

    public function shippingDetails(Request $request)
    {
        // dd('$request');
        //$data['shipping_date'] = today();
        $order = Order::findOrFail($request->id);
        //shipping label for canadapost
        if($request->partner == 'Canada Post')
        {
                $result = $this->CreateShipment($order->id,$request->date);
                //return ($result);
                if($result == 'error')
                    return ('error');
                else if($result == 'dimension')
                    return ('dimension');
        }
        else
        {
            $order->update(['shipping_partner' => $request->partner,
                'shippment_number' => $request->number,
                'shipping_date' => $request->date,

            ]);

        }
        $order = Order::findOrFail($request->id);
        //Sending shipping mail
        $email = $order->customer->email;
        Mail::send('front.emails.order-shipped', ['order' => $order], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Order Status Update');
        });
        //return redirect(route('order.index'))->with('success','Shipping Details Added Successfully');
        return ('success');
    }

    public function deliveryDate(Request $request)
    {
        // dd('$request');
        if($request->date < today())
            return ('date-error');
        else{
            $order = Order::findOrFail($request->id);
            $order->update(['delivery_date' => $request->date]);
            //Sending delivery date mail
            $email = $order->customer->email;
            Mail::send('front.emails.estimated-delivery', ['order' => $order], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Estimated Delivery Date');
            });
            //return redirect(route('order.index'))->with('success','Shipping Details Added Successfully');
            return ('success');
        }

    }
    public function abandoned_cart()
    {
        //$orders = Order::where('order_id',null)->get();
        $orders = Order::whereNotIn('customer_id', function($query){
            $query->select('customer_id')
            ->from('orders')
            ->where('order_id','!=',NULL);
        })->get();
       // dd($orders);
        return view('admin.order.abandoned_cart',['orders'=>$orders]);
    }
// shipping label
    public function CreateShipment($id,$date)
    {
        //dd($id);
        $order = Order::findOrFail($id);
        //dd($order->delivery_address);
        $shipping_address = Address::findOrFail($order->delivery_address);
        $dimension = Product_shipment::where('product_id',$order->product_id)->first();
        $province_array = array('NL'=>'Newfoundland and Labrador','PE'=>'Prince Edward Island',
                    'NS'=>'Nova Scotia','NB'=>'New Brunswick','QC'=>'Quebec','ON'=>'Ontario','MB'=>'Manitoba',
                    'SK'=>'Saskatchewan','AB'=>'Alberta','YT'=>'Yukon','NT'=>'Northwest Territories','NU'=>'Nunavut','BC'=>'British Columbia');
        //dd(array_search($shipping_address->province,$province_array));
        $postcode = $string = preg_replace('/\s+/', '', $shipping_address->postcode);
        $ship_array = array();
        $track_array = array();
        $price_array = array();
        $file_array = array();
        $manifest_array = array();
        if($dimension && $dimension->shipment_weight && $dimension->height && $dimension->length && $dimension->breadth)
        {
            for($i = 0;$i < $order->quantity; $i++){
                $datas = [
                    'postalCode' => $postcode,
                    'weight' => round($dimension->shipment_weight),
                    'length' => round($dimension->length),
                    'width' => round($dimension->breadth),
                    'height' => round($dimension->height),

                    'mailingDate' => date('Y-m-d',strtotime($date)),

                    'destination-name' => $shipping_address->firstname.' '.$shipping_address->lastname,
                    'destination-company' => $shipping_address->company,
                    'destination-address-line-1' => $shipping_address->address_1,
                    'destination-city' => $shipping_address->city,
                    'destination-prov-state' => array_search($shipping_address->province,$province_array),
                    'destination-country-code' => 'CA',
                    'destination-postal-zip-code' => $shipping_address->postcode,

                    'notification-email' => $order->customer->email,
                    'item-amount' => $order->product_price,
                    'reference-id' => $order->order_id,


                ];

                $canadapost = CanadaPost::CreateShipment($datas);
                //return ($canadapost);
                if($canadapost['shipment_id'] && $canadapost['tracking_pin'] && $canadapost['price'] && $canadapost['file_path']){

                    //return Response::download($canadapost['file_path']);
                    array_push($ship_array,$canadapost['shipment_id']);
                    array_push($track_array,$canadapost['tracking_pin']);
                    array_push($price_array,$canadapost['price']);
                    array_push($file_array,$canadapost['file_path']);
                    array_push($manifest_array,$canadapost['manifest_file_path']);
                }
                else{
                    //return back()->with('error','Failed to generate shipping label!');
                    return ('error');
                }
            }
            //store data to db
            $data = array('shipment_id'=>implode(',',$ship_array),
                'tracking_pin'=>implode(',',$track_array),
                'shipment_price'=>implode(',',$price_array),
                'shipping_label'=>implode(',',$file_array),
                'shipping_partner' => 'Canada Post',
                'shippment_number' => '',
                'shipping_date' => $date,
                'manifest_file'=>implode(',',$manifest_array),);
            $order->update($data);
            return ('success');
        }
        else{
            //return back()->with('error','Please Add product dimensions!');
            return ('dimension');
        }


    }
    // BULK DELETE
    public function bulk_delete($id){
        //dd($ids);
        Order::whereIn('customer_id',explode(',',$id))->delete();
        return back()->with('success','Entries deleted successfully!');
    }

}

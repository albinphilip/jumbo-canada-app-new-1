<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\AdminController;
use App\Product;
use App\Product_image;
use App\Product_specification;
use App\Product_variant;
use App\Product_video;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProductSpecificationController extends AdminController
{
    public function delete_spec(Request $request)
    {
        $spec=Product_specification::findOrFail($request->id);
        if($spec->delete())
            return ('success');
        else
            return ('error');
    }
    public function delete_image(Request $request)
    {
        $spec=Product_image::findOrFail($request->id);
        $file_path=env('IMAGE_PATH').$spec->image_path;
        if(file_exists($file_path))
        {
            @unlink($file_path);
        }
        $file_path=env('IMAGE_PATH').$spec->tumbnail_path;
        if(file_exists($file_path))
        {
            @unlink($file_path);
        }

        if($spec->delete())
            return ('success');
        else
            return ('error');


    }
    public function delete_video(Request $request)
    {
        $spec=Product_video::findOrFail($request->id);
        if($spec->delete())
            return ('success');
        else
            return ('error');
    }
    public function delete_variant(Request $request)
    {
        $spec=Product_variant::findOrFail($request->id);
        if($spec->delete())
        {
            //check variant is empty
            $variant  = Product_variant::where('product_id',$spec->product_id)->get();
            if(count($variant) == 0)
                Product::where('id',$spec->product_id)->update(['status'=>'Hidden']);
            return ('success');
        }
        else
            return ('error');


    }
}

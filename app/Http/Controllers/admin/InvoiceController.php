<?php

namespace App\Http\Controllers\admin;

use App\CanadaPost;
use App\Http\Controllers\AdminController;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class InvoiceController extends AdminController
{
    public function invoice($orderId)
    {
        $order = Order::where('order_id',$orderId)->get();
        return view('admin.order.invoice',['orders'=>$order]);
    }
    public function GetTrackingDetails($pin)
    {
        //$pin = '1371134583769923';

        $canadapost = CanadaPost::GetTrackingDetails($pin);
        return (['msg'=>'success','datas'=>$canadapost]);

    }
}

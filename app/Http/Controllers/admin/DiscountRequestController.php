<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\CouponCode;
use App\Discount;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DiscountRequestController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts = Discount::all();
        $categories = Category::all();
        return view('admin.discount.index',['discounts'=>$discounts,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request)
    {
        $discount = Discount::findOrFail($request->id);
        $discount->status = 'Rejected';
        $discount->reason = $request->reason;
        $discount->save();


        //sending status update to user
            $msg = 'This email is inform you that your application for discount  has been rejected and the reason for rejection is '.$discount->reason.'.';

            $email = $discount->email;
            Mail::send('front.emails.discount-status-change', ['data' => $discount->name, 'msg' => $msg], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Discount Request Rejected');
            });
        //dd('here');

        return response()->json("success");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        $discount = Discount::findOrFail($request->id);
        $number = mt_rand('111', 99999);
        $coupon_code = 'DISCOUNT' . $number;
        $data = array(
            'code' => $coupon_code,
            'expires_on' => $request->expiry,
            'applicable_to' => 'All',
            'reduction_type' => 'Percent',
            'value' => $request->discount,
            'total_redemptions' => 1,
            'redemptions_per_customer' => 1,
            'status' => 'Published',
            'category' => $request->category,
        );
        if (CouponCode::create($data)) {
            $discount->update(['status' => 'Approved']);

            //sending status update to user
            $msg = '';
            if ($discount->status == 'Approved') {
                $msg = 'Your Application for discount has been approved.'."\r\n".
                'Your Discount coupon details are,'."\r\n".
                'Coupon Code : '.$data['code'].','."\r\n".
                'Discount : '.$data['value'].'%,'."\r\n".
                'Expiry date :'.$data['expires_on'].','."\r\n".
                'Applicable to : '.$data['category'];
            }
                $email = $discount->email;
                Mail::send('front.emails.discount-status-change', ['data' => $discount->name, 'msg' => $msg], function ($message) use ($email) {
                    $message->to($email)
                        ->subject('Jumbo Canada | Discount request Approval');
                });
            //dd('here');

            return response()->json("success");

        }
    }

    public function edit($id)
    {
        $discount = Discount::findOrFail($id);
        return view('admin.discount.edit',['discount'=>$discount]);
    }

    public function update(Request $request,$id)
    {
        $discount = Discount::findOrFail($id);
        $data = $request->validate([
            'proof' => 'required',
        ]);
        $data['status'] = '';
        if ($request->proof) {
            $file_path = request('proof')->store('uploads/discount', 'public');
            $data['proof'] = $file_path;
            $path = $discount->proof;
            if(file_exists($path))
            {
                @unlink($path);
            }
        } else {
            $data['proof'] = '';
        }
        $discount->update($data);
        return redirect(route('discount.index'))->with('success','Proof Uploaded Successfully!');
    }
}

<?php

namespace App\Http\Controllers\front;

use App\Customer;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use Illuminate\Support\Facades\Mail;
class RegisterController extends Controller
{

    //use RegistersUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
        $this->middleware('guest:customer')->except('logout');
    }

    public function index()
    {
        if (Auth::guard('customer')->check()) {
            return redirect(route('edit-profile'));
        } else {
            return view('front.login.register');
        }
    }

    public function user_registration(Request $request)
    {
        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
        if ($resultJson->success != true) {
            return back()->with('error', 'Recaptcha error')->withInput();
        }
        if ($resultJson->score >= 0.3) {

            $data = $request->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:customers,email',
                'telephone' => 'required',
                'password' => 'required|same:confirm_password|regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d _\W]{8,16}$/i',
                'newsletter' => 'nullable'
            ]);

            $data['password'] = Hash::make($data['password']);
            $customer = Customer::create($data);
            Auth::guard('customer')->login($customer);
            //subscribing to newsletter
            if ($request->newsletter == 'Yes') {
                $sub = Subscriber::where('email',$request->email)->first();
                if(empty($sub)){
                    $subscriber = new Subscriber;
                    $subscriber->email = $request->email;
                    $subscriber->status = "subscribed";
                    $subscriber->save();
                }
            }
            //registration confirmation email
            $email = $request->email;
            Mail::send('front.emails.signup', ['name' => $request->firstname], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Signup Confirmation');
            });
            //dd('here');
            //return view('front.profile.profile');
            return redirect()->intended();

        }
    }

}

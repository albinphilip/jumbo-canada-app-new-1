<?php

namespace App\Http\Controllers\front;

use App\Customer;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;
use Mail;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;

class ForgotPasswordController extends Controller
{
    public function showLinkRequestForm()
    {
        return view('front.forgotpassword.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
        if ($resultJson->success != true) {
            return back()->with('error', 'Recaptcha error')->withInput();
        }
        if ($resultJson->score >= 0.3) {
            $customer = Customer::where('email', $request->email)->first();

            if (!$customer)
                return back()->with('message', 'Invalid Mail id')->withInput();
            else {

                $en_email = base64_encode($customer->email);
                $url = url('/');
                $reset_link = $url . "/password/reset/request/" . $en_email;
                //dd($reset_link);
                $data = array('email' => $customer->email, 'name' => $customer->firstname, 'reset_link' => $reset_link);

                $email = $customer->email;

                Mail::send('front.emails.password-reset', $data,
                    function ($message) use ($email) {
                        $message->to($email, 'Jumbocanada')
                            ->subject('Jumbo Canada | Reset password');
                    });

                if (count(Mail::failures()) > 0) {
                    return back()->with('message', 'Error')->withInput();
                } else
                    return back()->with('message', 'Password Reset Link Sent to your mail id')->withInput();
            }
        }
    }

    public function showResetForm($email)
    {
        $de_email = base64_decode($email);
        //dd($de_email);
        return view('front.forgotpassword.resetpassword', ['email' => $de_email]);
    }

    public function reset(Request $request)
    {
        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
        if ($resultJson->success != true) {
            return back()->with('error', 'Recaptcha error')->withInput();
        }
        if ($resultJson->score >= 0.3) {
            //dd($request->email);
            $customer = Customer::where('email', $request->email)->firstOrFail();
            $data = $request->validate([
                'password' => 'required|same:confirm_password|regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d _\W]{8,16}$/i',
            ]);
            $customer->password = Hash::make($data['password']);
            $customer->save();
            return back()->with('message', 'Password updated successfully!');

        }
    }
}

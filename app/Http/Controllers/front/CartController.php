<?php

namespace App\Http\Controllers\front;
use App\CouponCode;
use App\CouponRedemption;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Cart;
use Auth;
use Session;


class CartController extends Controller
{
    //show cart items
    public function cart()  {

        //forget session variables
        session()->forget('final_amount');
        session()->forget('reduction');
        session()->forget('coupon_id');
        session()->forget('shipping');
        session()->forget('tax');
        session()->forget('taxed');


        $cartCollection = \Cart::Content();
        //dd($cartCollection);
        return view('front.cart')->with(['carts' => $cartCollection]);
    }
//add product to cart
    public function add(Request$request){
        //add to cart
        \Cart::add(array(
            'id' => $request->product_id,
            'name' => $request->name,
            'qty' => $request->quantity,
            'price' => $request->price,
            'options' => array('image' => $request->image,
                                'variant' => $request->variant,
                                'rating'=>$request->rating,
                                'slug'=>$request->slug,
                                'brand'=>$request->brand,
    ),
                                ));
        $cart = \Cart::count();
        return response(['success'=>'success','cart'=>$cart]);
    }

    //decrement quantity of an item from cart
    public function decreaseQuantity($id)
    {
        //dd($id);
        $item = \Cart::get($id);
        Cart::update($id,$item->qty-1);
        $cart = \Cart::count();
        $total = \Cart::total();
        return response(['success'=>'success','count'=>$cart,'item'=>$item,'total'=>$total]);
    }

    //increment qty of an item from cart
    public function increaseQuantity($id)
    {
        //dd($id);
        $item = \Cart::get($id);
        Cart::update($id,$item->qty+1);
        $cart = \Cart::count();
        $total = \Cart::total();
        return response(['success'=>'success','count'=>$cart,'item'=>$item,'total'=>$total]);
        //return back()->with('success','Quantity Increased');
    }

     //delete item from cart
    public function remove($id)
    {
        Cart::remove($id);
        $cart = \Cart::count();
        return response(['success'=>'success','count'=>$cart,'total'=>\Cart::total()]);
        //return back()->with('success','Item Deleted From Cart');
    }
}

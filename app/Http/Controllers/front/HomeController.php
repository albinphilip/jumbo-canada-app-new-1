<?php

namespace App\Http\Controllers\front;
use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Popup;
use App\Product;
use App\Product_banner;
use App\Banner;
use App\Product_image;
use App\Product_specification;
use App\Product_variant;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
   public function index(){

    $banners = Cache::rememberForever('banners', function () {
        return Product_banner::all();
        });
        
        $products = Product::where('status','!=','Hidden')->with('category','brand','getImage','getVariant','reviews')->get();

        $popup = Cache::rememberForever('popup', function () {
        return Popup::first();
        });
       return view('front.home',['banners'=>$banners,'popup'=>$popup,'products'=>$products]);
   }

   //list products based on category
   public function product_list($product){
      // dd($product);
       $des = Category::where('name','LIKE','%'.$product.'%')->first();
       $banner = Banner::first();
       return view('front.product.product-list',['tab'=>$product,'cat'=>$des,'brand'=>'','banner'=>$banner]);
   }

   //list products based on brand
    public function product_brand_list($brand,$product){
        // dd($product);
        $brands = Brand::where('name','LIKE','%'.$brand.'%')->first();

        if($product != 'all')
             $des = Category::where('name','LIKE','%'.$product.'%')->first();
        else
            $des = 'all';
        return view('front.brand-list',['tab'=>$product,'cat'=>$des,'brand'=>$brands]);
    }

   public function product_detail($brand,$slug)
   {
       $product = Product::where('slug',$slug)->first();
       if(empty($product)) abort(404);
       //$variant= Product_variant::where('product_id',$product->id)->first();
       //dd($variant);
        $spec = Product_specification::where([['product_id',$product->id],['specification','=','Voltage, Wattage']])->first();
        $similiar = Product::where([['status','!=','Hidden'],['category_id',$product->category_id]])->with('getImage','brand','getVariant')->inRandomOrder()->limit(5)->get();
       return view('front.product.product-detail',['product'=>$product,'voltage'=>$spec,'similiars'=>$similiar]);
   }

    public function product_price($id){
       $variant=Product_variant::where('id',$id)->first();
       $product=Product::where('id',$variant->product_id)->first();
       if($product->offer != null && $product->offer_expire >= today())
       {
           $price = $variant->price - (($variant->price*$product->offer)/100);
       }
       else{
           $price = $variant->price;
       }
       if($variant->quantity > 0)
           $availability='In Stock';
       else
           $availability='Out of Stock';
        return response()->json(['price' => $price, 'availability' => $availability]);
    }


    public function test(){
        $sid = 'AC3d37f8d3e4667f97aad6eaeb14c0242e';
        $token = '8f718ef804d83e2c8746bd5f3b67afbf';
        $client=new Client($sid, $token);
        // Use the client to do fun stuff like send text messages!
        $client->messages->create(
        // the number you'd like to send the message to
            '+16476144136',
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+12512775203',
                // the body of the text message you'd like to send
                'body' => "Hi, Team Its Jithin. If you got this SMS please ping in Whatsapp"
            ]
        );

    }

}

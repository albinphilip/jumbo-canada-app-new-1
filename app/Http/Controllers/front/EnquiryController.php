<?php

namespace App\Http\Controllers\front;
use App\Enquiry;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Mail\EnquiryEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EnquiryController extends Controller
{
    public function store(Request $request){
        $resultJson=Gcaptcha::verifyCaptcha( $request->get('recaptcha-response'));
        //dd($resultJson);
        if ($resultJson->success != true) {
            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }
        if ($resultJson->score >= 0.3) {
           //dd($request);
            $data = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|email',
                'phone'=>'required',
                'message' => 'required'
            ]);
            if (Enquiry::create($data)) {
               // dd($data);
               // Mail::to(env('MAIL_TO'))->send(new EnquiryEmail($data));
                $email = env('MAIL_TO');
                Mail::send('front.emails.enquiry-mail', ['enquiry' => $data], function ($message) use ($email) {
                    $message->to($email)
                        ->subject('Jumbo Canada | New Enquiry');
                });
                return ('OK');
            }
        }
        else {
            return ('error');
        }
    }
}

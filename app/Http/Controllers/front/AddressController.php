<?php

namespace App\Http\Controllers\front;

use App\Address;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Order;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;

class AddressController extends Controller
{

    public function address()
    {
        $zones = Zone::where('country_id',38)->get();
        //dd($zones);
        return view('front.profile.address',['zones'=>$zones]);
    }
    public function storeaddress(Request $request)
    {
        //dd($request);
        $customer_id = Auth::guard('customer')->user()->id;
        //dd($customer_id);
        $data = $request->validate([
            'company' => 'nullable',
            'firstname' => 'required',
            'lastname' => 'required',
            'province' =>'required',
            'address_1'=>'required',
            'address_2'=>'nullable',
            'city'=>'required',
            'postcode'=>'required',
        ]);
        $data['zone_id']=1;
        $data['country_id']=1;
        $data['customer_id'] = $customer_id;
        $address = Address::create($data);
        if($address) {
            //session()->forget('address');
            //session()->put('address',$address->id);
            return back()->with('message', 'Address Added Successfully');
        }
        else{
            return back()->with('message','Some error occurred');
        }
    }

    public function editaddress($id)
    {
        $address = Address::findOrFail($id);
        $zones = Zone::where('country_id',38)->get();
        return view('front.profile.edit-address',['address'=>$address,'zones'=>$zones]);
    }
    public function updateaddress(Request $request,$id)
    {
        $address = Address::findOrFail($id);
        $customer_id = Auth::guard('customer')->user()->id;
        $data = $request->validate([
            'company' => 'nullable',
            'firstname' => 'required',
            'lastname' => 'required',
            'address_1'=>'required',
            'address_2'=>'nullable',
            'city'=>'required',
            'postcode'=>'required',
            'custom_field' =>'nullable',
            'province'=>'required',
        ]);
        if(!$request->custom_field)
            $data['custom_field'] = null;
        $data['customer_id'] = $customer_id;
        $data['zone_id']=1;
        $data['country_id']=1;
        $address->update($data);
        return redirect(route('address'))->with('message','Address Updated Successfully');;
    }

    public function deleteaddress($id)
    {
        $address = Address::findOrFail($id);

        //check address is associated with order table
        $order = Order::where('delivery_address',$id)->orWhere('billing_address',$id)->first();
        if($order)
        {
            $address->update(array('type'=>'hidden'));
        }
        else
            $address->delete();
        return back()->with('message','Address Deleted Successfully');
    }

}

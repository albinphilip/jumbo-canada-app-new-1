<?php

namespace App\Http\Controllers\front;

use App\Customer;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Product_variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Product;
use Session;
use Auth;
use Redirect;
use Cart;
use Str;

class LoginController extends Controller
{
    //use AuthenticatesUsers;

    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
        $this->middleware('guest:customer')->except('logout');

    }

    public function index()
    {
        //dd(url()->previous());
        if (Str::contains(url()->previous(), 'password/reset/request'))
            {
                Session::put('url', '/');
            }
        else
        {
            Session::put('url', url()->previous());

        }
            //dd(session()->get('url'));
        return view('front.login.login');
    }

    public function postLogin(Request $request)
    {
        //dd($request);
        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
        if ($resultJson->success != true) {
            return back()->with('message', 'Recaptcha error')->withInput();
        }
        if ($resultJson->score >= 0.3) {
            $request->validate([
                'email' => 'required',
                'password' => 'required',
            ]);
            $credentials = $request->only('email', 'password');

            if (Auth::guard('customer')->attempt($credentials)) {
                // Authentication passed...
                $request->session()->put('auth_user_session_object', Auth::guard('customer')->user());
                $this->addToCart();
                $url = redirect()->intended()->getTargetUrl();
                //dd($url."     ".$request->root().'/');
               // $url = session()->get('url');
                if ($url == $request->root().'/')
                    //dd('here');
                    return redirect(route('edit-profile'));
                else{
                        //dd('there');
                        return redirect()->intended($url);
                    }
                    

            }
            return back()->with('message', 'You have entered invalid credentials')->withInput();
        }
    }


    public function logout(Request $request)
    {


        $sessionId = session()->getId();
        $customer_id = Auth::guard('customer')->user()->id;

        //remove all items in cart of the current user
        $carts = \App\Cart::where('customer_id', $customer_id)->delete();
        // storing cart items to database
        $items = Cart::content();
        foreach ($items as $item) {
            // dd($item);
            //dd($item->options);
            $data = array(
                'product_id' => $item->id,
                'customer_id' => $customer_id,
                'session_id' => $sessionId,
                'quantity' => $item->qty,
                'image' => $item->options['image'],
                'variant' => $item->options['variant'],
               
            );
            \App\Cart::create($data);
        }

        Auth::guard('customer')->logout();
        $request->session()->forget($sessionId);
        Session::forget('cart');
        Session::forget('total');
        Session::forget('final_amount');
        Session::forget('coupon_id');
        Session::forget('shippingAddress');
        Session::forget('billingAddress');
        Session::forget('reduction');
        Session::forget('province');
        Session::forget('customer');
        Session::forget('instruction');
        return redirect(route('home'));
    }

    public function addToCart()
    {
        //count of items in cart
        $items = \App\Cart::where('customer_id', Auth::guard('customer')->user()->id)->get();
        $cartCollections = \Cart::Content();

        foreach ($items as $item) {
            $product = Product::where('id', $item->product_id)->first();

            //check product already in cart
            $cart = $cartCollections->where('id', $product->id)->first();
            if ($cart) {
                \Cart::remove($cart->rowId);
            }

            //calculate price
            if ($item->variant == null) {
                $variant = Product_variant::where('product_id', $item->product_id)->first();

            } else {
                $variant = Product_variant::where([['product_id', $item->product_id], ['value', $item->variant]])->first();

            }
            if ($product->offer != null && $product->offer_expire >= today())
                $price = $variant->price - ($variant->price * $product->offer / 100);
            else
                $price = $variant->price;
            //add to cart
            //rating
            $rating = 0;
            if ($product->reviews->count() > 0) {
                $rating = 0;
                foreach ($product->reviews as $reviews)
                    $rating = $rating + $reviews->rating / Count($product->reviews);
            }

            \Cart::add(array(
                'id' => $item->product_id,
                'name' => $product->name,
                'qty' => $item->quantity,
                'price' => $price,
                'options' => array('image' => $item->image, 'variant' => $item->variant, 'rating' => $rating, 'slug' => $product->slug,'brand'=>$product->brand->name),
            ));
        }
    }
}


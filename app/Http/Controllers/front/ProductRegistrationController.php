<?php

namespace App\Http\Controllers\front;

use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Product;
use App\Product_registration;
use App\Product_return;
use App\Product_serialnumber;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class ProductRegistrationController extends Controller
{
    public function create(Request $request)
    {
        $city='';

        $products = Product::all();
        $stores = Store::all();
        return view("front.product.product-registration", ['products' => $products, 'stores' => $stores]);
    }

    public function store(Request $request)
    {
        //dd($request);
//        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
//        if ($resultJson->success != true) {
//            return back()->with('error', 'Recaptcha error')->withInput();
//        }
        // if ($resultJson->score >= 0.3) {
        $data = $request->validate([
            'product_id' => 'required',
            'serial_number' => 'required',
            'safety_code' => 'required',
            'product_code' => 'required',
            'purchase_date' => 'required',
            'first_name' => 'required',
            'last_name' => 'nullable',
            'provenance' => 'nullable',
            'city' => 'nullable',
            'address' => 'nullable',
            'zipcode' => 'nullable',
            'phone' => 'required',
            'email' => 'required',
            'store_id' => 'required',
        ]);
        $data['ip_address'] = $request->ip();
        $data['status'] = 'New';
        /* $find_product = Product_serialnumber::where([
             'product_id' => $request->product_id,
             'serial_number' => $request->serial_number,
             'safety_code' => $request->safety_code,
             'product_code' => $request->product_code])->first();

         if (empty($find_product)) {

             return back()->with('error', 'Given product not found.!')->withInput();
         }*/
        //check already registered for warranty
        $find_registration = Product_registration::where([
            'product_id' => $request->product_id,
            'serial_number' => $request->serial_number,
            'safety_code' => $request->safety_code,
            'product_code' => $request->product_code])->first();
        if (!empty($find_registration)) {
            return back()->with('error', 'The Product is already registered')->withInput();
        } else {
            //Uploading and saving attached_file

            if ($request->attached_file) {
                $image_path = request('attached_file')->store('uploads/product-register', 'public');
                $data['attached_file'] = $image_path;
            } else {
                $data['attached_file'] = '';
            }
            //generating new registration id
            $last_reg_id = Product_registration::orderBy('reg_no', 'desc')->first();
            if ($last_reg_id == '') $data['reg_no'] = 10000;
            else $data['reg_no'] = $last_reg_id->reg_no + 1;
            $registration = Product_registration::create($data);
            //sending email confirmation to customer
            $email = $request->email;
            Mail::send('front.emails.product-reg-ack', ['data' => $registration], function ($message) use ($email, $image_path) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product Registration')
                    ->attach($image_path);
            });
            //Email to client
            $email = env('MAIL_TO');
            Mail::send('front.emails.client-product-registration', ['data' => $registration], function ($message) use ($email, $image_path) {
                $message->to($email)
                    ->subject('Jumbo Canada | New Product Registration')
                    ->attach($image_path)
                    ->addPart('Hello, welcome to Laravel!', 'text/plain');
            });
            return redirect(route('warranty-registration'))->with('success', 'Product registration completed successfully');
        }

    }

    // }

    public function reregister(Request $request)
    {
        $return = Product_return::where('return_id', $request->rma)->first();
        if ($return == null || $return->product_registration->email != $request->email)
            return Redirect::back()->with('error', 'RMA number or email does not match');
        //store previous product detail
        $new_reg = $return->product_registration->replicate();
        $new_reg->serial_number = $request->serial_number;
        $new_reg->safety_code = $request->safety_code;
        $new_reg->status = 'New';
        $new_reg->save();
        $return->product_registration->status = 'Replaced';
        $return->product_registration->save();
        //sending email confirmation to customer
        $email = $request->email;
        Mail::send('front.emails.product-reg-ack', ['data' => $new_reg], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product Registration');
        });
        //Email to client
        $email = env('MAIL_TO');
        Mail::send('front.emails.client-product-registration', ['data' => $new_reg], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product Re-Registration');
        });
        return Redirect::back()->with('success', 'Product registration completed successfully');
    }
}

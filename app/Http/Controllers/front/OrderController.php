<?php

namespace App\Http\Controllers\front;

use App\Additional_fee;
use App\Address;
use App\CanadaPost;
use App\CouponCode;
use App\CouponRedemption;
use App\Customer;
use App\Enquiry;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Mail\EnquiryEmail;
use App\Order;
use App\OrderFee_splitup;
use App\Product;
use App\Product_return;
use App\Product_variant;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;
use Cart;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Session;
use Stripe\Coupon;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use App;
use Illuminate\Support\Facades\Storage;
use App\Free_shipping;

class OrderController extends Controller
{

    public function payment()
    {
        $customer_id = Session::get('customer');
        $customer = Customer::findOrFail($customer_id);
        //dd($customer);
        $cartCollection = \Cart::Content();
        $fees = Additional_fee::where('province', Session::get('province'))->first();
        $total = str_replace(',', '', Session::get('total'));
        //dd(Session::get('total'));
        // shipping cost
        $shipping_charge = 0;
        $tax_shipping = 0;
        $cus_status =  Order::where('customer_id', $customer_id)->get();
        if ($cus_status) $status = 'Existing Customer';
        else $status = 'New Customer';
        session()->put('shipping_code','');
        if($total <= 250){
            foreach ($cartCollection as $product){

                $canada_shipping = 0;
                $shipping_cost = 0;
                //shipping discount
                $item = Product::findOrFail($product->id);
                $discount = App\Shipping_discount::where([['category','All'],['expires_on','>=',today()],['status','!=','Hidden']])
                            ->orwhere([['category',$item->category->name],['expires_on','>=',today()],['status','!=','Hidden']])
                            ->orWhere([['applicable_product',$product->id],['expires_on','>=',today()],['status','!=','Hidden']])
                            ->first();
                //dd($discount);
                if($discount != '' && ($discount->applicable_to == $status || $discount->applicable_to == 'All')){
                    //shipping discount exist
                    $shipping_cost = $discount->value*$product->qty;
                    session()->put('shipping_code',$discount->code);
                }
                else{
                    // shipping cost through canada post
                    $shipping = App\Product_shipment::where('product_id',$product->id)->first();
                    //product price
                    $variant = null;
                    $variant = $product->options['variant'];
                    if ($variant != null) {
                        $price = Product_variant::where([['product_id', $product->id], ['value', $variant]])
                            ->pluck('price')->first();
                    }
                    else
                    {
                        $price = Product_variant::where('product_id', $product->id)
                            ->pluck('price')->first();
                    }

                    if($shipping != null && $shipping->shipment_weight != null && $shipping->length != null && $shipping->breadth != null && $shipping->height != null)
                    {
                        $address = Address::findOrFail(session('shippingAddress'));
                        $postcode = $string = preg_replace('/\s+/', '', $address->postcode);

                        $datas = [
                            'postalCode' => $postcode,
                            'weight' => $shipping->shipment_weight,
                            'length' => round($shipping->length),
                            'width' => round($shipping->breadth),
                            'height' => round($shipping->height),
                            'item-amount' => $price,
                        ];

                        $canada_post = App\CanadaPost::GetRates($datas);
                        // dd($canada_post);
                        $canada_shipping = $canada_post * $product->qty;
                        if($canada_shipping == 0){
                            $shipping_cost = $fees->shipping_charge * $product->qty ;
                        }

                        // dd($canadapost{0});
                    }
                    else{

                        $shipping_cost = $fees->shipping_charge * $product->qty;
                    }
                }

                $shipping_charge = $shipping_charge + $shipping_cost+$canada_shipping;
                $tax_shipping = ($tax_shipping + $shipping_cost);

            }
            //dd($shipping_charge);

        }

        session()->put('shipping_charge',$shipping_charge);
        $tax = ($total + $tax_shipping) * $fees->tax / 100;
        $amount = $total + $shipping_charge + $tax;
        $amount = number_format($amount, 2);
        session()->put('tax_value',$tax);        
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        //dd($amount);
        $s_customer = \Stripe\Customer::create([
            'email' => $customer->email,
            'name' =>  $customer->firstname . ' ' . $customer->lastname,
            'description' => 'Ultralinks-Customer',
        ]);
        $payment_intent = PaymentIntent::create([
            'description' => $customer->firstname . ' ' . $customer->lastname . ', ' . $customer->email . ', ' . $customer->telephone,
            'amount' => $amount * 100,
            'customer' => $s_customer['id'],
            'currency' => 'CAD',
            'payment_method_types' => ['card'],
        ]);
        $amount = number_format($amount, 2, '.', ',');
        session()->put('final_amount', $amount);
        $intent = $payment_intent->client_secret;
        //save order details to order table
        foreach ($cartCollection as $item){
            $product = Product::findOrFail($item->id);
            $variant = null;
            $variant = $item->options['variant'];
            if ($variant != null) {
                $price = Product_variant::where([['product_id', $item->id], ['value', $variant]])
                    ->pluck('price')->first();
            }
            else
            {
                $price = Product_variant::where('product_id', $item->id)
                    ->pluck('price')->first();

            }
            $order = Order::create(array(
                'customer_id'=>$customer_id,
                'product_id'=>$item->id,
                'variant'=>$item->options['variant'],
                'quantity'=>$item->qty,
                'delivery_address' => session('shippingAddress'),
                'billing_address' => session('billingAddress'),
                'instruction' => (session()->has('instruction')) ? session('instruction') : null,
                'payment_status'=>'pending',
                'product_price'=>$price,
                'order_total' => session('final_amount'),
            ));
        }
        $customer_name= $customer->firstname . ' ' . $customer->lastname;        
        return view('front.payment')->with(['carts' => $cartCollection, 'intent' => $intent, 'tax' => $fees->tax, 'shipping' => $shipping_charge, 'taxed' => $tax,'customer_name'=>$customer_name]);
    }
    //confirm order for both guest and user checkout
    public function confirmOrder(Request $request)
    {

        $items = Cart::content();
        $customer_id = Session::get('customer');
        if (session()->has('coupon_id')) {
            $coupon_id = session('coupon_id');
            CouponRedemption::create(array(
                'coupon_id' => $coupon_id,
                'customer_id' => $customer_id,
            ));
        } else
            $coupon_id = null;
        //order_id
        $last_order_id = Order::where('order_id','!=',null)->orderBy('id', 'desc')->first();
        //dd($last_order_id->order_id);
        if ($last_order_id == '')

            $order_id = 1000;

        else{
            $orde_id = $last_order_id->order_id + 1;
            if(!(Order::where('order_id',$orde_id)->exists()))
                $order_id = $orde_id;
            else {
                $last_order_id = Order::orderBy('order_id', 'desc')->first();
                $order_id = $last_order_id->order_id + 1;
            }
        }
        //dd($order_id);

        $stock = 1;
        foreach ($items as $item) {
            $variant = null;
            $variant = $item->options['variant'];
            if ($variant != null) {
                $price = Product_variant::where([['product_id', $item->id], ['value', $variant]])
                    ->pluck('price')->first();
                if($stock >= 1)
                    $stock = Product_variant::where([['product_id', $item->id], ['value', $variant]])
                        ->pluck('quantity')->first();
            }
            else
            {
                $price = Product_variant::where('product_id', $item->id)
                    ->pluck('price')->first();
                if($stock >= 1)
                    $stock = Product_variant::where('product_id', $item->id)
                        ->pluck('quantity')->first();
            }

            $product = Product::findOrFail($item->id);
            if ($product->offer != null && $product->offer_expire >= today())
                $price = $price - ($price * $product->offer / 100);
            $data = array(
                'customer_id' => $customer_id,
                'coupon_id' => $coupon_id,
                'order_total' => session('final_amount'),
                'status' => 'Order placed',
                'delivery_address' => session('shippingAddress'),
                'product_id' => $item->id,
                'order_id' => $order_id,
                'variant' => $variant,
                'quantity' => $item->qty,
                'product_price' => $price,
                'billing_address' => session('billingAddress'),
                'ip_address' => $request->ip(),
                'instruction' => (session()->has('instruction')) ? session('instruction') : null,
                'payment_status'=>'paid',
                'shipping_code'=>session()->get('shipping_code'),
            );
            //$order = Order::create($data);
            $order = Order::where([['order_id' ,null],['customer_id',$customer_id],['product_id',$product->id]])->OrderBy('id','desc')->first();
            $order->update($data);
            //reduce quantity
            if ($variant != null)
                $var = Product_variant::where([['product_id', $item->id], ['value', $variant]])->first();
            else
                $var = Product_variant::where('product_id', $item->id)->first();
            //dd($var);
            if ($var->quantity > 0) {
                $quantity = $var->quantity - $item->qty;
                $var->update(array('quantity' => $quantity));
            }
        }
        // store fee split up in orderfee_splitups table
        $fees = Additional_fee::where('province', Session::get('province'))->first();
        if ($coupon_id != null)
            $reduction = session()->get('reduction');
        else $reduction = 0;
        $data = array(
            'order_table_id' => $order->id,
            'order_id' => $order->order_id,
            'sub_total' => \Cart::total(),
            'shipping_charge' => session()->get('shipping_charge'),
            'tax' => $fees->tax,
            'reduction' => $reduction,
            'tax_value' => session()->get('tax_value'),
        );
        OrderFee_splitup::create($data);
        $customer = Customer::findOrFail($customer_id);

        $orders = Order::where('order_id', '=', $order_id)->get();
        //dd($order);
        //Order details.pdf
        $file_path = $this->download_pdf($order->order_id);
        //dd($file_path);

        //Sending order confirmation emails
        $email = $customer->email;
        Mail::send('front.emails.order-confirm', ['orders' => $orders,'stock'=>$stock], function ($message) use ($email, $file_path) {
            $message->to($email)
                ->subject('Jumbo Canada | Order Confirmation')
                ->attach($file_path);
        });
        //send email to client
        $email = env('MAIL_TO');
        Mail::send('front.emails.client-new-order', ['orders' => $orders], function ($message) use ($email, $file_path, $order_id) {
            $message->to($email)
                    ->cc(['amardeep@jumbocanada.com','lennies@jumbocanada.com'])
                    ->subject($order_id . '- New order')
                    ->attach($file_path);
        });

        if (!Mail::failures()) {
            //Unlink the attachement file
            unlink($file_path);
        }
        //dd('here');
        //forget session values
        Session::forget('cart');
        Session::forget('total');
        Session::forget('final_amount');
        Session::forget('coupon_id');
        Session::forget('shippingAddress');
        Session::forget('billingAddress');
        Session::forget('reduction');
        Session::forget('province');
        Session::forget('customer');
        Session::forget('instruction');
        Session::forget('shipping_charge');


       /* if (Auth::guard('customer')->check())
            return redirect(route('orderDetails'))
                ->with('success', 'Your order has been placed successfully');
        else
        {
            //return view('front.guest-order-details', ['order_id' => $order->order_id, 'email' => $customer->email])
            //->with('success', 'Your order has been placed successfully.');
            session()->put('order_id',$order->order_id);
            session()->put('email',$customer->email);
            return redirect(route('guest.orderView'))
                ->with('success', 'Your order has been placed successfully');
        }*/
       $id=base64_encode($order->order_id);
        return redirect(route('order-confirmation',$id));

    }

    //shipping address for registered user (on radio click)
    public function store_address($request)
    {
        session()->forget('shippingAddress');
        session()->put('shippingAddress', $request);
        // save province in session
        $address = Address::findOrFail($request);
        session()->forget('province');
        session()->put('province', $address->province);

        return ('done');
    }

    public function orderDetails()
    {
        $customer_id = Auth::guard('customer')->user()->id;
        $orders = Order::where([['customer_id', $customer_id],['order_id','!=',null]])->get();
        //dd($orders);
        return view('front.profile.order-details', ['orders' => $orders]);
    }

    public function orderCancel($id)
    {
        $order = Order::findOrFail($id);
        if ($order->status == 'Shipped' || $order->status == 'Delivered')
            return back()->with('error', 'Item Shipped cannot be cancelled');
        else if ($order->status == 'Cancel & refunded')
            return back()->with('error', 'Order Already cancelled');
        else {

            //update order status
            $order->update(['status' => 'Cancel requested']);
            //send email to customer
            $email = $order->customer->email;
            Mail::send('front.emails.order-cancel', ['order' => $order], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Order Cancel');
            });
            //send email to client
            Mail::send('front.emails.client-order-cancel', ['order' => $order], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | New Order Cancel Request');
            });

            if (Auth::guard('customer')->check())
                return back()->with('success', 'Successfully Requested Order Cancel  ');
            else
                return view('front.guest-order-details')->with(['email' => $order->customer->email, 'order_id' => $order->order_id])->with('success', 'Successfully Requested for Order Cancel');
        }
    }

//order return for both guest and registered user
    public function orderReturn(Request $request)
    {
        //dd($request);
        $data = $request->validate([
            'order_id' => 'required',
            'dead' => 'nullable',
            'faulty' => 'nullable',
            'order_error' => 'nullable',
            'wrong_item' => 'nullable',
            'reason' => 'nullable',
            'is_open' => 'required',
            'image1' => 'nullable',
            'image2' => 'nullable'
        ]);
        //upload image
        if ($request->image1) {
            $image_path = request('image1')->store('uploads/returns', 'public');
            $data['image1'] = $image_path;
        } else {
            $data['image1'] = '';
        }
        $return = Product_return::create($data);
        $order = Order::findOrFail($request->order_id);
        $order->status = "Return Requested";
        $order->save();
        //send email to customer
        $email = $order->customer->email;
        Mail::send('front.emails.return-requested', ['order' => $order], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product Return');
        });
        //send email to client
        Mail::send('front.emails.client-return-request', ['order' => $order, 'return' => $return], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product Return');
        });
        if (Auth::guard('customer')->check())
            return redirect(route('orderDetails'))->with('success', 'Return request submitted');
        else {
            //return redirect(route('guest.orderView'))->with('success','Return Reason Saved');
            $order = Order::findOrFail($request->order_id);

            return view('front.guest-order-details', ['order_id' => $order->order_id, 'email' => $order->customer->email])->with('success', 'Return request submitted');
        }

    }

    public function returnReason($id)
    {
        $order = Order::findOrFail($id);
        if ($order->status != 'Delivered')
            return back()->with('error', 'Cannot Return Undelivered Product');
        else
            return view('front.product.product-return', ['id' => $id]);
    }

    public function storeProvince($province)
    {
        Session::put('province', $province);
        return ('done');
    }

    // method to create pdf
    public function download_pdf($order_id)
    {

        $orders = Order::where('order_id', $order_id)->get();
        $data = '<html lang="en">
<body>
<table role="presentation" align="center" border="0" cellspacing="0" cellpadding="10" width="100%" bgcolor="#edf0f3"
       style="background-color:#ffffff;table-layout:fixed;">
    <tbody>
    <tr>
        <td>
                <div
                    style="background-color:#ffffff;max-width:600px;margin:auto; box-shadow: 1px 1px 8px rgba(0,0,0,0.1);">

                      <table cellspacing="0" cellpadding="0" align="center"
                           style="width:100%;border:3px solid #fb3333; border-top-width:5px; border-left-width:3px;margin-top: 50px;">
                        <tbody>
                        <tr>
                            <td align="center" style="vertical-align:middle">
                                <div style="padding: 10px; background: #f8f8f8;">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center"
                                           style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="vertical-align:middle;padding-top:10px;"
                                                bgcolor="#f8f8f8">
                                                <a href="https://jumbocanada.torontomalayali.ca"><img
                                                        src="https://jumbocanada.torontomalayali.ca/assets/img/logo.png"
                                                        alt="jumbocanada.com" height="35" style="display:block"></a></td>
                                            <td align="right"
                                                style="vertical-align:middle;"
                                                bgcolor="#f8f8f8">
                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td align="right">
                                                            <table align="right" cellpadding="0"
                                                                   cellspacing="0" border="0">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="right"
                                                                        colspan="4"
                                                                        style="padding: 5px 0; font-size: 11px;">
                                                                        <a
                                                                            href="mailto:support@jumbocanada.com">support@jumbocanada.com</a><br/>
                                                                            <a href="tel:+1 8557733844"> +1 (855) 773 3844</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <table cellspacing="0" cellpadding="0" border="0" align="center"
                                       style="width:100%" bgcolor="#fff">
                                    <tbody>
                                    <tr>
                                        <td align="center" style="vertical-align:middle;height:10px"
                                            bgcolor="#ffffff">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"
                                            style="vertical-align:middle;padding:0 15px; font-size:13px!important;line-height:1.255!important; color: #696969"
                                            bgcolor="#ffffff">';
        foreach ($orders as $order) {
            $data = $data . '<table
                    style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                    <thead>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"
                            colspan="2">Order Details
                 </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                            <b>Order ID:</b> ' . $order->order_id . '<br>
                            <b>Date Added:</b>' . date("d-M-Y", strtotime($order->created_at)) . '<br>
                            <b>Payment Method:</b> Credit / Debit Card<br>
                            <b>Shipping Method:</b> Flat Shipping Rate
                 </td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                            <b>E-mail:</b>
                            <a href="mailto:ggbranjithgmail.com" target="_blank">' . $order->customer->email . '</a><br>
                            <b>Telephone:</b>' . $order->customer->telephone . '<br>
                            <b>Order Status:</b> Complete<br>
                            <b>IP Address:</b>' . $order->ip_address . '
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table
                    style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                    <thead>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                     Shipping
                            Address
                            </td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                        Payment
                            Address
                            </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' .
                $order->deliveryAddress->firstname . ' ' . $order->deliveryAddress->lastname . '<br>' .
                $order->deliveryAddress->address_1 . ' ' . $order->deliveryAddress->address_2 . '<br>' .
                $order->deliveryAddress->city . '<br>' .
                $order->deliveryAddress->province . ' '.$order->deliveryAddress->postcode.'<br>
                        </td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' .
                $order->billingAddress->firstname . '  ' . $order->billingAddress->lastname . '<br>' .
                $order->billingAddress->address_1 . ' ' . $order->billingAddress->address_2 . '<br>' .
                $order->billingAddress->city . '<br>' .
                $order->billingAddress->province . ' '.$order->billingAddress->postcode.'<br>' .
                '</td>
                    </tr>
                    </tbody>
                </table>';
            break;
        }
        $data = $data . '<table
            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
            <thead>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
             Product
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
             Model
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
             Quantity
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
             Price
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
             Total
                </td>
            </tr>
            </thead>
            <tbody>';
        foreach ($orders as $order) {
            $data = $data . '<tr>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' . $order->product->name . '</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">' . $order->product->model . '</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">' . $order->quantity . '</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">C$' . number_format($order->product_price, 2) . '</td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                C$' . number_format($order->product_price * $order->quantity, 2)
                . '</td>
        </tr>';
        }
        $data = $data . '</tbody>';
        foreach ($orders as $order) {
            $data = $data . '<tfoot>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Sub-Total:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$' . $order->orderFeesplitUp->sub_total .
                '</td>
                </tr>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b> Shipping Charge:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$' . number_format($order->orderFeesplitUp->shipping_charge, 2) .
                '</td>
                </tr>';
            if ($order->orderFeesplitUp->reduction != 0) {
                $data = $data . '<tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Discount :</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$' . number_format($order->orderFeesplitUp->reduction, 2) .
                    '</td>
                </tr>';
            }
            if($order->orderFeesplitUp->tax_value == null){
                if($order->shipping_partner == 'Canada Post' && $order->shipping_code == '' ){
                    $taxed = ($order->orderFeesplitUp->sub_total - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
                }
                else
                    $taxed = ($order->orderFeesplitUp->sub_total+$order->orderFeesplitUp->shipping_charge - $order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100;
            }
            else
                $taxed = $order->orderFeesplitUp->tax_value;
            $data = $data.'<tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                    colspan="4"><b>Tax Rate ( '.$order->orderFeesplitUp->tax.'%):</b></td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                C$'.number_format(($taxed),2).
                '</td>
                </tr>
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Total:</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                 C$'.number_format(($order->order_total),2).
                '</td>
                </tr>
                </tfoot>';
            break;
        }
        $data = $data . '</table>';
        $data = $data . ' </td>
                           </tr>
                         </tbody>
                       </table>
                      </td>
                     </tr>
                    </tbody>
                   </table>
                </div>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>';
        $pdf = App::make('dompdf.wrapper');
        // PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->loadHTML($data);
        file_put_contents('OrderDetails.pdf', $pdf->output());
        //return $pdf->download('Order Details.pdf');
        return ('OrderDetails.pdf');
    }
// order confirmation after payment
    public function confirmation($id)
    {
        $id=base64_decode($id);
        $orders = Order::where('order_id',$id)->get();
        $products = Product::where('status','!=','Hidden')->with('getImage','brand','getVariant')->inRandomOrder()->limit(7)->get();
        return view('front.order-confirmation',['orders'=>$orders,'products'=>$products]);
    }
    //order confirmation print
    public function print_order($id)
    {
        $id=base64_decode($id);
        $file_path = $this->download_pdf($id);
        return response()->file($file_path)->deleteFileAfterSend(true);


    }
    public function GetTrackingDetails($pin)
    {
        //$pin = '1371134583769923';
        $canadapost = CanadaPost::GetTrackingDetails($pin);
        return (['msg'=>'success','datas'=>$canadapost]);

    }
}

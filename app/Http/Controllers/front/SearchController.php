<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $products = Product::where('name', 'LIKE','%'.$request->search . '%')->get();
            $output = '';
            if (count($products) > 0) {
                $output = '<ul class="list-group"
                style="display: block; position: absolute; z-index: 1;overflow: auto">';
                foreach ($products as $product) {
                    $output .= '<li class="list-group-item"><input type="hidden" id="pid" value="'.$product->slug.'">' .$product->name.
                                '<input type="hidden" id="brand" value="'.$product->brand.'"></li>';
                }
                $output .= '</ul>';
            } else {
                $output .= '<li class="list-group-item ">' . 'Sorry! No matching product found.' . '</li>';
            }
            // return output result array
            return $output;
        }
    }

}


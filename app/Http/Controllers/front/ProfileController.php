<?php

namespace App\Http\Controllers\front;

use App\Address;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;

class ProfileController extends Controller
{
    public function profile()
    {
        return view('front.profile.my-profile');
    }

    public function editProfile()
    {
        return view('front.profile.edit-profile');
    }

    public function getPassword()
    {
        return view('front.profile.password');
    }

    public function favourite()
    {
        return view('front.profile.favourite-list');
    }


    public function resetPassword(Request $request)
    {
        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
        if ($resultJson->success != true) {
            return back()->with('error', 'Recaptcha error')->withInput();
        }
        if ($resultJson->score >= 0.3) {
            $customer = Auth::guard('customer')->user();
            //dd($customer);
            if (Hash::check($request->current, $customer->password)) {
                $data = $request->validate([
                    'password' => 'required|same:confirm_password|regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d _\W]{8,16}$/i',
                ]);
                $customer->password = Hash::make($data['password']);
                $customer->save();
                return back()->with('message', 'Password updated successfully!');
            } else {
                return back()->withInput()->with('message', 'Current Password is wrong');
            }
        }
    }

    public function storeProfile(Request $request)
    {
        $data = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'telephone' => 'required'
        ]);
        $customer = Auth::guard('customer')->user();
        $customer->update($data);
        return back()->with('message', 'Data Updated Successfully');
    }
}

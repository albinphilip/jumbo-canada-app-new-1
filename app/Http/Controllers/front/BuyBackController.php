<?php

namespace App\Http\Controllers\front;

use App\Buy_back;
use App\Buy_back_code;
use App\CouponCode;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class BuyBackController extends Controller
{
    public function step1()
    {
        return view('front.buy-back.step1');
    }

    public function step2()
    {
        return view('front.buy-back.step2');
    }

    public function step3()
    {
        return view('front.buy-back.step3');
    }

    public function store(Request $request)
    {
//        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
//        if ($resultJson->success != true) {
//            return back()->with('error', 'Recaptcha error')->withInput();
//        }
//        if ($resultJson->score >= 0.3) {
            $data = $request->validate([
                'reason' => 'required',
                'serial_number' => 'nullable',
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required'
            ]);

            $buy_back = Buy_back::create($data);

//        dd($request->all(),$buy_back->id);

            return redirect(route('buy-back.step3'))->with('buy_back_id', $buy_back->id);
        //}
    }

    public function generate_code(Request $request)
    {
//        $resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
//        if ($resultJson->success != true) {
//            return back()->with('error', 'Recaptcha error')->withInput();
//        }
//        if ($resultJson->score >= 0.3) {

            $buy_back = Buy_back::findOrFail($request->buy_back_id);

            //Uploading and saving attached_file
            if ($request->attached_file) {
                $image_path = request('attached_file')->store('uploads/buy-back', 'public');
                $data['attached_file'] = $image_path;
            } else {
                $data['attached_file'] = '';
            }

            $buy_back->update($data);

            //Buy_back_code::create($new_date);
//        //generate coupon code based on product condition
//        $cpn = new CouponCode();
//        $cpn->code = mt_rand(11111, 99999999);
//        $cpn->expires_on = date('Y-m-d', strtotime('+5 years'));
//        $cpn->applicable_to = 'All';
//        $cpn->reduction_type = "Amount";
//        $cpn->value = 20;
//        $cpn->total_redemptions = 1;
//        $cpn->redemptions_per_customer = 1;
//        $cpn->status='Published';
//        $cpn->save();
            //sending email confirmation to customer
            $email = $buy_back->email;
            $name = $buy_back->name;
            Mail::send('front.emails.buy-back', ['name' => $name], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Buy Back Offer');
            });
            //send email to client
            $email = env('MAIL_TO');
            $name = $buy_back->name;
            Mail::send('front.emails.client-buy-back', ['data' => $buy_back], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | New Buy Back request');
            });
            return redirect(route('buy-back.step3'))->with('success', 'Your buy back request has been submitted. Please check your e-mail for details');
        }
    //}
}

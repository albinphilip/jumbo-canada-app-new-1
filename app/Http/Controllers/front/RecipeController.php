<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Recipe;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    /**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $medias=Recipe::where('category',"Media")->get();
        $recipes=Recipe::where('category',"Recipe")->get();
        return view('front.recipe',['recipes'=>$recipes,'medias'=>$medias]);
    }
}

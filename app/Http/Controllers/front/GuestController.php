<?php

namespace App\Http\Controllers\front;

use App\Additional_fee;
use App\Address;
use App\CouponCode;
use App\CouponRedemption;
use App\Customer;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderFee_splitup;
use App\Product;
use App\Product_variant;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Cart;
use Session;

class GuestController extends Controller
{

    //guest checkout shipping address
    public function guest_storeaddress(Request $request)
    {

        //store details in customer table
        $data = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
        ]);
        $data['password'] = Hash::make('');

        $customer = Customer::where('email',$request->email)->first();
        if(!$customer)
        {
            $customer = Customer::create($data);
        }
        else
        {
             $customer->update([
                'telephone' => $data['telephone'],
            ]);
        }
        $data = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'address_1'=>'required',
            'address_2'=>'nullable',
            'city'=>'required',
            'postcode'=>'required',
        ]);
        $data['zone_id']=1;
        $data['country_id']=1;
        $data['type']='shipping';
        $data['customer_id'] = $customer->id;
        $address = Address::create($data);
        session()->forget('shippingAddress');
        session()->put('shippingAddress',$address->id);
        return back()->with('data', $customer);

    }

    //apply coupon for both guest and registered user
    public function coupon(Request $request)
    {
        $data = $request->validate([
            'coupon' => 'required',
        ]);

        //Check coupon exists
        $coupon_details = CouponCode::where('code', $data['coupon'])->first();
        if ($coupon_details == '') return back()->with('error', 'Coupon Code Does not Exists');
        $customer_id = session()->get('customer');
        $total_amount = str_replace(',', '', \Cart::total());
        $total_redemption = CouponRedemption::where('coupon_id', $coupon_details->id)->count();
        $cus_redemption = CouponRedemption::where([['coupon_id', $coupon_details->id], ['customer_id', $customer_id]])->count();
        $order = Order::where([['customer_id', $customer_id],['order_id','!=','']])->get();
        if ($order) $status = 'Existing Customer';
        else $status = 'New Customer';
        if ($coupon_details->category != 'All' && $coupon_details->category != null){
            $items = \Cart::content();
            $flag = 1;
            foreach ($items as $item) {
                $product = Product::findOrFail($item->id);
                //dd($coupon_details->category);
                if ($product->category->name == $coupon_details->category) {
                    $flag = 0;
                    break;
                }
            }
            if ($flag != 0) {
                return back()->with('error', 'Coupon not applicable to this category ');
            }
        }
        if ($coupon_details->applicable_product != null){
            $items = \Cart::content();
            $flag = 1;
            foreach ($items as $item) {
                $product = Product::findOrFail($item->id);
                //dd($coupon_details->category);
                if ($product->name == $coupon_details->product->name) {
                    $flag = 0;
                    break;
                }
            }
            if ($flag != 0) {
                return back()->with('error', 'Coupon not applicable to this Product ');
            }
        }
        if(!$coupon_details || $coupon_details->expires_on < today()||$coupon_details->status == 'Hidden')
        {
            return back()->with('error','Invalid Coupon');

        }
        else if($total_redemption >= $coupon_details->total_redemptions || $cus_redemption >= $coupon_details->redemptions_per_customer)
        {
            return back()->with('error','Coupon Limit Exceeded');
        }
        else if(($status == 'New Customer' && $coupon_details->applicable_to == 'Existing Customers')
            ||($status == 'Existing Customer' && $coupon_details->applicable_to == 'New Customers'))
        {

            return back()->with('error','Coupon Not Applicable');
        }

        else{
            //Session::set('total', \Cart::total());

            if($coupon_details->reduction_type == 'Percent')
            {
                $items = \Cart::content();
                foreach ($items as $item) {
                    $product = Product::findOrFail($item->id);
                    //dd($coupon_details->category);
                    if (($coupon_details->applicable_product != null && $product->name == $coupon_details->product->name)) {
                        //dd($product->name);
                        $reduction = ($item->price * $coupon_details->value) / 100;
                        break;
                    }
                    else if(($coupon_details->category!=null && ($product->category->name == $coupon_details->category || $coupon_details->category == 'All'))){
                        //dd($product->name);
                        $reduction = ($item->price * $coupon_details->value) / 100;
                        break;
                    }
                }
                $total = $total_amount-$reduction;
            }
            else
            {
                $reduction = $coupon_details->value;
                $total = $total_amount - $reduction;
            }
            //check whether amount < 1$
            if($total < 1)
            $total = 1;
            $total = number_format($total,'2','.',',');
            $request->session()->put('coupon_id', $coupon_details->id);
            /*CouponRedemption::create(array(
                'coupon_id' => $coupon_details->id,
                'customer_id' => $customer_id,
            ));*/
            session()->put('total',$total);
            session()->put('reduction',$reduction);
            return redirect(route('checkout'));
        }
    }
    public function orderDetails(Request $request)
    {
        /*$resultJson = Gcaptcha::verifyCaptcha($request->get('recaptcha_response'));
        if ($resultJson->success != true) {
            return back()->with('error', 'Recaptcha error')->withInput();
        }
        if ($resultJson->score >= 0.3) {*/
            $customer_id = Customer::where('email', $request->email)->pluck('id')->first();
            $order = Order:: where('order_id', $request->order_id)->first();
            if (!$customer_id || !$order)
                return redirect(route('guest.orderView'))->with('error', 'Invalid Details')->withInput();
            else {
                $orders = Order::where([['customer_id', $customer_id], ['order_id', $request->order_id]])->get();
                //dd($orders);
                //return view('front.guest-order-details', ['orders' => $orders]);
                $id=base64_encode($order->order_id);
                return redirect(route('order-confirmation',$id));
            }

        //}
    }
    public function orderView()
    {
        return view('front.guest-order-view');
    }
}

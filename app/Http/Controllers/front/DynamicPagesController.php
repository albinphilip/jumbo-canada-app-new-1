<?php

namespace App\Http\Controllers\front;
use App\DynamicPages;
use App\Http\Controllers\Controller;

class DynamicPagesController extends Controller
{
    public function getContent($slug){
        $page=DynamicPages::where('slug',$slug)->first();
        return(view('front.policy',['page'=>$page]));
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Front end routes
//Route::get('/',function (){return view('coming-soon');});
//Route::get('/home', 'front\HomeController@index')->name('home');
Route::get('/', 'front\HomeController@index')->name('home');
Route::post('/enquiry', 'front\EnquiryController@store')->name('enquiry');
Route::get('/about-jumbo-canada', function () {
    return view('front.about');
})->name('about');
Route::get('/contact-us', function () {
    return view('front.contact');
})->name('contact');
Route::get('/set-province/{province}', 'front\OrderController@storeProvince');
Route::post('/subscribe', 'front\SubscriptionController@subscribe')->name('subscription');
Route::get('/recipe', 'front\RecipeController@index')->name('recipe');
Route::get('/faq', 'front\FaqController@index')->name('faq');
Route::get('/store-locator', 'front\StoreController@index')->name('store');
Route::get('policy/{id}', 'front\DynamicPagesController@getContent')->name('policy');

Route::get('/warranty-registration', 'front\ProductRegistrationController@create')->name('warranty-registration');
Route::post('/product-registration/store', 'front\ProductRegistrationController@store')->name('product-registration.store');
Route::get('/warranty-reregistration', function () {
    return view('front.product.product-re-registration');
})->name('warranty-re-registration');
Route::post('/warranty-reregistration-post', 'front\ProductRegistrationController@reregister')->name('warranty-re-registration.store');
Route::get('/repair-product', 'front\ProductRepairRegistrationController@create')->name('repair-product');
Route::post('/product-repair-registration/store', 'front\ProductRepairRegistrationController@store')->name('product-repair-registration');
Route::get('/buy-back/step1', 'front\BuyBackController@step1')->name('buy-back.step1');
Route::get('/buy-back/step2', 'front\BuyBackController@step2')->name('buy-back.step2');
Route::get('/buy-back/step3', 'front\BuyBackController@step3')->name('buy-back.step3');
Route::post('/buy-back/step2/store', 'front\BuyBackController@store')->name('buy-back.step2.store');
Route::post('/generate-discount-code', 'front\BuyBackController@generate_code')->name('generate-discount-code');

Route::get('/products/{product}', 'front\HomeController@product_list')->name('listproduct');
Route::get('/products/{brand}/{product}', 'front\HomeController@product_brand_list')->name('brand_listproduct');
Route::get('/product/{brand}/product-detail/{product}', 'front\HomeController@product_detail')->name('productdetail');
Route::get('/productprice/{product}', 'front\HomeController@product_price')->name('productprice');
Route::get('/product/compare/{id}', 'front\ProductCompareController@product_store')->name('compare.store');
Route::get('/product/comparison/{id}', 'front\ProductCompareController@comparison')->name('product.comparison');
Route::get('/product/comparison/destroy/{id}', 'front\ProductCompareController@destroy')->name('comparison.destroy');

Route::get('/customer-signup', 'front\RegisterController@index')->name('register-user');
Route::post('/registration', 'front\RegisterController@user_registration')->name('postregistration');

Route::get('/customer-login', 'front\LoginController@index')->name('customerlogin');
Route::post('/post-login', 'front\LoginController@postLogin')->name('post-login');

Route::get('/search', 'front\SearchController@search')->name('search');

Route::get('/partner-with-us', 'front\PartnerController@index')->name('partner');
Route::post('/partner/store', 'front\PartnerController@store')->name('partner.store');

Route::post('/confirm-order', 'front\OrderController@confirmOrder')->name('confirm-order');

Route::middleware('customerAuth')->group(function () {
    Route::get('/profile', 'front\ProfileController@profile')->name('profile');

    Route::get('/address', 'front\AddressController@address')->name('address');
    Route::post('/address', 'front\AddressController@storeaddress')->name('storeaddress');
    Route::get('/delete-address/{id}', 'front\AddressController@deleteaddress')->name('delete-address');
    Route::get('/edit-address/{id}', 'front\AddressController@editaddress')->name('edit-address');
    Route::post('/update-address/{id}', 'front\AddressController@updateaddress')->name('update-address');
    Route::get('/edit-profile', 'front\ProfileController@editProfile')->name('edit-profile');
    Route::post('/editProfile', 'front\ProfileController@storeProfile')->name('save-profile');
    Route::get('/reset-customer-password', 'front\ProfileController@getPassword')->name('customer-password');
    Route::post('/resetPassword', 'front\ProfileController@resetPassword')->name('resetPassword');
    Route::get('/customer-logout', 'front\LoginController@logout')->name('customerlogout');

    Route::post('/applyCoupon', 'front\CartController@coupon')->name('appplyCoupon');

    Route::get('/store-delivery-address/{id}', 'front\OrderController@store_address');
    Route::get('/order-details', 'front\OrderController@orderDetails')->name('orderDetails');

    Route::get('/wishlist', 'front\WishlistController@index')->name('wishlist.index');
    Route::get('/wishlist/store/{id}', 'front\WishlistController@store')->name('wishlist.store');
    Route::get('/wishlist/destroy/{id}', 'front\WishlistController@destroy')->name('wishlist.destroy');

    Route::post('/product/review/store', 'front\ReviewController@store')->name('review.store');
    Route::get('/product/review/{id}', 'front\ReviewController@index')->name('review.index');

    Route::post('/user/shipping-address', 'front\ShippingController@user_storeaddress')->name('user.storeaddress');
    Route::post('/user/billing-address', 'front\ShippingController@user_billingaddress')->name('user.billingaddress');


});
// Password Reset Routes...
Route::get('password/forgot', 'front\ForgotPasswordController@showLinkRequestForm')->name('password.forgot');
Route::post('password/sendemail', 'front\ForgotPasswordController@sendResetLinkEmail')->name('password.sendemail');
Route::get('password/reset/request/{token}', 'front\ForgotPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/resetpassword', 'front\ForgotPasswordController@reset')->name('password');
//cart
Route::get('/cart', 'front\CartController@cart')->name('cart.index');
Route::post('/add-to-cart', 'front\CartController@add')->name('cart.store');
Route::get('/delete-from-cart/{id}', 'front\CartController@remove')->name('cart.remove');
Route::get('/decrease-quantity/{id}', 'front\CartController@decreaseQuantity')->name('cart-minus');
Route::get('/increase-quantity/{id}', 'front\CartController@increaseQuantity')->name('cart-plus');
//guest checkout routes
Route::post('/guest/applyCoupon', 'front\GuestController@coupon')->name('guest.appplyCoupon');
Route::get('/checkout', 'front\OrderController@payment')->name('checkout');
Route::get('/checkout-shipping', 'front\ShippingController@shipping')->name('shipping');
Route::get('/guest-store-delivery-address/{address}', 'front\GuestController@guest_address');
Route::post('/guest/address', 'front\ShippingController@guest_storeaddress')->name('guest.storeaddress');
//order details
Route::post('/guest/order-details', 'front\GuestController@orderDetails')->name('guest.orderDetails');
Route::get('/guest/order-view', 'front\GuestController@orderView')->name('guest.orderView');
Route::get('/return-reason/{id}', 'front\OrderController@returnReason')->name('returnReason');
Route::post('/order-return', 'front\OrderController@orderReturn')->name('orderReturn');
Route::get('/order-cancel/{id}', 'front\OrderController@orderCancel')->name('orderCancel');

Route::get('/exclusive-discount-for-students-and-new-immigrants', 'front\DiscountController@create')->name('discount');
Route::post('/discount-request/store', 'front\DiscountController@store')->name('discount.store');

Route::get('/order-confirmation/{id}','front\OrderController@confirmation')->name('order-confirmation');
Route::get('/order-confirmation-print/{id}','front\OrderController@print_order')->name('order-print');

Route::get('/front/order/tracking/{id}','front\OrderController@GetTrackingDetails')->name('front_order.tracking');

//Auth Routes
// Authentication Routes...
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
    'as' => 'password.update',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Registration Routes...
Route::get('register', 'front\RegisterController@index');
Route::post('register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);

//Admin End Routes
Route::middleware('auth')->group(function () {
    Route::get('/admin', 'admin\HomeController@index')->name('adminHome');
    Route::resource('/admin/post', 'admin\PostController')->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
    Route::resource('/admin/recipe', 'admin\RecipeController')->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
    Route::resource('/admin/faq', 'admin\FaqController')->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
    Route::resource('/admin/store', 'admin\StoreController')->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
    Route::resource('/admin/enquiry', 'admin\EnquiryController')->only(['index', 'destroy']);
    Route::get('/admin/bulkDelete/{id}', 'admin\EnquiryController@bulkDelete')->name('bulkDelete');
    Route::resource('/admin/password', 'admin\PasswordController')->only(['edit', 'update']);

    Route::resource('/admin/category', 'admin\CategoryController')->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);
    Route::resource('/admin/brand', 'admin\BrandController')->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);
    Route::resource('/admin/product', 'admin\ProductController')->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);

    Route::resource('/admin/serial', 'admin\ImportController')->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);
    Route::get('/admin/serial/{file}', 'admin\ImportController@getDownload');

    Route::resource('/admin/product-registration', 'admin\ProductRegistrationController')->only(['index', 'edit', 'update']);
    Route::resource('/admin/product-repair-registration', 'admin\ProductRepairRegistrationController')->only(['index', 'edit', 'update', 'store']);
    Route::resource('/admin/product-banner', 'admin\ProductBannerController')->only(['index', 'create', 'store', 'destroy']);
    Route::resource('/admin/buy-back', 'admin\BuyBackController')->only(['index']);
    Route::get('/admin/buy-back-code', 'admin\BuyBackController@buy_back_code')->name('buy_back_code');

    Route::get('/admin/delete_spec', 'admin\ProductSpecificationController@delete_spec')->name('specification_delete');
    Route::get('/admin/delete_image', 'admin\ProductSpecificationController@delete_image')->name('image_delete');
    Route::get('/admin/delete_variant', 'admin\ProductSpecificationController@delete_variant')->name('variant_delete');
    Route::get('/admin/delete_video', 'admin\ProductSpecificationController@delete_video')->name('video_delete');

    Route::resource('/admin/couponcode', 'admin\CouponCodeController')->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
    Route::resource('/admin/customer', 'admin\CustomerController')->only(['index', 'destroy']);
    Route::resource('/admin/order', 'admin\OrderController')->only(['index', 'update']);
    Route::resource('/admin/return', 'admin\ReturnController')->only(['index']);
    Route::post('/admin/return/reject', 'admin\ReturnController@reject')->name('return.reject');
    Route::post('/admin/return/approve', 'admin\ReturnController@approve')->name('return.approve');
    Route::post('/admin/return/shipping-details', 'admin\ReturnController@shippingDetails')->name('returnShipping');
    Route::get('/admin/customer-order/{id}', 'admin\OrderController@customer')->name('customer-order');
    Route::get('/admin/product/out-of-stock', 'admin\ProductController@out')->name('out-of-stock');
    Route::resource('/admin/partner', 'admin\PartnerRegistrationController')->only(['index', 'update', 'edit']);
    Route::resource('/admin/fees', 'admin\AdditionalFeesController')->only(['index', 'create', 'store', 'update', 'edit', 'destroy']);
    Route::get('/admin/orderfee-splitup/{order_id}', 'admin\OrderController@splitup')->name('splitup');
    Route::post('/admin/order/shipping-details', 'admin\OrderController@shippingDetails')->name('order.shippingDetails');
    Route::resource('/admin/pages', 'admin\DynamicPageController')->only(['edit', 'update']);
    Route::post('/admin/order/export-to-csv', 'admin\ExportOrderController@export')->name('order.export');
    Route::get('/admin/product-registration/export-to-csv', 'admin\ProductRegistrationController@exportData')->name('warranty.export');
    Route::get('/admin/order/download-pdf/{id}', 'admin\ExportOrderController@download_pdf')->name('download-pdf');
    Route::post('/admin/order/delivery-date', 'admin\OrderController@deliveryDate')->name('order.deliveryDate');

    Route::post('/admin/buyback/approve', 'admin\BuyBackController@approve')->name('buyback.approve');
    Route::resource('/admin/discount', 'admin\DiscountRequestController')->only(['index', 'update', 'edit']);
    Route::post('/admin/discount/approve', 'admin\DiscountRequestController@approve')->name('discount.approve');
    Route::post('/admin/discount/reject', 'admin\DiscountRequestController@reject')->name('discount.reject');

    Route::get('/admin/report/tax', 'admin\ReportController@tax')->name('tax');
    Route::get('/admin/report/order-report', 'admin\ReportController@order')->name('order');
    Route::get('/admin/report/performance-report', 'admin\ReportController@performance')->name('performance');
    Route::post('/admin/report/tax-report', 'admin\ReportController@tax_report')->name('tax-report');
    Route::post('/admin/report/order-report', 'admin\ReportController@order_report')->name('order-report');
    Route::post('/admin/report/performance-report', 'admin\ReportController@performance_report')->name('performance-report');
    Route::post('/admin/customer/export-to-csv', 'admin\CustomerController@export')->name('customer.export');

    Route::post('/admin/repair-registration/reject', 'admin\ProductRepairRegistrationController@reject')->name('repair.reject');

    Route::get('/admin/report/return-report', 'admin\ReportController@return')->name('return');
    Route::post('/admin/report/return-report', 'admin\ReportController@return_report')->name('return-report');
     // popup
    Route::resource('/admin/popup', 'admin\PopupController')->only(['index', 'create', 'store']);

    Route::get('/product/bestseller/{id}' , 'admin\ProductController@bestseller')->name('product.bestseller');
    Route::get('/product/remove-bestseller/{id}' , 'admin\ProductController@remove_bestseller')->name('product.remove-bestseller');

    Route::get('/admin/order/abandoned-cart','admin\OrderController@abandoned_cart')->name('abandoned_cart');
    Route::get('/admin/order/shipping-label/{orderId}', 'admin\OrderController@CreateShipment')->name('order.shippingLabel');
    Route::get('/admin/order/bulkDelete/{id}','admin\OrderController@bulk_delete')->name('abandoned_cart.delete');

    //order invoice
    Route::get('/admin/order/invoice/{id}','admin\InvoiceController@invoice')->name('order.invoice');
    Route::get('/admin/order/tracking/{id}','admin\InvoiceController@GetTrackingDetails')->name('order.tracking');
    //shipping discount
    Route::resource('/admin/shippingdiscount', 'admin\ShippingDiscountController')->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
    // free shipping
    Route::resource('/admin/free_shipping', 'admin\FreeShippingController')->only(['index', 'create', 'store']);
    // product page banner
    Route::resource('/admin/banner', 'admin\BannerController')->only(['index', 'create', 'store']);


});
//test routes
//Route::get('/test','front\HomeController@test');
Route::get('/return-product', function () {
    return view('front.product-return');
})->name('returnProduct');
Route::post('/post-product-return', 'front\ProductReturnController@productReturn')->name('productReturn');
Route::get('/return-product-store-status-update', function () {
    return view('front.product-return-status');
})->name('returnStatusUpdate');
Route::post('/return-product-store-status-update', 'front\ProductReturnController@returnStatusUpdate')->name('returnStatusUpdate.store');

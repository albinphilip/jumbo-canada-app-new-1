<!DOCTYPE html>
<html>
<head><meta charset="utf-8">
    <meta name="description" content="Ultralinks Distribution Inc. Ontario is an authorized for sales and distribution of Preethi Kitchen Appliances in Canada . Preethi Kitchen Appliances Pvt Ltd, Chennai are the manufacturer of the product and entire responsible for the brand, warranty, claims etc. Preethi is India's largest mixer grinder brand and a leading Kitchen Appliances company in India and South East Asia. This platform is the official gateway for purchasing Preethi products from the original source at the right pricing. Moreover we ensures support with a 100% product & customer satisfaction.">
    <title>Preethi Kitchen Appliances Canada</title>
    <style type="text/css">body, html {
            height: 100%;
            margin: 0;
        }

        .bgimg {
            height: 100%;
            background-position: center;
            background-size: cover;
            position: relative;
            color: #052640;
            font-size: 25px;
        }

        .topleft {
            position: absolute;
            top: 0;
            left: 16px;
        }

        .bottomleft {
            position: absolute;
            bottom: 0;
            left: 16px;
        }

        .middle {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            text-align: center;
        }

        hr {
            margin: auto;
            width: 40%;
        }
    </style>
</head>
<body>
<div class="bgimg">
    <div class="middle"><img alt="" src="{{asset('assets/img/logo.png')}}" />
        <h1>Under Maintenance</h1>
    </div>

    <div class="bottomleft">
        <p>Contact: <a href="tel:18557733844">+1 (855) 773 3844</a>&nbsp;| <a href="mailto:support@jumbocanada.com">support@jumbocanada.com</a></p>
    </div>
</div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NSZBWLK');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO META TAGS -->
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta property="og:url" content="{{Request::url()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:description" content="@yield('description')"/>
    <meta property="og:image" content="{{asset('assets/img/thumb.png')}}"/>
    <link rel="canonical"     href="@yield('canonical')"/>
    <!-- SEO META TAGS -->

    <!-- ICONS -->
    <link rel="icon" href="{{asset('favicon.png')}}?v1.2" type="image/png">
    <!-- ICONS -->

    <!--STYLESHEETS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    <!-- STYLESHEET CDN -->

    <!-- STYLESHEET INTERNAL -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}?v1.6">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.theme.default.min.css')}}">
    <!-- STYLESHEET INTERNAL -->
     <!-- Add fancyBox -->
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

 

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NSZBWLK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<nav class="navbar navbar-expand-lg navbar-light">
    <!--    <a href="" class="toggle-link header-link">-->
    <!--        <img src="assets/img/toggler.png" alt="" class="img-fluid">-->
    <!--    </a>-->
    <a  href="{{route('cart.index')}}" id="c_count" class="cart header-link">
        <img src="{{asset('assets/img/cart-icon.png')}}" alt="" class="img-fluid">
        <span class="count">{{\Cart::count()}}</span>
    </a>
    <div class="dropdown account">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @if(Auth::guard('customer')->check())
                {{Auth::guard('customer')->user()->firstname}}
            @else
                Sign In
            @endif
            <img src="{{asset('assets/img/arrow-white-down.png')}}" alt="" class="img-fluid">

        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @if(Auth::guard('customer')->check())

                <li><a class="dropdown-item" href="{{route('edit-profile')}}">My Profile</a></li>
                <li><a class="dropdown-item" href="{{route('cart.index')}}">My Saved Carts</a></li>
                <li><a class="dropdown-item" href="{{route('orderDetails')}}">My Orders</a></li>
                <li><a class="dropdown-item" href="{{route('wishlist.index')}}">My Wish Lists</a>
                <li><a class="dropdown-item" href="{{route('customerlogout')}}">Logout</a></li>
            @else
                <li><a class="dropdown-item" href="{{route('customerlogin')}}">Login</a></li>
                <li><a class="dropdown-item" href="{{route('register-user')}}">Create Account</a></li>
            @endif
        </ul>
    </div>
    <div class="container-fluid">
        <a class="navbar-brand" href="{{route('home')}}">
            <img src="{{asset('assets/img/logo.svg')}}?v1.2" class="img-fluid" alt="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="collapsibleNavbar">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('home')}}">home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('about')}}">about us</a>
                </li>
                <li class="nav-item dropdown ddown">
                    <a class="nav-link dropdown-toggle drp" href="#" data-toggle="dropdown"
                       aria-expanded="false">brands<img src="{{asset('assets/img/header-plus.svg')}}" alt=""></a>
                    <ul class="dropdown-menu">
                        @foreach($brands as $brand )
                        <li class="ddown"><a class="dropdown-item"
                               href="{{route('brand_listproduct',[strtolower($brand->name),urlencode('all')])}}">{{$brand->name}}</a></li>
                        @endforeach

                    </ul>
                </li>
                <li class="nav-item dropdown ddown">
                    <a class="nav-link dropdown-toggle drp" href="#" data-toggle="dropdown"
                       aria-expanded="false">products <img src="{{asset('assets/img/header-plus.svg')}}" alt=""></a>
                    <ul class="dropdown-menu pdt">
                        @foreach($categories->sortBy('id') as $category)
                          <li class="ddown">  <a class="dropdown-item"
                               href="{{route('listproduct',strtolower(explode(' ',$category->name)[0]))}}"><img
                                    src="{{asset($category->icon)}}" alt="">
                                {{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </li>

                
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('store')}}">store locator</a>
                </li>
                <li class="nav-item dropdown ddown">
                    <a class="nav-link dropdown-toggle drp" href="#" data-toggle="dropdown"
                       aria-expanded="false">support<img src="{{asset('assets/img/header-plus.svg')}}" alt=""></a>
                    <ul class="dropdown-menu">
                        <li class="ddown"><a class="dropdown-item" href="{{route('warranty-registration')}}">Warranty Registration</a></li>
                        <li class="ddown"><a class="dropdown-item" href="{{route('repair-product')}}">Repair Product</a></li>
                        <li class="ddown"><a class="dropdown-item" href="{{route('buy-back.step1')}}">Recycle Product</a></li>
                        <li class="ddown"><a class="dropdown-item" href="{{route('returnProduct')}}">Return Product</a></li>
                        <li class="ddown"><a class="dropdown-item" href="{{route('discount')}}">Discount Registration</a></li>
                        <li class="ddown"><a class="dropdown-item" href="{{route('contact')}}">Contact Us</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-form-link" href="javascript:void(0)"><img src="{{asset('assets/img/search.svg')}}" alt=""></a>
                    <div class="col-12">
                        <form class="nav-form" action="#" method="get" id="search_form" >
                            @csrf
                            <input type="text" placeholder="Search..." autocomplete="off" class="form-control" id="search"
                                   name="search">
                            <button type="button" class="form-control" onclick="live_search();"><img src="{{asset('assets/img/search-white.png')}}" alt="">
                            </button>
                            <div class="result-block" id="search-list" ></div>

                        </form>
                    </div>

                </li>

            </ul>

        </div>
    </div>
</nav>
@yield('content')

<footer>
    <div class="footer-info">
        <div class="container">
            <ul>
                <li>
                    <div class="inner-content">
                        <div class="image">

                            <img src="{{asset('assets/img/free-delivery.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="detail">
                            <h4>Free delivery</h4>
                            <h6>On orders over C${{$free}}</h6>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner-content">
                        <div class="image">
                            <img src="{{asset('assets/img/heart.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="detail">

                            <h4>expert help</h4>
                            <h6>just ask us!</h6>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner-content">
                        <div class="image">

                            <img src="{{asset('assets/img/canada.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="detail">

                            <h4>100% CANADIAN</h4>
                            <h6>AND PROUD OF IT!</h6>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner-content">
                        <div class="image">

                                        <img src="{{asset('assets/img/conversation.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="detail">

                            <h4>price guarantee</h4>
                            <h6>we'll match any price!</h6>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="stay">
                        <img src="{{asset('assets/img/mail-send.png')}}" alt="">
                        <h5>STAY IN TOUCH</h5>
                        <p>Get THE LATEST DEALS, UPDATES & MORE</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <form method="post" id="subscribe">
                        @csrf
                        <input type="email" placeholder="Enter your email" class="form-control" name="email" required id="email_sub">
                        <button type="submit" class="form-control" id="save">subscribe</button>
                    </form>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 ">
                    <ul class="list-inline social-media" hidden>
                        <li class="list-inline-item"><span>Follow us:</span></li>
                        <li class="list-inline-item"><a href=""><i class="fa fa-facebook" aria-hidden="true"></i>
                            </a></li>
                        <li class="list-inline-item"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i>
                            </a></li>
                        <li class="list-inline-item"><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a></li>
                        <li class="list-inline-item"><a href=""><i class="fa fa-instagram" aria-hidden="true"></i>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-3">
                            <h4>Shop</h4>
                            <ul class="quick-links">
                                @foreach($brands as $brand )
                                    <li><a href="{{route('brand_listproduct',[strtolower($brand->name),urlencode("all")])}}">
                                            {{$brand->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h4>About</h4>
                            <ul class="quick-links">
                                <li><a href="{{route('about')}}">About Us</a></li>
                                <li><a href="{{route('contact')}}">Contact us</a></li>
                                <li><a href="{{route('store')}}">Stores</a></li>
                                <li><a href="{{route('faq')}}">FAQ</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h4>Policy</h4>
                            <ul class="quick-links">
                                <li><a href="{{route('policy','terms-and-conditions-of-warranty-and-safe-appliances-usage')}}">Terms & Conditions</a></li>
                                <li><a href="{{route('policy','privacy-policy')}}">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h4>Support</h4>
                            <ul class="quick-links">
                                <li><a href="{{route('warranty-registration')}}">Warranty Registration</a></li>
                                <li><a href="{{route('repair-product')}}">Repair Product</a></li>
                                @if(Auth::guard('customer')->check())
                                    <li><a href="{{route('orderDetails')}}">Return product</a></li>
                                @else
                                    <li><a href="{{route('guest.orderView')}}">Return product</a></li>
                                @endif
                                <li><a href="{{route('discount')}}">Discount Registration</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="contact-info">
                        <h4>Contact Info</h4>
                        <h5>Free support line !</h5>
                        <h4><a href="tel:18557733844">+1 (855) 773 3844</a></h4>
                        <h5>Email</h5>
                        <h4><a href="mailto:support@jumbocanada.com">support@jumbocanada.com</a></h4>
                    </div>
                </div>
            </div>
            <div class="accordion" id="accordion-two">
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse"
                                    data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree">
                                About us
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                         data-parent="#accordion-two">
                        <div class="card-body">
                            <p>A kitchen is every woman largest financial and emotional investment. 
                            At Jumbo Canada, our goal is to assist you in crafting your dream 
                            kitchen with the right appliance, especially sourced <i>‘Make in India’</i> 
                            range of products.</p>

                        </div>
                    </div>
                </div>

            </div>
            <p class="copy-right">All rights reserved. Copyright {{now()->year}} | Designed by :<a href="https://mapletechspace.com/" target="_blank">Maple Tech Space</a></p>

        </div>
    </div>
</footer>
<!-- from submission -->
<script defer src="{{asset('assets/js/php-email-form.js')}}"></script>
<!-- form submission -->
   <!--SCRIPTS CDN -->
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!--    slider-price-->
    <script defer src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    <!--SCRIPTS CDN -->

    <!--SCRIPTS INTERNAL -->
    <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}?v1.1"></script>
    <!--SCRIPTS INTERNAL -->
    <!-- RECAPTCHA -->
    <script src="https://www.google.com/recaptcha/api.js?render="></script>
    <!-- SweetAlert2 -->
    <script defer src="{{asset('/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <script defer>
         $("#subscribe").on('submit', function (event) {
        
            $("#save").val('Please wait ..');
            $("#save").html('Please wait ..');
            email =$('#email_sub').val();
            console.log(email);
            event.preventDefault();
            //grecaptcha.ready(function () {
              //  grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "contact"}).then(function (token) {
                    $.ajax({
                        type: 'post',
                        url: '/subscribe',
                        data:{ email,
                        "_token": "{{ csrf_token() }}",
                    },
                        success: function (response) {

                            if (response == 'OK'){


                                fireTost('Thank you for subscribing');

                            }
                            else if(response == 'YES'){
                                fireTost('Already Subscribed');

                            }
                            else{
                                alert(response);
                                fireTosterror(response);

                            }
                            $("#save").html('Subscribe');
                            $("#subscribe")[0].reset();
                                },
                        error: function () {
                            alert('Some error occurred');
                        }
                    });
              //  })
            //  })
        });
        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: false,
               // position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
        function fireTosterror(message) {
            const Toast1 = Swal.mixin({
                toast: false,
              //  position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast1.fire({
                icon: 'error',
                title: message,
            })
        }
        $(function () {
        const Toast = Swal.mixin({
            toast: false,
            showConfirmButton: true,
            timer: 7000
        });

        @if ($message = Session::get('success'))
        Toast.fire({
            icon: 'success',
            title: '{{$message}}.'
        })
        @endif
        @if ($message = Session::get('error'))
        Toast.fire({
            icon: 'error',
            title: '{{$message}}.'
        })
        @endif
        @if ($message = Session::get('warning'))
        Toast.fire({
            icon: 'warning',
            title: '{{$message}}.'
        })
        @endif
    });
    $('input[type="file"]').change(function (e) {
        var fileName = e.target.files[0].name;
        $("#fileupload-img").html('<img src="{{asset('assets/img/attach.svg')}}"alt="Preethi mixer - ultra kitchen appliances"> ' + fileName)
    });



</script>
<script>

    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });

    // make it as accordion for smaller screens
    if ($(window).width() < 200) {
        $('.dropdown-menu a').click(function (e) {
            e.preventDefault();
            if ($(this).next('.submenu').length) {
                $(this).next('.submenu').toggle();
            }
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('.submenu').hide();
            })
        });
    }
    $(function () {
        $('.drp').on('click', function (e) {

            if (!$('.submenu').next().hasClass('show')) {
                $('.submenu').removeClass('show');
            }


        });
        $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
            }
            var $subMenu = $(this).next('.dropdown-menu');
            $subMenu.toggleClass('show');


            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                $('.dropdown-submenu .show').removeClass('show');
            });


            return false;
        });

    });

    $(document).ready(function () {
        // setTimeout(function () {
        //     $('#tracking-modal').modal('show');
        // }, 1000);
        $(".nav-form-link").click(function () {
            $(".nav-form").toggleClass("main");

        });
        $('.thumb').click(function (event) {
            var img = $('.image-view img');
            var alink = $('.image-view a');
            img.attr('src', $(this).attr('data-src'));
            alink.attr('href', $(this).attr('data-src'));
            $(".image-view iframe").removeClass("main-nav");
            $(".image-view .popup-link").removeClass("main-nav");
        });
        $('.thumb-video').click(function (event) {
            // $('.image-view').append('<div id="img-loader"><i class="fa fa-spinner fa-spin fa-2x"></i></div>');
            $(".image-view iframe").addClass("main-nav");
            $(".image-view .popup-link").addClass("main-nav");

            // img.load(function () {
            //     $('#img-loader').remove();
            // });
        });
        $(".fav").on('click', function () {
            $(this).toggleClass("active");
        });
        $("#fileupload-img").click(function () {
            $("#upload-file-img").click();
        });
        $("#fileupload-img-two").click(function () {
            $("#upload-file-img-two").click();
        });
        $("#fileupload-img-three").click(function () {
            $("#upload-file-img-three").click();
        });
        $("#fileupload-img-four").click(function () {
            $("#upload-file-img-four").click();
        });
        var owl = $('.thumb-images');
        owl.owlCarousel({
            items: 3,
            loop: false,
            margin: 10,
            nav: true,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            autoplaySpeed: 2000,
        });
        var owl = $('.accessories-slider');
        owl.owlCarousel({
            loop: true,
            nav: true,
            margin: 30,
            dots: false,
            center: false,
            autoplay: false,
            autoplayTimeout: 5500,
            autoplayHoverPause: true,
            autoplaySpeed: 2500,
            responsive: {
                0: {
                    items: 1.75,
                    margin: 15
                },
                600: {
                    items: 3
                },
                992: {
                    items: 4
                },
                1200: {
                    items: 5
                },

                1600: {
                    items: 5
                }
            }
        });
        var owl = $('.banner-slider');
        owl.owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            // autoplayHoverPause: true,
            autoplaySpeed: 1500,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 1
                },
                992: {
                    items: 1
                },
                1200: {
                    items: 1
                },

                1600: {
                    items: 1
                }
            }

        });
        var owl = $('.more-products-slider');
        owl.owlCarousel({
            loop: true,
            nav: false,
            margin: 30,
            dots: true,
            center: false,
            autoplay: false,
            autoplayTimeout: 5500,
            autoplayHoverPause: true,
            autoplaySpeed: 2500,
            responsive: {
                0: {
                    items: 1,
                    margin: 15
                },
                600: {
                    items: 2
                },
                992: {
                    items: 2
                },
                1200: {
                    items: 2
                },

                1600: {
                    items: 2
                }
            }
        });
        var owl = $('.products-slider');
        owl.owlCarousel({
            loop: true,
            nav: true,
            margin: 30,
            dots: false,
            autoplay: false,
            autoplayTimeout: 5500,
            autoplayHoverPause: true,
            autoplaySpeed: 2500,
            responsive: {
                0: {
                    items: 2,
                    margin: 15,
                    nav: false
                },
                600: {
                    items: 3
                },
                992: {
                    items: 3
                },
                1200: {
                    items: 4
                },

                1600: {
                    items: 4
                }
            }
        });
        $(".page").click(function (event) {
            $('.navbar-collapse').collapse('hide');
        });
       // $(document).scroll(function (event) {
       //     $('.navbar-collapse').collapse('hide');
       // });
    });


</script>
<script defer>
    //search button
    function live_search() {
        //alert('hhh');
        var query = $('#search').val();
        if (query != '') {
            $.ajax({
                url: '{{URL::to('search')}}',
                type: "GET",
                data: {'search': query},
                success: function (data) {
                    //console.log(data);
                    $('#search-list').html(data);
                }
            })
        }
    }

    $('#search').on('keyup', function () {
        var query = $(this).val();
        if (query != '') {
            $.ajax({
                url: '{{URL::to('search')}}',
                type: "GET",
                data: {'search': query},
                success: function (data) {
                    //console.log(data);
                    $('#search-list').html(data);
                }
            })
        }
    });

    // initiate a click function on each search result
    $(document).on('click', 'ul.list-group li', function () {
        var value = $(this).text();
        console.log('value=' + value);
        if (value != '') {
            $('#search').val(value);
            $('#search-list').html("");
            product_id = $(this).closest('li').find('#pid').val();
            brand = $(this).closest('li').find('#brand').val();

            console.log(product_id);
            url = '/product/'+ brand +'/product-detail/' + product_id;
            $('#search_form').attr('action', url);
            document.forms["search_form"].submit();
        }

    });
</script>
<script>
    $(document).click(function () {
        $("#search-list").html('')
    })

</script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
    var lazyloadImages = document.querySelectorAll("img.lazy");    
    var lazyloadThrottleTimeout;
    
    function lazyload () {
        if(lazyloadThrottleTimeout) {
        clearTimeout(lazyloadThrottleTimeout);
        }    
        
        lazyloadThrottleTimeout = setTimeout(function() {
            var scrollTop = window.pageYOffset;
            lazyloadImages.forEach(function(img) {
                if(img.offsetTop < (window.innerHeight + scrollTop)) {
                img.src = img.dataset.src;
                img.classList.remove('lazy');
                }
            });
            if(lazyloadImages.length == 0) { 
            document.removeEventListener("scroll", lazyload);
            window.removeEventListener("resize", lazyload);
            window.removeEventListener("orientationChange", lazyload);
            }
        }, 20);
    }
    
    document.addEventListener("scroll", lazyload);
    window.addEventListener("resize", lazyload);
    window.addEventListener("orientationChange", lazyload);
    });
</script>
@yield('additionalScripts')
</body>
</html>


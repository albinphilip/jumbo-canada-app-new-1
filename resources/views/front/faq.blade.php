@extends('front.layouts.app')
@section('title','FAQ | Jumbo Canada')
@section('description','Check the Frequently Asked Questions and answers here. I you have any more queries feel free to write to us at support@jumbocanada.com')
@section('keywords','')
@section('content')
    <div class="faq page">
        <div class="container-fluid">
            <h1>Frequently asked questions</h1>
            @foreach($faqs as $faq)
                <h3>{{$faq->question}}</h3>
                <p>{!! $faq->answer !!}</p>
            @endforeach
        </div>
    </div>
@endsection

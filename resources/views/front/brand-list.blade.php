@extends('front.layouts.app')
@if($brand->name == 'Preethi' && $tab == 'all')
    @section('title','Preethi Mixer Grinder | Preethi Home Appliances | Preethi Product')
    @section('description',"Your search for the best mixer grinder online ends here. Preethi Mixer Grinder is the best in India; at Jumbo we give the best at affordable prices.")
    @section('keywords','preethi mixer grinder,preethi wet grinder,preethi products,preethi steele mixer grinder,preethi mixer grinder jar,blue leaf mixer grinder,Preethi products online,online shop for preethi,preethi accessories,preethi mixie accessories')
@elseif($brand->name == 'Preethi' && $tab == 'countertop')
    @section('title','Indian Mixer Grinder | Preethi countertop Mixers')
    @section('description',"Best Indian Mixer Grinders in Canada. Top selling countertop mixers. Preethi authorized dealer. 100% genuine products.")
    @section('keywords','')
@elseif($brand->name == 'Preethi' && $tab == 'parts')
    @section('title','Indian Mixer Parts | Preethi Mixer Accessories')
    @section('description',"Accessories of Best Indian Mixer Grinder in Canada. 100% genuine parts and accessories. Preethi parts and accessories. ")
    @section('keywords','')

@elseif($brand->name == 'Butterfly' && $tab == 'all')
    @section('title','Butterfly Idli Cooker | Stainless Steel Idli Plates')
    @section('description',"Looking for the best quality kitchen appliances for your home? Shop Butterfly's range of appliances from Jumbo at the best price that is ergonomic & built to last.")
    @section('keywords','butterfly idli maker,butterfly stainless steel idli cooker,idli cooker butterfly,kitchen appliances,home appliances,shop kitchenware online,kitchen appliances store,the kitchen store canada,the kitchen shop online')
@elseif($brand->name == 'Butterfly' && $tab == 'kitchenware')
    @section('title','Butterfly Cookware | Indian Kitchenware Canada')
    @section('description',"Butterfly cookware and Kitchenware in Canada. Buy 100% original products from Jumbo Canada.")
    @section('keywords','')

@elseif($brand->name == 'Vidiem' && $tab == 'all')
    @section('title','Mixer Grinder Vidiem | Vidiem Products Online')
    @section('description',"Buy the Latest Collection of Table Top Wet Grinder, and Juicer Mixer Grinders Online from Jumbo. Best Designs at the best price only from Jumbo.")
    @section('keywords','vidiem appliances,vidiem brand,vidiem products,kitchen appliances,online store for kitchen accessories,best kitchen appliances,kitchen appliances near me,kitchen accessories online shopping,best home appliances,home appliances store,kitchen appliances store')
@elseif($brand->name == 'Vidiem' && $tab == 'countertop')
    @section('title','Vidiem Mixer Grinder | Vidiem Countertop Mixers')
    @section('description',"Top Indian Mixer Grinders in Canada. Best selling countertop mixers. Vidiem authorized dealer. 100% genuine products.")
    @section('keywords','')
@elseif($brand->name == 'Vidiem' && $tab == 'parts')
    @section('title','Vidiem Mixer Parts Canada | Vidiem Mixer Accessories')
    @section('description',"Accessories of top Indian Mixer Grinder in Canada. 100% genuine parts and accessories. Vidiem parts and accessories. ")
    @section('keywords','')
    
@elseif($brand->name == 'KENT' && $tab == 'all')
    @section('title','Kent Products | Kent Online Products')
    @section('description',"KENT Offers World's best RO Purifiers, Modern Kitchen Appliances, & Vacuum Cleaners. Shop from Jumbo to get at the best price.")
    @section('keywords','kent products,shop kitchenware online,kitchen appliances store,the kitchen store canada,the kitchen shop online,online shop for appliances,canada kitchen store online,online home appliances store,kent online products,kitchen tools online store,online shop kitchen accessories,kitchen items shopping online')
@elseif($brand->name == 'KENT' && $tab == 'water')
    @section('title','Kent Water Filters | Kent Products in Canada')
    @section('description',"KENT has introduced Alkaline Water Filter Pitcher to provide you and your family with safe, clean, and germ-free water with the right pH levels.")
    @section('keywords','')
@endif
@section('canonical')https://jumbocanada.com/products/{{strtolower($brand->name)}}/{{$tab}}@endsection

@section('content')
    <div class="product-list-page page">
        <div class="banner-common">
        @if($brand->banner_image != NULL)
            <img src="{{asset($brand->banner_image)}}" alt="" class="img-fluid w-100">  
        @else
            <img src="{{asset('assets/img/product-banner.jpeg')}}" alt="" class="img-fluid w-100">
        @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="accordion" id="accordion-four">
                        <!--                        <div class="card">-->
                        <!--                            <div class="card-header" id="collapse-new">-->
                        <!--                                <h5 class="mb-0">-->
                        <!--                                    <button class="btn btn-link collapsed" data-toggle="collapse"-->
                        <!--                                            data-target="#collapse-new" aria-expanded="false"-->
                        <!--                                            aria-controls="collapse-new">-->
                        <!--                                        preethi-->
                        <!--                                    </button>-->
                        <!--                                </h5>-->
                        <!--                            </div>-->
                        <!--                            <div id="collapse-new" class="collapse" aria-labelledby="collapse-new"-->
                        <!--                                 data-parent="#accordion-four">-->
                        <!--                                <div class="card-body">-->
                        <!--                                    <ul>-->
                        <!--                                        <li><a href="" class="active">Mixer Grinder</a></li>-->
                        <!--                                        <li><a href="">Mixer Grinder</a></li>-->
                        <!--                                        <li><a href="">Mixer Grinder</a></li>-->
                        <!--                                    </ul>-->
                        <!---->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                        </div>-->



                <!-- brands -->
                        @foreach($brands as $c_brand)
                            @if($brand == '') @php($name = '')
                            @elseif($brand != null && is_string($brand) ) @php($name = $brand)
                            @else @php($name = $brand->name) @endif
                            @if($name == $c_brand->name) @php($active = 'show') @php($collapsed = '')
                            @else @php($active = '') @php($collapsed = 'collapsed') @endif
                            <div class="card">
                                <div  id="heading{{$c_brand->id}}" class="card-header">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link {{$collapsed}} " data-toggle="collapse"
                                                data-target="#collapse{{$c_brand->id}}" aria-expanded="true"
                                                aria-controls="collapse{{$c_brand->id}}">
                                            {{$c_brand->name}}
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse{{$c_brand->id}}" class="collapse {{$active}}" aria-labelledby="heading{{$c_brand->id}}"
                                     data-parent="#accordion-four">
                                    <div class="card-body">
                                        <ul>

                                            @foreach($categories->sortBy('name') as $category)
                                                @if(\App\Product::getProduct($category->id,$c_brand->id) > 0)
                                                <li><a href="{{route('brand_listproduct',[strtolower($c_brand->name),strtolower(explode(' ',$category->name)[0])])}}"
                                                       @if($tab==strtolower(explode(' ',$category->name)[0]) && $name == $c_brand->name) class="active" @endif>{{$category->name}}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- compare link -->


                @if($cat != 'all' )
                    @if(session()->has('compare-'.strtolower(explode(' ',$cat->name)[0])))

                    <a href="{{route('product.comparison',strtolower(explode(' ',$cat->name)[0]))}}" class="compare-link" style="background-color: #353172 !important">compare
                        {{strtolower(explode(' ',$cat->name)[0])}}</a>
                     @endif
                @else
                    @if(session()->has('compare-'.$tab))

                        <a href="{{route('product.comparison',$tab)}}"  style="background-color: #353172 !important">compare
                            {{$tab}}</a>
                    @endif
                @endif
                <div class="col-sm-8">
                    <div class="products">
                        <div class="row">
                            @if($cat != 'all')
                                @foreach($cat->products->sortByDesc('getVariant.price') as $product)
                                    @if($product->status !='Hidden')
                                    @if($brand !='' && $product->brand->name != $brand->name) @continue; @endif
                                    <div class="col-6 col-xl-4 col-lg-6 col-sm-4 col-md-6">
                                        <div class="product-block">
                                            <div class="pdt-image">
                                                <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                                    <img @if($product->getImage != null)
                                                         src="{{asset($product->getImage->thumbnail_path)}}" @endif alt="" class="img-fluid w-100" id="image{{$product->id}}">
                                                </a>
                                            </div>
                                            <div class="pdt-details">
                                                <p class="brand" id="brand[{{$product->id}}]">{{$product->brand->name}}</p>
                                                <p class="name" id="name[{{$product->id}}]">{{$product->name}}</p>
                                                <p class="price">
                                                    @if($product->offer != null && $product->offer_expire >= today())
                                                        <span class="offer">-{{$product->offer}}%</span><span
                                                        class="old-price">C${{$product->getVariant->price}}</span>
                                                        @php($price = $product->getVariant->price - ($product->getVariant->price*$product->offer/100))
                                                    @else
                                                        @php($price = $product->getVariant->price)
                                                    @endif
                                                    <span id="price[{{$product->id}}]">C${{$price}}</span>
                                                </p>
                                                <p hidden id="variant[{{$product->id}}]">{{$product->getVariant->value}}</p>
                                                <p hidden id="slug[{{$product->id}}]">{{$product->slug}}</p>

                                                @if($product->reviews->count()>0)
                                                    @php($rating = 0)
                                                    @foreach($product->reviews as $reviews)
                                                        @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                                    @endforeach
                                                    <p hidden id="rating[{{$product->id}}]">{{$rating}}</p>
                                                @else
                                                    <p hidden id="rating[{{$product->id}}]">{{0}}</p>
                                                @endif

                                            </div>
                                            <div class="actions">
                                                <a href="#" onclick="addToCart({{$product->id}},'shop')" class="shop">shop now</a>
                                                <a href="#" onclick="addToCart({{ $product->id }},'cart')" class="add">add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            @else
                                @foreach($brand->products->sortByDesc('getVariant.price') as $product)
                                    @if($product->status !='Hidden')
                                        <div class="col-6 col-xl-4 col-lg-6 col-sm-4 col-md-6">
                                            <div class="product-block">
                                                <div class="pdt-image">
                                                    <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                                        <img @if($product->getImage != null)
                                                             src="{{asset($product->getImage->thumbnail_path)}}" @endif alt="" class="img-fluid w-100" id="image{{$product->id}}">
                                                    </a>
                                                </div>
                                                <div class="pdt-details">
                                                    <p class="brand" id="brand[{{$product->id}}]">{{$product->brand->name}}</p>
                                                    <p class="name" name="name[{{$product->id}}]" id="name[{{$product->id}}]">{{$product->name}}</p>
                                                    <p class="price">
                                                        @if($product->offer != null && $product->offer_expire >= today())
                                                            <span class="offer">-{{$product->offer}}%</span><span
                                                                class="old-price">C${{$product->getVariant->price}}</span>
                                                            @php($price = $product->getVariant->price - ($product->getVariant->price*$product->offer/100))
                                                        @else
                                                            @php($price = $product->getVariant->price)
                                                        @endif
                                                        <span id="price[{{$product->id}}]">C${{$price}}</span>
                                                    </p>
                                                    <p hidden id="variant[{{$product->id}}]">{{$product->getVariant->value}}</p>
                                                    <p hidden id="slug[{{$product->id}}]">{{$product->slug}}</p>

                                                    @if($product->reviews->count()>0)
                                                        @php($rating = 0)
                                                        @foreach($product->reviews as $reviews)
                                                            @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                                        @endforeach
                                                        <p hidden id="rating[{{$product->id}}]">{{$rating}}</p>
                                                    @else
                                                        <p hidden id="rating[{{$product->id}}]">{{0}}</p>
                                                    @endif
                                                </div>
                                                <div class="actions">
                                                    <a href="#" onclick="addToCart({{$product->id}},'shop')" class="shop">shop now</a>
                                                    <a href="#" onclick="addToCart({{ $product->id }},'cart')" class="add">add to cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="buy-back">
                <a href="">
                    <img src="{{asset('assets/img/buy-back-exchange.png')}}" alt="" class="img-fluid w-100">
                </a>
            </div>

        </div>

    </div>
@endsection
@section('additionalScripts')
    <script>
        function addToCart(id,tag) {
            $('#msg' + id).html('<i class="fa fa-cog fa-spin"></i> Adding');
            console.log(id);
            variant = null;
            rating = null;
            url = "/add-to-cart";
            var name = document.getElementById('name[' + id + ']').textContent;
            console.log(name);

            var quantity = 1;
            var price = document.getElementById('price[' + id + ']').textContent;
            price = price.slice(2);
            var variant = document.getElementById('variant[' + id + ']').textContent;
            var image = $('#image' + id).attr('src');
            var rating = document.getElementById('rating[' + id + ']').textContent;
            var slug = document.getElementById('slug[' + id + ']').textContent;
            var brand = document.getElementById('brand[' + id + ']').textContent;
            //console.log(rating);
            console.log(slug);
            console.log(image);
            //console.log(variant);

            $.ajax({
                url: url,
                method: 'post',
                data: {
                    product_id: id,
                    name: name,
                    quantity: quantity,
                    price: price,
                    variant: variant,
                    image: image,
                    rating: rating,
                    slug: slug,
                    brand: brand,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        $('#msg' + id).hide();
                        fireTost('Item Added To Cart');
                        console.log(response.cart)
                        $('#c_count span').text(response.cart);
                        if(tag == 'shop')
                            location.href= '/cart';
                    }
                },

            });
        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }

        $('document').ready(function(){

        });
    </script>
@endsection

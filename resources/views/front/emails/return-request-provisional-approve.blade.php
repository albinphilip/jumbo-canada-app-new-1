@extends('front.emails.user-mail')
@section('preText')
    Your request to return {{$return->product_registration->product->name}} has been approved provisionally.
@section('body')
    Hi {{$return->product_registration->first_name}} {{$return->product_registration->last_name}},
    <p>Your request to return {{$return->product_registration->product->name}} has been approved provisionally. We request you to courier the product through Canada post using attached return slip.</p>
    <p>You may contact our customer care <a href="tel:+1 8557733844"> +1 (855) 773 3844</a> for assistance.
    </p>

@endsection

@extends('front.emails.user-mail')
@section('preText')
    Your request to return {{$return->product_registration->product->name}} has been approved provisionally.
@section('body')
    Hi {{$return->product_registration->first_name}} {{$return->product_registration->last_name}},
    <p>Your request to return {{$return->product_registration->product->name}} has been approved provisionally. We
        request you to return the product at {{$return->product_registration->store->address}} and get
        replacement<small>*</small>.</p>
    <p>Contact detail of the store is given below</p>
    <p>Phone: <a
            href="tel:{{$return->product_registration->store->phone}}">{{$return->product_registration->store->phone}}</a>
        <br/>Email: <a
            href="mailto:{{$return->product_registration->store->email}}">{{$return->product_registration->store->email}}</a>
    </p>
    <p>Please note your return request id is <b>{{$return->return_id}}</b></p>
    <p>You may contact our customer care <a href="tel:+1 8557733844"> +1 (855) 773 3844</a> for assistance.
    </p>
    <p><small><i>Please note that product will be replaced only after manual verification at the store.</i></small></p>

@endsection

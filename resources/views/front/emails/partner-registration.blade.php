@extends('front.emails.user-mail')
@section('preText')
    Thank you for showing your interest in becoming a partner of ultrakitchenappliances.com. We are reviewing your details and will get back to you sooner
@endsection
@section('body')
Hi {{$name}},
<p>Thank you for showing your interest in becoming a partner of ultrakitchenappliances.com. We are reviewing your details and will get back to you sooner.  You may feel free to contact us on <a href="tel: +18557733844">+1 (855) 773 3844</a></p>
@endsection

@extends('front.emails.user-mail')
@section('preText')
    We have received your request to avail Exclusive Discount for Students
    and New Immigrants. We are reviewing your details and will get back to you soon.
@endsection
@section('body')
    Hi {{$discount->name}},
    <p>We have received your request to avail Exclusive Discount for Students
        and New Immigrants.</p>
    <p>We are reviewing your details and will get back to you soon.
        In case if you have any further concern please write to <a
            href="mailto: support@jumbocanada.com"></a>support@jumbocanada.com</p>
@endsection

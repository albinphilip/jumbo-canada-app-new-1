@extends('front.emails.user-mail')
@section('preText')
    {{$data->product->name}} - Warranty registration -  Name - {{$data->first_name}} {{$data->last_name}} Serial Number- {{$data->serial_number}} - Product Code- {{$data->product_code}} - Safety Code- {{$data->safety_code}}
@endsection
@section('body')
    Hi,
    <p>You have new product warranty registration request</p>
    Name: {{$data->first_name}} {{$data->last_name}}<br/>
    Product: {{$data->product->name}}<br/>
    Serial Number: {{$data->serial_number}}<br/>
    Product Code: {{$data->product_code}}<br/>
    Safety Code: {{$data->safety_code}}<br/>
    Date of Purchase: {{date('M d, Y',strtotime($data->purchase_date))}}<br/>
    Place of Purchase:{{$data->store->address}}, {{$data->store->city}}<br/>
    Registration Number: {{$data->reg_no}}
@endsection

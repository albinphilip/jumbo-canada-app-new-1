@extends('front.emails.user-mail')
@section('preText')
    Welcome to Jumbocanada.com. Your account has been created. You can view your order details, wish list and saved carts using your registered email-id and password.
@endsection
@section('body')
    <p>Dear {{$name ?? ''}},</p>
    <p>Welcome to Jumbocanada.com </p>
    <p>Preethi, now seen as a giant in the industry, had a humble beginning in 1978. From a small mixer-grinder brand,
        it has grown into a nationally reputed kitchen appliances company. Auto-Cooker/ Warmers, Induction Cook Tops,
        Electric Pressure Cookers and Coffee Makers are just a few among our products to enter the hearts and kitchens
        of many families across the world. And these are the families that inspire us to innovate. Every Preethi
        appliance is efficient, contemporary and easy to use. And it keeps evolving to support and often improve the
        lifestyle of many a people.</p>
    <p>You can view your order details, wish list and saved carts using your registered email-id and password.</p> <a
        href="{{env('APP_URL')}}/customer-login"
        target="_blank"
        style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block; background-color: #E51F25">
        Login Here
    </a>

@endsection

@extends('front.emails.user-mail')
@section('preText')
    New buy back request - {{$data->name}}  Phone: {{$data->phone}}  Email: {{$data->email}} Reason for recycling: {{$data->reason}}
@endsection
@section('body')
    Hi, <br/>
    You have new buy back request<br/>
    Client: {{$data->name}}<br/>
    Phone: {{$data->phone}}<br/>
    Email: {{$data->email}}<br/>
    Reason for recycling: {{$data->reason}}
@endsection

Hi,
You have new product warranty registration request
Name: {{$data->first_name}} {{$data->last_name}}
Product: {{$data->product->name}}
Serial Number: {{$data->serial_number}}
Product Code: {{$data->product_code}}
Safety Code: {{$data->safety_code}}
Date of Purchase: {{$data->purchase_date}}
Place of Purchase:{{$data->store->address}}, {{$data->store->city}}
Registration Number: {{$data->reg_no}}


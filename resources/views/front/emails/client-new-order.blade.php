@extends('front.emails.user-mail')
@section('preText')
    @foreach($orders as $order)
        New order for  {{$order->product->name}} |  Order ID is {{$order->order_id}} | by {{$order->deliveryAddress->firstname}}  {{$order->deliveryAddress->lastname}}
        @break
    @endforeach
@endsection
@section('body')
    @foreach($orders as $order)
        Hi, We have new order,
        <table
            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
            <thead>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"
                    colspan="2">Order Details
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b>Order ID:</b> {{$order->order_id}}<br>
                    <b>Date Added:</b> {{date('d-M-Y',strtotime($order->created_at))}}<br>
                    <b>Payment Method:</b> Credit / Debit Card<br>
                    <b>Shipping Method:</b> Flat Shipping Rate
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b>E-mail:</b>
                    <a href="mailto:ggbranjith@gmail.com" target="_blank">{{$order->customer->email}}</a><br>
                    <b>Telephone:</b> {{$order->customer->telephone}}<br>
                    <b>Order Status:</b> Complete<br>
                </td>
            </tr>
            </tbody>
        </table>
        <table
            style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
            <thead>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Shipping
                    Address
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Payment
                    Address
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    {{$order->deliveryAddress->firstname}}  {{$order->deliveryAddress->lastname}}<br>
                    {{$order->deliveryAddress->address_1}} {{$order->deliveryAddress->address_2}}<br>
                    {{$order->deliveryAddress->city}}<br>
                    {{$order->deliveryAddress->postcode}}<br/>
                    {{$order->deliveryAddress->province}}<br>
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    {{$order->billingAddress->firstname}}  {{$order->billingAddress->lastname}}<br>
                    {{$order->billingAddress->address_1}} {{$order->billingAddress->address_2}}<br>
                    {{$order->billingAddress->city}}<br>
                    {{$order->billingAddress->postcode}}<br/>
                    {{$order->billingAddress->province}}<br>
                </td>
            </tr>
            </tbody>
        </table>
        @break;
    @endforeach
    <table
        style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
        <thead>
        <tr>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                Product
            </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                Model
            </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
                Quantity
            </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
                Price
            </td>
            <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">
                Total
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">{{$order->product->name}}</td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">{{$order->product->model}}</td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$order->quantity}}</td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    C${{number_format($order->product_price,2)}}</td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    C${{number_format($order->product_price*$order->quantity,2)}}
                </td>
            </tr>
        @endforeach
        </tbody>
        @foreach($orders as $order)
            <tfoot>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                    colspan="4"><b>Sub-Total:</b></td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    C${{$order->orderFeesplitUp->sub_total}}
                </td>
            </tr>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                    colspan="4"><b>Flat Shipping Rate:</b></td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    C${{number_format($order->orderFeesplitUp->shipping_charge,2)}}
                </td>
            </tr>
            @if($order->orderFeesplitUp->reduction!=0)
                <tr>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                        colspan="4"><b>Discount :</b></td>
                    <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                        C${{number_format($order->orderFeesplitUp->reduction,2)}}
                    </td>
                </tr>
            @endif
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                    colspan="4"><b>Tax Rate ( {{$order->orderFeesplitUp->tax}}%):</b></td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    C${{number_format(($order->orderFeesplitUp->sub_total+$order->orderFeesplitUp->shipping_charge-$order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100,2)}}
                </td>
            </tr>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"
                    colspan="4"><b>Total:</b></td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    C${{number_format(($order->orderFeesplitUp->sub_total+$order->orderFeesplitUp->shipping_charge-$order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100+$order->orderFeesplitUp->sub_total+$order->orderFeesplitUp->shipping_charge-$order->orderFeesplitUp->reduction,2)}}
                </td>
            </tr>
            </tfoot>
            @break
        @endforeach
    </table>

@endsection

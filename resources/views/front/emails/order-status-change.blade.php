@extends('front.emails.user-mail')
@section('preText')
    Order status update from jumbocanada. Status of your order has been updated
@endsection
@section('body')
    Hi {{$order->customer->firstname ?? ''}},
    <p>{{$msg ?? ''}}</p>
@endsection

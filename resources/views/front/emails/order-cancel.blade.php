@extends('front.emails.user-mail')
@section('preText')
    We have received your request to cancel your order <b>{{$order->product->name}}. Our representative will contact you soon
@endsection
@section('body')
    Hi {{$order->customer->name}},
    <p>We have received your request to cancel your order <b>{{$order->product->name}}</b></p>
    <p>Our representative will contact you soon and in case of emergency please give us a call on <a
            href="tel:+1 8557733844"> +1 (855) 773 3844</a></p>
@endsection

@extends('front.emails.user-mail')
@section('preText')
    Your Buy back request  has been approved. Coupon Code : {{$coupon['code']}} Discount : {{$coupon['value'].'%'}}
    Expiry date :{{$coupon['expires_on']}}
@endsection
@section('body')
    Dear {{$buyback->name ?? ''}}, <br/>
    Your Buy back request  has been approved.<br/>
    Your Discount coupon details are,<br/>
    Coupon Code : {{$coupon['code']}} <br/>
    Discount : {{$coupon['value'].'%'}}<br/>
    Applicable To : {{$coupon['category'].'%'}}<br/>
    Expiry date :{{$coupon['expires_on']}} <br/>
    <br>
    <p>If you have any query, please give us a call on <a
            href="tel:+1 8557733844"> +1 (855) 773 3844</a></p>
@endsection

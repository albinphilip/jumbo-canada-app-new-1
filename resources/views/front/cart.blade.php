@extends('front.layouts.app')
@section('title','Cart | Jumbo Canada')
@section('description','The Jumbo cart allows a shopper to pick out items to buy online. A shopper only has to point and click on a product and check out at the end.')
@section('keywords','canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,kitchen shop online shopping,kitchen supplies online canada,kitchen products,kitchen online shop,,kitchen supplies shop,kitchen supply store canada,online store for kitchen accessories')
@section('canonical','https://jumbokitchenappliances.com/cart')
@section('content')
    @php($customer=Auth::guard('customer')->user())
    <div class="cart page">
        <div class="container-fluid">
            <ul class="list-inline text-center breadcrumb">
                <li class="list-inline-item"><a href="#" style="color: darkblue">CART</a></li>
                <li class="list-inline-item"><a href="#">CHECKOUT</a></li>
                <li class="list-inline-item"><a href="#">PAYMENT</a></li>
            </ul>
            <div class="cart-inner">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="cart-items">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h3>CART ITEMS</h3>

                                </div>
                            </div>
                        </div>
                        @if($carts->isEmpty())
                            <p >Your cart is empty!</p>
                        @else
                            <p id="empty" style="display: none;">Your cart is empty!</p>
                        @foreach($carts as $cart)
                                <div class="cart-item" id="cart_item{{$cart->id}}">
                                    <div class="product-image">
                                        <a href="{{route('productdetail',[strtolower($cart->options['brand']),$cart->options['slug']])}}"><img src="{{asset($cart->options['image'])}}" alt="{{$cart->name}}"
                                                        class="img-fluid"></a>
                                    </div>
                                    <div class="product-details">
                                        <a href="{{route('productdetail',[strtolower($cart->options['brand']),$cart->options['slug']])}}" class="pname">{{$cart->name}}</a>
                                        @if($cart->options['rating'] > 0)
                                            <div class="rating">
                                                <span>{{number_format($cart->options['rating'], 2, '.', ',')}}/5</span>
                                                <ul class="list-inline star">
                                                    <li>
                                                       @php($rating = $cart->options['rating'])
                                                       @foreach(range(1,5) as $i)
                                                           @if($rating>0)
                                                               @if($rating>0.5)
                                                                   <i class="fa fa-star" style="color: goldenrod;"></i>
                                                               @else
                                                                   <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                               @endif
                                                           @else
                                                               <i class="fa  fa-star-o"></i>
                                                           @endif
                                                           @php($rating--)
                                                       @endforeach
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="quantity">
                                            <input type="hidden" id="cart_id{{$cart->id}}" value="{{$cart->rowId}}">
                                            <span>Quantity</span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item">
                                                    <button
                                                        onclick="cart_minus({{$cart->id}})" >
                                                        <img src="{{asset('assets/img/minus-button.png')}}" alt="">
                                                    </button>
                                                </li>
                                                <li class="list-inline-item">
                                                    <input type="text" placeholder="" id="cart_count{{$cart->id}}" value="{{$cart->qty}}" readonly>
                                                </li>
                                                <li class="list-inline-item">
                                                    <button
                                                        onclick="cart_plus({{$cart->id}})">
                                                        <img src="{{asset('assets/img/plus-button.png')}}" alt="">
                                                    </button>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>
                                    <div class="actions">
                                        <button class="delete"
                                                onclick="cart_remove({{$cart->id}})"><img
                                                src="{{asset('assets/img/delete.png')}}" alt="" class="img-fluid">
                                        </button>
                                        <div class="amount">
                                            <p class="price">Price</p>
                                            @php($price = $cart->price*$cart->qty)
                                            <h5 id="cart_price{{$cart->id}}">C${{$price}}</h5>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="secure-payment">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="{{asset('assets/img/secure.png')}}" alt="" class="img-fluid secure">
                                    <h4>100% <br><span>secure payment</span></h4>
                                </div>
                                <div class="col-sm-8">
                                    <img src="{{asset('assets/img/pay.png')}}" alt="" class="img-fluid pay">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h3>Amount</h3>
                        <div class="cart-items-pay">
                            <table>
                                <tbody>
                                <tr>
                                    <td>TOTAL</td>
                                    <td id="cart_total">C${{\Cart::total()}}</td>
                                </tr>
                                @php(session()->put('total', \Cart::total()))
                                </tbody>
                            </table>
                        </div>
                        @if(!$carts->isEmpty())
                            <a href="{{route('shipping')}}" id="check-btn" class="proceed">Proceed to checkout</a>
                            <div class="clearfix"></div>

                            @if(Session::has('msg'))
                                <p style="color:red;"> {{ Session::get('msg') }}</p>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        function cart_minus(id){
            rowId = $('#cart_id'+id).val();
            url = '/decrease-quantity/'+rowId;
            $.ajax({
                url: url,
                method: 'get',

                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        //$('#msg').hide();
                        fireTost('Quantity Decreased');
                        $('#cart_count'+id).val(response.item['qty']);
                        console.log(response.count)
                        $('#c_count span').text(response.count);
                        price = parseInt(response.item['price'])*response.item['qty'];
                        $('#cart_price'+id).text('C$'+price);
                        $('#cart_total').text('C$'+response.total);
                        if(response.item['qty']== 0)
                            $('#cart_item'+id).remove();
                        if(response.total == 0) {
                            $('#empty').css('display', 'block');
                            $('#check-btn').css('display', 'none');

                        }
                    }
                },

            });
        }
        function cart_plus(id){
            rowId = $('#cart_id'+id).val();
            qty =  parseInt($('#cart_count'+id).val()) + 1;
            url = '/increase-quantity/'+rowId
            $.ajax({
                url: url,
                method: 'get',

                success: function (response) {
                    // console.log(response)
                    if (response.success == 'success') {
                        //$('#msg').hide();
                        fireTost('Quantity Increased');
                        $('#cart_count'+id).val(response.item['qty']);
                        console.log(response.item['qty'])
                        $('#c_count span').text(response.count);
                        price = parseInt(response.item['price'])*response.item['qty'];
                        $('#cart_price'+id).text('C$'+price);
                        $('#cart_total').text('C$'+response.total);

                    }
                },

            });
        }

        function cart_remove(id){
            rowId = $('#cart_id'+id).val();
            url = '/delete-from-cart/'+rowId;
            $.ajax({
                url: url,
                method: 'get',

                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        //$('#msg').hide();
                        $('#cart_item'+id).remove();
                        fireTost('Item Deleted From Cart');
                        console.log(response.count)
                        $('#c_count span').text(response.count);
                        $('#cart_total').text('C$'+response.total);
                        if(response.total == 0){
                            $('#check-btn').css('display','none');
                            $('#empty').css('display','block');
                            $('#c_count span').text(0);
                        }

                    }
                },

            });
        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
    </script>
@endsection




@extends('front.layouts.app')
@section('title','Jumbo Canada| Return')
@section('description','')
@section('keywords','')
@section('content')
    <div class="register page">
        <div class="register-inner">
            <h1>Product return status update</h1>
            <form action="{{route('returnStatusUpdate.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" for="reg_num">RMA Number <small>*</small></label>
                        <input type="text" class="form-control" required placeholder="RMA"
                               name="reg_num"
                               value="{{request('rma')}}" required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="sec_code">Security Code<small>*</small></label>
                        <input type="text" class="form-control" required placeholder="Security Code"
                               name="sec_code"
                               value="{{request('sec')}}" required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="status">Status<small>*</small></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">SELECT</option>
                            <option>Replaced</option>
                            <option>Rejected</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="comment">Comments<small>*</small></label>
                        <textarea class="form-control" id="comment" required placeholder="" name="comment"></textarea>
                    </div>
                    <div class="col-12">
                        <input type="submit" value="submit">
                    </div>
                </div>
            </form>
            <div class="col-12">
                <div class="response" id="pleaseWait" style="display: none;">
                    Please wait <i class="fa fa-cog fa-spin"></i>
                </div>
                <div id="subscribed"></div>
            </div>
        </div>
    </div>
@endsection


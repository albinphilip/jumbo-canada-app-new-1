@extends('front.layouts.app')
@section('title','Jumbo Canada | Order Details')
@section('description','')
@section('keywords','')
@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <h1>Order Details</h1>
            <div class="row">
                <div class=" col-12 profile-inner">
                    @isset($orders)
                    @if($orders->isEmpty())
                        No items in order history!
                    @else
                        @if(Session::has('success'))
                            <p style="color:red;">{{Session::get('success')}}</p>
                            @php(session()->forget('success'))
                        @endif
                        <table class="d-none d-sm-block">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Product Details</th>
                                <th>Price Details</th>
                                <th>Order Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders->sortByDesc('id') as $order)
                                <tr>
                                    <td> @if($order->product->getImage != null)
                                            <a href = "{{route('productdetail',[strtolower($order->product->brand->name),$order->product->slug])}}">
                                                <img src="{{asset($order->product->getImage->thumbnail_path)}}" id="image" alt=""
                                                     class="img-fluid product-image">
                                            </a>@endif
                                    </td>
                                    <td>
                                        <h4 id="name">{{$order->product->name}}</h4>
                                        @if($order->product->reviews->count()>0)
                                            <div class="rating">
                                                @php($rating = 0)
                                                @foreach($order->product->reviews as $reviews)
                                                    @php($rating = $rating+$reviews->rating / Count($order->product->reviews))
                                                @endforeach
                                                <span>{{number_format($rating, 2, '.', ',')}}/5</span>
                                                <ul class="list-inline star">Vidiem
                                                    @foreach(range(1,5) as $i)
                                                        @if($rating >0)
                                                            @if($rating >0.5)
                                                                <i class="fa fa-star" style="color: goldenrod;"></i>
                                                            @else
                                                                <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                            @endif
                                                        @else
                                                            <i class="fa  fa-star-o"></i>
                                                        @endif
                                                        @php($rating--)
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <p>Date of Order : {{date_format($order->created_at,'d-M-Y')}}</p>
                                        @if($order->delivery_date != '')
                                            <p>Estimated Delivery Date: {{date('d-M-Y',strtotime($order->delivery_date))}}</p>
                                        @endif
                                    </td>
                                    <td>
                                        C${{$order->order_total}}
                                    </td>
                                    <td>
                                        @if($order->status == 'Delivered')
                                            <div class="status-delivered">Delivered</div>
                                        @else
                                            <div class="status not-delivered">{{$order->status}}</div>
                                        @endif
                                            <div class="row pl-3 pt-5" style="display: inline">
                                                @php($id = base64_encode($order->order_id))
                                                <a href="{{route('order-confirmation',$id)}}" target="_blank" class="btn btn-primary btn-sm" style="background-color: red;"><i class="fa fa-eye"></i></a>
                                            @if($order->shipping_partner == 'Canada Post')
                                                    <a href="" class="btn btn-primary btn-sm " style="background-color: red;" data-pin="{{$order->tracking_pin}}" data-toggle="modal" data-target="#tracking-modal">Track </a>
                                                @endif

                                            </div>
                                    </td>
                                    <td>
                                        <ul class="list actions">
                                            @if($order->status=='Order placed')
                                                <li class="list-inline-item pb-2">
                                                    <small><a href="{{route('returnProduct')}}"
                                                              onclick="return confirm('Are you sure?')"
                                                              class="add">Cancel</a></small>
                                                </li>
                                            @endif
                                            @if($order->status == 'Delivered')
                                                        <li class="list-inline-item pb-2">
                                                        <small><a href="{{route('returnReason',$order->id)}}"
                                                              class="add">Return</a></small>
                                                        </li>
                                            @endif
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                @endisset
                </div>
            </div>
        </div>
    </div>
    <!-- tracking modal -->
    <div class="modal fade" id="tracking-modal" tabindex="-1" role="dialog"
         aria-labelledby="tracking-modal"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!--                <h5 class="modal-title">Add Addresses</h5>-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <h4>shipped with delivery</h4>
                        <h5>Tracking ID: <span id="track_id"></span></h5>
                        <div class="day">
                            <h6>friday 5, feb</h6>
                            <ul class="time-location">
                                <li>
                                    <div class="time">2:00 am</div>
                                    <div class="location">
                                        <h6>delivered</h6>
                                        <span>kannur kerala in</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">2:00 am</div>
                                    <div class="location">
                                        <h6>delivered</h6>
                                        <span>kannur kerala in</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">2:00 am</div>
                                    <div class="location">
                                        <h6>delivered</h6>
                                        <span>kannur kerala in</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">2:00 am</div>
                                    <div class="location">
                                        <h6>delivered</h6>
                                        <span>kannur kerala in</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">2:00 am</div>
                                    <div class="location">
                                        <h6>delivered</h6>
                                        <span>kannur kerala in</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">2:00 am</div>
                                    <div class="location">
                                        <h6>delivered</h6>
                                        <span>kannur kerala in</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "OrderDetail" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });


        window.onload=function(){
            if (($('#email').val() !== '')&&($('#order_id').val() !== '') ){
                window.setTimeout(function() { document.orderDetails.submit(); }, 2000);
            }
        };
    </script>
    <script>
        $(document).ready(function(){
            $('#tracking-modal').on("show.bs.modal", function (e) {

                id = $(e.relatedTarget).data('pin');
                $("#track_id").text($(e.relatedTarget).data('pin'));
                url = '/front/order/tracking/'+id;
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"get",id:id},
                    success: function (result) {
                        if(result.msg == 'success')
                        {
                            console.log(result);

                        }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to get tracking details");
                    }
                });

            });

        });
    </script>
@endsection

@extends('front.layouts.profile')
@section('title','Jumbo Canada | My Wish list')
@section('description','')
@section('keywords','')
@section('profileHead','My Wish Lists')
@section('profileContent')
    @if($lists->isEmpty())
        Your Favourite List is Empty
    @else
        <table class="d-none d-sm-block">
            <thead>
            <tr>
                <th>Product</th>
                <th>Product Details</th>

                <th>Stock</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($lists->sortByDesc('id') as $item)
                @php($product = $item->product)
                <tr>
                    <td> @if($product->getImage != null)
                            <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                <img src="{{asset($product->getImage->image_path)}}"
                                     id="image{{$product->id}}" alt="" class="img-fluid product-image">
                            </a>@endif
                    </td>
                    <td>
                        <h4 id="name[{{$product->id}}]">{{$product->name}}</h4>
                        @if($product->reviews->count()>0)
                            <div class="rating">
                                @php($rating = 0)
                                @foreach($product->reviews as $reviews)
                                    @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                @endforeach
                                <p hidden id="rating[{{$product->id}}]">{{$rating}}</p>
                                <span>{{number_format($rating, 2, '.', ',')}}/5</span>
                                <ul class="list-inline star">
                                    <li>
                                        @foreach(range(1,5) as $i)
                                            @if($rating>0)
                                                @if($rating>0.5)
                                                    <i class="fa fa-star" style="color: goldenrod;"></i>
                                                @else
                                                    <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                                @endif
                                            @else
                                                <i class="fa  fa-star-o"></i>
                                            @endif
                                            @php($rating--)
                                        @endforeach
                                    </li>
                                </ul>
                            </div>
                        @endif
                        <p hidden id="rating[{{$product->id}}]">{{0}}</p>
                    </td>
                    @if($product->getVariant->quantity > 0 )
                        <td>In Stock</td>
                    @else
                        <td>Pre Booking</td>
                    @endif
                    @if($product->offer!=null && $product->offer_expire >= today())
                        @php($price = $product->getVariant->price-
                                      ($product->getVariant->price*$product->offer)/100)
                    @else
                        @php($price = $product->getVariant->price)
                    @endif
                    <td id="price[{{$product->id}}]">C${{$price}}</td>
                    <p hidden id="variant[{{$product->id}}]">{{$product->getVariant->value}}</p>
                    <p hidden id="slug[{{$product->id}}]">{{$product->slug}}</p>
                    <p hidden id="brand[{{$product->id}}]">{{$product->brand->name}}</p>

                    <td>
                        <ul class="list-inline actions">
                            <li class="list-inline-item"><a href="{{route('wishlist.destroy',$item->id)}}"
                                                            onclick="return confirm('Are you sure?')" class="remove"><i
                                        class="fa fa-heart"
                                        aria-hidden="true" style="color: #a00000"></i></a>
                            </li>
                            @if($product->status != 'Hidden')
                                <li class="list-inline-item"><a href="#" onclick="addToCart({{$product->id}})"
                                                                class="add">add</a>
                                    <span id="msg{{$product->id}}" style="color:red;"></span>
                                </li>
                            @endif
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
@section('additionalScripts')
    <script>
        function addToCart(id) {
            $('#msg' + id).html('<i class="fa fa-cog fa-spin"></i> Adding');
            console.log(id);
            variant = null;
            rating = null;
            url = "/add-to-cart";
            var name = document.getElementById('name[' + id + ']').textContent;
            console.log(name);

            var quantity = 1;
            var price = document.getElementById('price[' + id + ']').textContent;
            price = price.slice(2);
            var variant = document.getElementById('variant[' + id + ']').textContent;
            var image = $('#image' + id).attr('src');
            var rating = document.getElementById('rating[' + id + ']').textContent;
            var slug = document.getElementById('slug[' + id + ']').textContent;
            var brand = document.getElementById('brand[' + id + ']').textContent;
            //console.log(rating);
            console.log(slug);
            console.log(image);
            //console.log(variant);

            $.ajax({
                url: url,
                method: 'post',
                data: {
                    product_id: id,
                    name: name,
                    quantity: quantity,
                    price: price,
                    variant: variant,
                    image: image,
                    rating: rating,
                    slug: slug,
                    brand: brand,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        $('#msg' + id).hide();
                        fireTost('Item Added To Cart');
                        console.log(response.cart)
                        $('#c_count span').text(response.cart);
                    }
                },

            });
        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
    </script>
@endsection

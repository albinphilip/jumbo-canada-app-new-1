@extends('front.layouts.profile')
@section('subtitle','| Edit Address')
@section('description','')
@section('keywords','')
@section('profileHead','Edit Addresses')
@section('profileContent')
    @php($customer=Auth::guard('customer')->user())

    <div class="modal-body">
        <div class="body-inner">
            <form action="{{route('update-address',$address->id)}}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-sm-6">
                        <label>First Name</label>
                        <input type="text" placeholder="First Name" name="firstname" required class="form-control"
                               value="{{$address->firstname}}">
                    </div>
                    <div class="col-sm-6">
                        <label>Last Name</label>
                        <input type="text" placeholder="Last Name" name="lastname" required class="form-control"
                               value="{{$address->lastname}}">
                    </div>
                </div>
                <label>Company</label>
                <input type="text" placeholder="Company" name="company"  class="form-control"
                       value="{{$address->company}}">
                <label>Address line 1</label>
                <input type="text" placeholder="Address Line 1" name="address_1"  class="form-control"
                       value="{{$address->address_1}}" required>
                <label>Address Line 2</label>
                <input type="text" placeholder="Address Line 2" name="address_2" class="form-control"
                       value="{{$address->address_2}}">
                <div class="form-group">
                    <select name="province" class="form-control" id="province" required>
                        <option value="Nunavut" {{old('province',$address->province) == 'Nunavut' ? 'selected' : ''}}>Nunavut</option>
                        <option value="Quebec" {{old('province',$address->province) == 'Quebec' ? 'selected' : ''}}>Quebec</option>
                        <option value="Northwest Territories" {{old('province',$address->province) == 'Northwest Territories' ? 'selected' : ''}}>Northwest Territories</option>
                        <option value="Ontario" {{old('province',$address->province) == 'Ontario' ? 'selected' : ''}}>Ontario</option>
                        <option value="British Columbia" {{old('province',$address->province) == 'British Columbia' ? 'selected' : ''}}>British Columbia</option>
                        <option value="Alberta" {{old('province',$address->province) == 'Alberta' ? 'selected' : ''}}>Alberta</option>
                        <option value="Saskatchewan" {{old('province',$address->province) == 'Saskatchewan' ? 'selected' : ''}}>Saskatchewan</option>
                        <option value="Manitoba" {{old('province',$address->province) == 'Manitoba' ? 'selected' : ''}}>Manitoba</option>
                        <option value="Yukon" {{old('province',$address->province) == 'Yukon' ? 'selected' : ''}}>Yukon</option>
                        <option value="Newfoundland and Labrador" {{old('province',$address->province) == 'Newfoundland and Labrador' ? 'selected' : ''}}>Newfoundland and Labrador</option>
                        <option value="New Brunswick" {{old('province',$address->province) == 'New Brunswick' ? 'selected' : ''}}>New Brunswick</option>
                        <option value="Nova Scotia" {{old('province',$address->province) == 'Nova Scotia' ? 'selected' : ''}}>Nova Scotia</option>
                        <option value="Prince Edward Island" {{old('province',$address->province) == 'Prince Edward Island' ? 'selected' : ''}}>Prince Edward Island</option>

                    </select>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label>City</label>
                        <input type="text" placeholder="City" name="city" class="form-control"
                               value="{{$address->city}}">
                    </div>
                    <div class="col-sm-6">
                        <label>Zip Code</label>
                        <input type="text" placeholder="Pin" name="postcode" class="form-control"
                               value="{{$address->postcode}}">
                    </div>
                </div>
                <div class="pretty p-default p-curve">
                    <input type="checkbox" name="custom_field" value="yes" {{($address->custom_field == 'yes') ? 'checked' :''}} >
                    <div class="state">
                        <label>Make this my default address</label>
                    </div>
                </div>
                <div class="actions">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <input type="submit" class="form-control" value="Update">
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>

@endsection

@extends('front.layouts.profile')
@section('subtitle','| Edit Profile')
@section('description','')
@section('keywords','')
@section('profileHead','My Profile')
@section('profileContent')
    @php($customer=Auth::guard('customer')->user())
        <form action="{{route('save-profile')}}" method="POST">
            @csrf
            <h3>Your Personal Details</h3>
            <div class="row">
                <div class="col-sm-6"><input type="text" placeholder="First Name" name="firstname" required
                                             class="form-control" value="{{$customer->firstname}}"></div>
                <div class="col-sm-6"><input type="text" placeholder="last Name" name="lastname" required
                                             class="form-control" value="{{$customer->lastname}}"></div>
                <div class="col-sm-6"><input type="email" placeholder="Email" name="email" required
                                             class="form-control" value="{{$customer->email}}" readonly></div>
                <div class="col-sm-6">
                    <input type="tel" placeholder="Phone Number" data-inputmask='"mask": "(999) 999-9999"' data-mask value="{{$customer->telephone}}"
                           name="telephone" required class="form-control">
                </div>
            </div>
            <input type="submit" value="update">
        </form>

@endsection
@section('additionalScripts')
    <!-- InputMask -->
    <script src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
@endsection
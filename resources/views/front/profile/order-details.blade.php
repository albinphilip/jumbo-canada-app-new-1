@extends('front.layouts.profile')
@section('title','Jumbo Canada | Order Details')
@section('description','')
@section('keywords','')
@section('profileHead','My Order Details')
@section('profileContent')
    @if($orders->isEmpty())
        No items in order history!
    @else
        <table class="d-none d-sm-block">
            <thead>
            <tr>
                <th>Product</th>
                <th>Product Details</th>
                <th>Price Details</th>
                <th>Order Status</th>
                <th>Actions</th>
           </tr>
            </thead>
            <tbody>
            @foreach($orders->sortByDesc('id') as $order)
                <tr>
                    <td> @if($order->product->getImage != null)
                          <a href = "{{route('productdetail',[strtolower($order->product->brand->name),$order->product->slug])}}">
                            <img src="{{asset($order->product->getImage->thumbnail_path)}}" id="{{$order->product->getImage->thumbnail_path}}" alt=""
                                 class="img-fluid product-image">
                          </a>
                        @endif
                    </td>
                    <td>
                        <h4 id="{{$order->product->slug}}">{{$order->product->name}}</h4>
                        @if($order->product->reviews->count()>0)
                            <div class="rating">
                                @php($rating = 0)
                                @foreach($order->product->reviews as $reviews)
                                    @php($rating = $rating+$reviews->rating / Count($order->product->reviews))
                                @endforeach
                                <span>{{number_format($rating, 2, '.', ',')}}/5</span>
                                <ul class="list-inline star">
                                    <li>
                                    @foreach(range(1,5) as $i)
                                        @if($rating >0)
                                            @if($rating >0.5)
                                                <i class="fa fa-star" style="color: goldenrod;"></i>
                                            @else
                                                <i class="fa fa-star-half" style="color: goldenrod;"></i>
                                            @endif
                                        @else
                                            <i class="fa  fa-star-o"></i>
                                        @endif
                                        @php($rating--)
                                    @endforeach
                                    </li>
                                </ul>
                            </div>
                        @endif
                        <p>Date of Order : {{date_format($order->created_at,'d-M-Y')}}</p>
                        @if($order->delivery_date != '')
                            <p>Estimated Delivery Date : {{date('d-M-Y',strtotime($order->delivery_date))}}</p>
                        @endif
                    </td>
                    <td>
                         C${{$order->order_total}}
                    </td>
                    <td>
                        @if($order->status == 'Delivered')
                            <div class="status-delivered">Delivered</div>
                        @else
                            <div class="status not-delivered">{{$order->status}}</div>
                        @endif
                        <div class="row pl-3" >
                            @php($id = base64_encode($order->order_id))
                            <span class="p-1"><a href="{{route('order-confirmation',$id)}}" target="_blank" class="btn btn-primary btn-sm" style="background-color: darkblue;"><i class="fa fa-eye"></i></a></span>
                            @for($n = 0; $n < $order->quantity; $n++ )
                                @if($order->shipping_partner == 'Canada Post')
                                    @php($track_array = explode(',',$order->tracking_pin))
                                <span class="p-1">
                                    <a href="" id="track{{$order->id}}" class="btn btn-primary btn-sm" style="background-color: darkblue;"
                                       onclick="return track({{$track_array[$n]}});" type="button">Track({{$n+1}})
                                    </a>
                                </span>

                                @endif
                            @endfor
                        </div>


                    </td>
                    <td>
                        <ul class="list-inline actions">
                            <li class="list-inline-item p-2">
                                @if(($order->status =='Order placed'))
                                    <small><a href="{{route('orderCancel',$order->id)}}"
                                              onclick="return confirm('Are you sure?')"
                                              class="add">Cancel</a></small>

                                @elseif($order->status == 'Delivered')
                                    <small><a href="{{route('returnProduct')}}"
                                                                        class="add">Return</a></small>
                                @endif
                            </li>
                            <li class="list-inline-item p-2">
                                <small><a href="{{route('review.index',$order->product_id)}}"
                                          class="add">Review</a></small>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    <!-- tracking modal -->
    <div class="modal" id="tracking-modal" tabindex="-1" role="dialog"
         aria-labelledby="tracking-modal"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!--                <h5 class="modal-title">Add Addresses</h5>-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <h4>shipped with Canada Post</h4>
                        <h5>Tracking ID: <span id="track_id"></span></h5>
                        <div class="day">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
            function track(pin) {
                //id = $(e.relatedTarget).data('pin');
                $("#track_id").text(pin);
                url = '/front/order/tracking/'+pin;
                //alert('hh');
                $.ajax({
                    type: "GET",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"get",id:pin},
                    success: function (result) {
                        if(result.msg == 'success') {
                            console.log(result.datas);
                            if(!result.datas[0])
                                $('.day').html('<p> Tracking History Not Available</p>');
                            else {
                                $('.day').html('');
                                for (i = 0; i < result.datas.length; i++) {
                                    c_date =  result.datas[i]["date"][0];
                                    d_date = c_date.split('-');
                                    new_date = new Date(d_date[0],d_date[1]-1,d_date[2]);
                                    $('.day').append('<h6>' +new_date.toDateString() + '</h6>' +
                                        '<ul class="time-location">' +
                                        '<li>' +
                                        '<div class="time">' + result.datas[i]["time"][0] + '</div>' +
                                        '<div class="location">' +
                                        '<h6>' + result.datas[i]["description"][0] + '</h6>' +
                                        '<span>' + result.datas[i]["site"][0] + '</span>' +
                                        '</div>' +
                                        '</li>' +
                                        '</ul>');
                                }
                            }
                            $('#tracking-modal').modal('show');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to get tracking details");
                    }
                });
                return false;
            }
    </script>
@endsection

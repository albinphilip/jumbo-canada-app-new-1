@extends('front.layouts.app')
@section('title','Warranty Registration | Jumbo Canada')
@section('description','Register here to collect product information and claim the warranty coverage that will keep your product up to date.')
@section('keywords','warrenty,Warranty Registration,kitchen supplies online canada,kitchen products,kitchen online shop,kitchen supplies shop,kitchen accessories online shopping,kitchen accessories store')
@section('canonical','https://jumbokitchenappliances.com/warranty-registration')
@section('content')
    <?php
    $province = '';
    $customer = Auth::guard('customer')->user();
    if (Auth::guard('customer')->user())
        if ($customer->getAddress)
            $province = $customer->getAddress->province;
    ?>
    <div class="register page">
        <div class="register-inner">
            <h1>Register your product</h1>
            <form action="{{route('product-registration.store')}}" method="post"
                  id="product-registration-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" for="product_id">Product <small>*</small></label>
                        <select name="product_id"  class="form-control" id="product_id" required>
                            <option value="">Select Product</option>
                            @foreach($products as $product)
                                <option value="{{$product->id}}"
                                        @if(old('product_id') == $product->id) selected @endif >{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="product_code">Model Number <small>*</small></label>
                        <div class="serial">
                            <input type="text" class="form-control"  pattern=".{5,10}" placeholder="MG298"
                                   name="product_code" value="{{ old('product_code') }}" required = 'required' id="product_code">
                            <img src="{{asset('assets/img/serial-number.png')}}" alt="latest Ultra kitchen appliances" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="serial_number">Serial Number <small>*</small></label>
                        <div class="serial">
                            <input type="text" class="form-control"  pattern=".{10,16}" placeholder="2005461413"
                                   name="serial_number" value="{{ old('serial_number') }}" required = "required" id="serial_number">

                            <img src="{{asset('assets/img/serial-number.png')}}" alt="Product support- Ultra kitchen appliances" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="safety_code">Safety Code <small>*</small></label>
                        <div class="serial">
                            <input type="text" class="form-control" required pattern="(PT21ULT.[0-9]{2,}|PT20ULT.[0-9]{3,4}|PTMG21.[0-9]{2,})"
                                   placeholder="PT20ULT9978" id="safety_code"
                                   name="safety_code" value="{{ old('safety_code') }}" >

                            <img src="{{asset('assets/img/serial-number.png')}}" alt="kitchen appliances product support- Ultra kitchen appliances" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label class="control-label" for="purchase_date">Date of Purchase <small>*</small></label>
                        <input type="date" min="{{date("Y-m-d",strtotime("-4 year"))}}" max="{{date("Y-m-d")}}"
                               class="form-control"   name="purchase_date" id="purchase_date"
                               value="{{ old('purchase_date') }}" required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="store_id">Place Of Purchase <small>*</small></label>
                        <select class="form-control" id="store_id" name="store_id" required>
                            <option value="">Select</option>
                            @foreach($stores as $store)
                                <option value="{{$store->id}}">{{$store->address}}, {{$store->city}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="first_name">First Name <small>*</small></label>
                        <input type="text" class="form-control"  placeholder="First Name" name="first_name" id="first_name"
                               value="@if(Auth::guard('customer')->check()){{$customer->firstname}} @else{{ old('first_name') }}@endif"
                               required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="last_name">Last Name</label>
                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name"
                               value="@if(Auth::guard('customer')->check()){{$customer->lastname}} @else{{ old('last_name') }}@endif">
                    </div>

                    <div class="col-sm-6">
                        <label class="control-label" for="address">Address</label>
                        <input type="text" class="form-control" placeholder="Address" name="address" id="address"
                               value="@if(Auth::guard('customer')->check())@if($customer->getAddress) {{$customer->getAddress->address_1}}@endif @else{{ old('address') }}@endif" required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="city">City</label>
                        <input type="text" class="form-control" placeholder="City" name="city" id="city"
                               value="@if(Auth::guard('customer')->check())@if($customer->getAddress){{$customer->getAddress->city}}@endif @else{{ old('city') }}@endif">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="provenance">Province</label>
                        <select class="form-control" id="provenance" name="provenance">
                            <option value="">Select Province</option>
                            <option value="Nunavut"
                                    @if($province=='Nunavut') selected
                                    @elseif(old('provenance') == 'Nunavut') selected @endif>
                                Nunavut
                            </option>
                            <option value="Quebec"
                                    @if($province=='Quebec') selected
                                    @elseif(old('provenance') == 'Quebec') selected @endif>
                                Quebec
                            </option>
                            <option value="Northwest Territories"
                                    @if($province=='Northwest Territories') selected
                                    @elseif(old('provenance') == 'Northwest Territories') selected @endif>
                                Northwest
                                Territories
                            </option>
                            <option value="Ontario"
                                    @if($province=='Ontario') selected
                                    @elseif(old('provenance') == 'Ontario') selected @endif>
                                Ontario
                            </option>
                            <option value="British Columbia"
                                    @if($province=='British Columbia') selected
                                    @elseif(old('British Columbia') == 'British Columbia') selected @endif>
                                British Columbia
                            </option>
                            <option value="Alberta"
                                    @if($province=='Alberta') selected
                                    @elseif(old('provenance') == 'Alberta') selected @endif>
                                Alberta
                            </option>
                            <option value="Saskatchewan"
                                    @if($province=='Saskatchewan') selected
                                    @elseif(old('provenance') == 'Saskatchewan') selected @endif>
                                Saskatchewan
                            </option>
                            <option value="Manitoba"
                                    @if($province=='Manitoba') selected
                                    @elseif(old('provenance') == 'Manitoba') selected @endif>
                                Manitoba
                            </option>
                            <option value="Yukon" @if($province=='Yukon') selected
                                    @elseif(old('provenance') == 'Yukon') selected @endif>Yukon
                            </option>
                            <option value="Newfoundland and Labrador"
                                    @if($province == 'Newfoundland and Labrador') selected
                                    @elseif(old('provenance') == 'Newfoundland and Labrador') selected @endif>
                                Newfoundland
                                and Labrador
                            </option>
                            <option value="New Brunswick"
                                    @if($province=='New Brunswick') selected
                                    @elseif(old('provenance') == 'New Brunswick') selected @endif>
                                New
                                Brunswick
                            </option>
                            <option value="Nova Scotia"
                                    @if($province == 'Nova Scotia') selected
                                    @elseif(old('provenance') == 'Nova Scotia') selected @endif>
                                Nova
                                Scotia
                            </option>
                            <option value="Prince Edward Island"
                                    @if($province=='Prince Edward Island') selected
                                    @elseif(old('provenance') == 'Prince Edward Island') selected @endif>
                                Prince Edward
                                Island
                            </option>
                        </select>
                    </div>

                    <div class="col-sm-6">
                        <label class="control-label" for="zipcode">Zipcode</label>
                        <input type="text" class="form-control" placeholder="Zipcode" name="zipcode"  id="zipcode"
                               value="@if(Auth::guard('customer')->check())@if($customer->getAddress){{$customer->getAddress->postcode}}@endif @else{{ old('zipcode') }}@endif" required>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="phone">Phone Number </label>
                        <input type="text" class="form-control" required  data-inputmask='"mask": "(999) 999-9999"' data-mask
                               placeholder="Contact Number" name="phone" id="phone"
                               value="@if(Auth::guard('customer')->check()){{$customer->telephone}}@else{{ old('phone') }}@endif">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="email">Email Address <small>*</small></label>
                        <input type="email" class="form-control" required placeholder="Email Address" name="email" id="email"
                               value="@if(Auth::guard('customer')->check()){{$customer->email}} @else{{ old('email') }}@endif">
                    </div>
                    <div class="col-sm-6">
                        <div class="upload">
                            <label class="control-label" for="upload-file-img">Attach Your Invoice *</label>
                            <input required name="attached_file" class="form-control" id="upload-file-img" type="file"
                                   accept="image/png, image/jpeg, application/pdf">
                            <a id="fileupload-img"><span>Attach Your Invoice</span><img
                                    src="{{asset('assets/img/attach.svg')}}"
                                    alt="upload"></a>

                        </div>
                    </div>
                    <div class="col-sm-6" style="margin-top: 20px">
                        <input type="checkbox" style="height: 20px; width: 10%" required="" id="agree"> <label
                            for="agree">I Agree
                            <a target="_blank"
                               href="{{route('policy','terms-and-conditions-of-warranty-and-safe-appliances-usage')}}">
                                Terms and Conditions</a></label>
                    </div>
                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    <div class="col-12">
                        <input type="submit" value="submit">
                    </div>
                </div>
            </form>
            <div class="col-12">
                <div class="response" id="pleaseWait" style="display: none;">
                    Please wait <i class="fa fa-cog fa-spin"></i>
                </div>
                <div id="subscribed"></div>
            </div>
        </div>
    </div>

@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "productRegistration"}).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
    <!-- InputMask -->
    <script  src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
    <script  type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}&sensor=false&libraries=places"></script>
    <script  type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var options = {
                componentRestrictions: {country: ['CA']},
            };
            console.log('here');
            var places = new google.maps.places.Autocomplete(document.getElementById('address'), options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var add=address.split(",");
                console.log(add[0],add[1],add[2]);
                var province=add[2].substr(0,3);
                console.log(province);
                province_array = {'NL':'Newfoundland and Labrador','PE':'Prince Edward Island',
                    'NS':'Nova Scotia','NB':'New Brunswick','QC':'Quebec','ON':'Ontario','MB':'Manitoba',
                    'SK':'Saskatchewan','AB':'Alberta','YT':'Yukon','NT':'Northwest Territories','NU':'Nunavut','BC':'British Columbia'};

                $("#address").val(add[0]);
                $('#city').val(add[1]);
                console.log(province_array[province.trim()]);
                $('select[name^="provenance"] option:selected').attr("selected",null);
                // $("#province").find('option[value="'+province_array[province.trim()]+'"]').attr('selected','selected');
                $('#provenance').val(province_array[province.trim()]);
                $('#zipcode').val(add[2].substr(3,8));
                console.log($('#provenance').val());
            });
        });
// change category
    $('#category').on('change',function(){
        category = $('this').val;

    });
    </script>

@endsection


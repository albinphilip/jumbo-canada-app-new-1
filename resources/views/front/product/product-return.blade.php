@extends('front.layouts.app')
@section('title','Product Return Registration | Jumbo Canada')
@section('description','Troubleshooting cancellation and Return. Register for product return registration. Select Product Category and product detail to initiate the service.')
@section('canonical','https://jumbokitchenappliances.com/return-product')
@section('keywords','preethi product return,register for product return,canada appliances store,online home appliances store,best appliances in online,branded home products,kitchen products')
@section('content')
    <div class="return-reason page">
        <div class="container">
            <div class="return-inner">
                <h1>Reason for returning</h1>
                    <form action="{{route('orderReturn')}}" method="POST" enctype="multipart/form-data">

                     @csrf
                    <input type="hidden" value="{{$id}}" name="order_id">
                    <input type="text" placeholder="Dead On Arrival" class="form-control" name="dead">
                    <textarea class="form-control" placeholder="Faulty, please supply details" name="faulty"></textarea>
                    <p>Product is opened?</p>
                    <div class="pretty p-default p-curve">
                        <input type="radio" name="is_open" value="Yes" checked required>
                        <div class="state p-primary-o">
                            <label>Yes</label>
                        </div>
                    </div>
                    <div class="pretty p-default p-curve">
                        <input type="radio" name="is_open" value="No"/>
                        <div class="state p-primary-o">
                            <label>No</label>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                        <div class="upload">
                            <input class="form-control" id="upload-file-img" type="file"
                                   accept="image/png, image/jpeg" name="image1">

                            <a id="fileupload-img"><span>Upload Image</span><img
                                    src="{{asset('assets/img/attach.svg')}}"
                                    alt="upload"></a>

                        </div>
                    <input type="submit" value="return">
                </form>
            </div>
        </div>
    </div>

@endsection

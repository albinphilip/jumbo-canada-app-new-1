@extends('front.layouts.app')
@section('title','Buy back step 3 | Jumbo Canada')
@section('description','')
@section('keywords','')
@section('content')
    <div class="buy-back-page page">
        <div class="banner">
            <img src="{{asset('assets/img/product-list-banner.jpg')}}" alt="" class="img-fluid">
            <!--            <div class="container-fluid">-->
            <!--                <div class="row">-->
            <!--                    <div class="col-sm-6">-->
            <!--                        <h1>Go Green - Buy Back Offer</h1>-->
            <!--                        <p>With a core aim to conserve nature and recycle waste materials we introduce the Go Green Buy-->
            <!--                            Back-->
            <!--                            Offer where we provide customers exclusive discounts on recycling their old Preethi-->
            <!--                            Products. Lets-->
            <!--                            work together to make this a sustainable eco system. Lets go Green !</p>-->
            <!--                    </div>-->
            <!--                    <div class="col-sm-6"></div>-->
            <!--                </div>-->
            <!---->
            <!--            </div>-->
        </div>
        <div class="buy-back-inner">
            <h2>Buy Back Offer</h2>
            <div class="container">
                <ul class="head-block list-inline">
                    <li class="list-inline-item ">
                        <div class="block">
                            <p>Step01</p>
                            <h5>Find your nearest recycling
                                location</h5>
                        </div>
                    </li>
                    <li class="list-inline-item">
                        <div class="block">
                            <p>Step02</p>
                            <h5>Enter details of product to be recycled
                                / recycled product</h5>
                        </div>
                    </li>
                    <li class="list-inline-item active">
                        <div class="block">
                            <p>Step03</p>
                            <h5>Attach your Receipt and
                                Avail discount</h5>
                        </div>
                    </li>
                </ul>
                <div class="form-block">
                    @if ($message = Session::get('success'))
                        <h3>Your buy back request has been submitted. Please check your inbox for details.</h3>
                        <ul class="list-inline links">
                            <li class="list-inline-item"><a href="{{route('home')}}">Continue</a></li>
                        </ul>
                    @else
                        <form method="post" action="{{route('generate-discount-code')}}" enctype="multipart/form-data"
                              id="code-generate">
                            @csrf
                            <div>
                                <div class="upload">

                                    <input name="buy_back_id" id="buy-back-id" class="form-control" type="text" hidden
                                           value="{{Session::get('buy_back_id')}}">
                                    <input name="attached_file" class="form-control" id="upload-file-img" type="file"
                                           accept="image/png, image/jpeg, application/pdf"
                                           required>

                                    <a id="fileupload-img"><span>Attach Your Invoice *</span><img
                                            src="{{asset('assets/img/attach.svg')}}"
                                            alt="upload"></a>
                                </div>
                                <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                                <button type="submit" id="generate-code" class="discount-code">Upload and Get Discounts
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "BuyBackStep3" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection

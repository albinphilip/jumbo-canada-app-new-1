@extends('admin.layouts.datatable')
@section('title','Tax')
@section('tableTitle','Tax')
@section('tableHead')
    <th>#</th>
    <th>Province</th>
    <th>Date of Order</th>
    <th>Order Id</th>
    <th>Customer</th>
    <th>Order Total</th>
    <th>Tax Slab</th>
    <th>Tax amount</th>
@endsection
@section('tableBody')
    @php  $total_cost=0; @endphp
    @foreach($orders->sortByDesc('order_id') as $order)
        @if($order->status != 'Cancel & refunded')
        <tr>
            <td></td>
            <td>{{$order->deliveryAddress->province}}</td>
            <td>{{date_format($order->created_at,'Y-m-d')}}</td>
            <td>{{$order->order_id }}</td>
            <td>{{$order->customer->firstname.' '.$order->customer->lastname}}</td>
            <td>C${{$order->order_total}}</td>
           <td>{{$order->orderFeesplitUp->tax}}</td>
            <td>C$
                @if($order->orderFeesplitUp->tax_value == null)
                    @if($order->shipping_partner == 'Canada Post' && $order->shipping_code == '' )
                        {{($order->orderFeesplitUp->sub_total-$order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100}}
                    @else
                        {{($order->orderFeesplitUp->sub_total+$order->orderFeesplitUp->shipping_charge-$order->orderFeesplitUp->reduction)*$order->orderFeesplitUp->tax/100}}
                    @endif
            @else
                {{$order->orderFeesplitUp->tax_value}}
                @endif
            </td>
        </tr>
        @endif
    @endforeach

    <!-- date filter -->
    <div class="input-daterange">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="province" class="control-label">Province</label>
                    <select name="province" id="province" class="form-control">
                        <option value="All">All</option>
                        <option value="Nunavut">Nunavut</option>
                        <option value="Quebec">Quebec</option>
                        <option value="Northwest Territories">Northwest Territories</option>
                        <option value="Ontario">Ontario</option>
                        <option value="British Columbia">British Columbia</option>
                        <option value="Alberta">Alberta</option>
                        <option value="Saskatchewan">Saskatchewan</option>
                        <option value="Manitoba">Manitoba</option>
                        <option value="Yukon">Yukon</option>
                        <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                        <option value="New Brunswick">New Brunswick</option>
                        <option value="Nova Scotia">Nova Scotia</option>
                        <option value="Prince Edward Island">Prince Edward Island</option>

                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="from" class="control-label">From</label>
                    <input type="text" id="from_date" name="from_date" class="form-control dateFilter" value="{{old('from_date')}}" >
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="to" class="control-label">To</label>
                    <input type="text" id="to_date" name="to_date" required class="form-control dateFilter" value="{{old('to_date')}}" >
                </div>
            </div>
            <div class="col-1 pt-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
            <div class="col-2 pt-4" id="download_div">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="exportForm()" target="_blank"><i class="fa fa-download"></i></button>
                </div>
                <div style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-4" id="total_div" style="display: none;">
            <div class="form-group">
                <label for="Amount" class="control-label">Total Amount</label>
                <input type="text" value="" class="form-control" name="total_amount" id="total_amount" readonly>
            </div>
        </div>
    </div>
@endsection

@section('evenMoreScripts')
    <!-- date filter -->
    <script>
        var total_amount = 0;
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-m-dd"
            });
            var id_prev = 0;
            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#from_date').datepicker("getDate");
                    var max = $('#to_date').datepicker("getDate");
                    var province = $('#province').val();// selected province
                    var hireDate = new Date(data[2]); //date of order
                    var prov = data[1];
                    // console.log('from'+min);
                    //console.log('to'+max);
                    if(min != null)
                    {
                        min=min.setHours(0,0,0,0);
                    }
                    if(max != null)
                    {
                        max=max.setHours(0,0,0,0);
                    }
                    hireDate=hireDate.setHours(0,0,0,0);
                    var order_total = data[5].slice(2); // $order->order_total
                    order = parseFloat(order_total.replace(/,/g,""));
                    //var status = data[2];
                    var order_id = data[3];
                    // console.log('var:'+ order);
                    //  console.log(total_amount);
                    if (min == null && max == null && (province==prov || province == 'All')) {
                        if(id_prev != order_id)
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true; }
                    else if (min == null && hireDate <= max && (province==prov || province == 'All')) {
                        if( id_prev != order_id)
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true;}
                    else if(max == null && hireDate >= min && (province==prov || province == 'All')) {
                        if( id_prev != order_id)
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true;}
                    else if (min <= hireDate && hireDate <= max && (province==prov || province == 'All')) {
                        if( id_prev != order_id)
                        {   console.log(id_prev);
                            console.log(order);
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true; }
                    return false;
                }
            );
            $("#filter").click(function () {

                var myDataTable = $('#tb1').DataTable();
                myDataTable.draw();
                //console.log(total_amount);
                $('#total_div').css('display','block');
                $('#total_amount').val('C$'+parseFloat(total_amount).toFixed(2));
                total_amount = 0;
                id_prev = 0;

            });
        });
        function exportForm()
        {
            var from = $('#from_date').datepicker("getDate");
            var to = $('#to_date').datepicker("getDate");
            var province = $('#province').val();// selected province
            console.log(province);
            $('#response').css('display','block');
            if(from != null )
                from = from.getFullYear() + "/" +
                    (from.getMonth() + 1) + "/" + from.getDate();
            if(to != null)
                //to = to.toLocaleDateString();
                to = to.getFullYear() + "/" +
                    (to.getMonth() + 1) + "/" + to.getDate();
            console.log(from);

            url = '/admin/report/tax-report';
            $.ajax({
                type: "post",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"post",from: from,to:to,province:province},
                success: function (result) {
                    console.log(result);

                    if(result.success = 'success')

                    {
                        $('#response').css('display','none');
                        window.open(result.path,'_blank');
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });
        }
    </script>
@endsection



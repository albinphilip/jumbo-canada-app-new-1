@extends('admin.layouts.form')
@section('additionalStyles')
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
@endsection
@section('title','Add new FAQ')
@section('actionUrl')
    {{route('faq.store')}}
@endsection
<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add FAQ')
@section('indexRoute')
    {{route('faq.index')}}
@endsection
@section('formBody')
    <div class="col-12">
        <div class="form-group">
            <label for="question" class="control-label">Question</label>
            <input type="text" id="question" name="question" required class="form-control {{$errors->has('question') ? 'is-invalid' : ''}}" value="{{old('question')}}">
            @if($errors->has('question'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('title')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="answer" class="control-label">Answer <small>(Do not capitalise entire sentence)</small></label>
            <textarea id="answer" name="answer"
                      class="form-control textarea {{$errors->has('answer') ? 'is-invalid' : ''}}" >{{old('answer')}}
            </textarea>
            @if($errors->has('answer'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('answer')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        $(function () {
            // Summernote
            $('.textarea').summernote({
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                }
            })
        })
    </script>
@endsection


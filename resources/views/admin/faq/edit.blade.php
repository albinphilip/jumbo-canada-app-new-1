@extends('admin.layouts.form')
@section('additionalStyles')
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
@endsection
@section('title','Edit FAQ')
@section('actionUrl')
    {{route('faq.update',$faq)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Update FAQ')
@section('indexRoute')
    {{route('faq.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="question" class="control-label">Question</label>
            <input type="text" id="question" name="question" required class="form-control {{$errors->has('question') ? 'is-invalid' : ''}}" value="{{$faq->question}}">
            @if($errors->has('question'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('question')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="answer" class="control-label">Answer <small>(Do not capitalise entire sentence)</small></label>
            <textarea id="answer" name="answer"
                      class="form-control textarea {{$errors->has('answer') ? 'is-invalid' : ''}}" >{{$faq->answer}}
            </textarea>
            @if($errors->has('answer'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('answer')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        $(function () {
            // Summernote
            $('.textarea').summernote({
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                }
            })
        })
    </script>
@endsection

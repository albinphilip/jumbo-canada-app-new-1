@extends('admin.layouts.datatable')
@section('title','Orders')
@section('tableTitle','Order')
@section('tableHead')
    <th>#</th>
    <th>Order Id</th>
    <th>Status</th>
    <th>Customer</th>
    <th>Email Id</th>
    <th>Phone</th>
    <th>Product</th>
    <th>Date of Order</th>
    <th>Order Total</th>

@endsection
@section('tableBody')
    @php  $total_cost=0; @endphp
    @foreach($orders->sortByDesc('id') as $order)
        <tr>
            <td></td>
            <td><a href="{{route('order.invoice',$order->order_id)}}">{{$order->order_id }}</a><p class="pb-2"></p>
            </td>
            <td>{{$order->status}}</td>
            <td>{{$order->customer->firstname.' '.$order->customer->lastname}}</td>
            <td>{{$order->customer->email}}</td>
            <td>{{$order->customer->telephone}}</td>
            <td>{{$order->product->name}}<br/></td>
            <td>{{date_format($order->created_at,'Y-m-d')}}</td>
            <td>C${{$order->order_total}}<br/>
            <!--<a href="{{route('splitup',$order->order_id)}}"><i class="fa fa-eye"></i></a>-->
            </td>
        </tr>
    @endforeach

    <!-- date filter -->
        <div class="input-daterange">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="from" class="control-label">From</label>
                        <input type="text" id="from_date" name="from_date" class="form-control dateFilter" value="{{old('from_date')}}" >
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="to" class="control-label">To</label>
                        <input type="text" id="to_date" name="to_date" required class="form-control dateFilter" value="{{old('to_date')}}" >
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="status" class="control-label">Status</label>
                        <select name="status" id="status" class="form-control" multiple>
                            <option value="All" selected>All</option>
                            <option value="Order placed">Order placed</option>
                            <option value="Processing">Processing</option>
                            <option value="Shipped">Shipped</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Cancel requested">Cancel Requested</option>
                            <option value="Cancel & refunded">Cancel & refunded</option>
                            <option value="Return Requested">Return Requested</option>
                            <option value="Return Confirmed ">Return Confirmed</option>

                        </select>
                    </div>
                </div>
                <div class="col-1 pt-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                </div>
                <div class="col-2 pt-4" id="download_div">
                    <div class="form-group">
                        <!--<label for="button" class="control-label">Download (CSV)</label><br/>-->
                        <button type="button" class="btn btn-success" onclick="exportForm()" target="_blank"><i class="fa fa-download"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4" id="total_div" style="display: none;">
                <div class="form-group">
                    <label for="Amount" class="control-label">Total Amount</label>
                    <input type="text" value="" class="form-control" name="total_amount" id="total_amount" readonly>
                </div>
            </div>
        </div>
@endsection

@section('evenMoreScripts')
    <!-- date filter -->
    <script>
        var total_amount = 0;
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-m-dd"
            });
            var id_prev = 0;
            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#from_date').datepicker("getDate");
                    var max = $('#to_date').datepicker("getDate");
                    var hireDate = new Date(data[7]); //date of order
                    var order_status = $('#status').val();
                   console.log(order_status);
                    //console.log('to'+max);
                    if(min != null)
                    {
                        min=min.setHours(0,0,0,0);
                    }
                    if(max != null)
                    {
                        max=max.setHours(0,0,0,0);
                    }
                    hireDate=hireDate.setHours(0,0,0,0);
                    var order_total = data[8].slice(2); // $order->order_total
                    order = parseFloat(order_total.replace(/,/g,""));
                    var status = data[2];
                    var order_id = data[1];
                   // console.log('var:'+ order);
                   //  console.log(total_amount);
                    if (min == null && max == null && ((jQuery.inArray('All',order_status)!= -1)||(jQuery.inArray(status,order_status)!=-1))) {
                        if(status != 'Cancel & refunded' && id_prev != order_id  && order_id != '')
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true; }
                    else if (min == null && hireDate <= max && ((jQuery.inArray('All',order_status)!= -1)||(jQuery.inArray(status,order_status)!=-1))) {
                        if(status != 'Cancel & refunded' && id_prev != order_id  && order_id != '')
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true;}
                    else if(max == null && hireDate >= min && ((jQuery.inArray('All',order_status)!= -1)||(jQuery.inArray(status,order_status)!=-1))) {
                        if(status != 'Cancel & refunded' && id_prev != order_id  && order_id != '')
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true;}
                    else if (min <= hireDate && hireDate <= max && ((jQuery.inArray('All',order_status)!= -1)||(jQuery.inArray(status,order_status)!=-1))) {
                        if(status != 'Cancel & refunded' && id_prev != order_id  && order_id != '')
                        {   console.log(id_prev);
                            console.log(order);
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true; }
                    return false;
                }
            );
            $("#filter").click(function () {

                var myDataTable = $('#tb1').DataTable();
                myDataTable.draw();
                //console.log(total_amount);
                $('#total_div').css('display','block');
                $('#total_amount').val('C$'+parseFloat(total_amount).toFixed(2));
                total_amount = 0;
                id_prev = 0;

            });
        });
        function exportForm()
        {
            console.log('submit');
            var from = $('#from_date').datepicker("getDate");
            var to = $('#to_date').datepicker("getDate");
            var status = $('#status').val();
            if(from != null )
                from = from.getFullYear() + "/" +
                    (from.getMonth() + 1) + "/" + from.getDate();
            if(to != null)
                //to = to.toLocaleDateString();
                to = to.getFullYear() + "/" +
                    (to.getMonth() + 1) + "/" + to.getDate();
            console.log(from);

            url = '/admin/order/export-to-csv';
            $.ajax({
                type: "post",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"post",from: from,to:to,status:status},
                success: function (result) {
                    console.log(result);

                    if(result.success = 'succes')
                    {
                        window.open(result.path);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });
        }
    </script>
@endsection



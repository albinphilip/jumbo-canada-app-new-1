@extends('admin.layouts.datatable')
@section('title','Additional Fee and Taxes')
@section('tableTitle','Additional Fee and Taxes')
@section('createRoute')
    {{route('fees.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Province</th>
    <th>Shipping Charge</th>
    <th>Tax</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($fees->sortByDesc('id') as $fee)
        <tr>
            <td></td>
            <td>{{$fee->province}}</td>
            <td>{{$fee->shipping_charge}}</td>
            <td>{{$fee->tax}}</td>
            <td>
                <form action="{{route('fees.destroy',$fee->id)}}" method="POST" id="delete-form-{{$fee->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$fee->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('fees.edit',$fee->id)}}"><i class="fa fa-edit"></i> </a>
            </td>

        </tr>
    @endforeach
@endsection

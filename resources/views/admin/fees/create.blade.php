@extends('admin.layouts.form')
@section('title','Add Additional Fees')
@section('actionUrl')
    {{route('fees.store')}}
@endsection
@section('actionName','Add Fee')
@section('indexRoute')
    {{route('fees.index')}}
@endsection
@section('formBody')
    <div class="col-6">
        <div class="form-group">
            <label for="province" class="control-label">Province</label>
            <select name="province" class="form-control" id="province" required>
                <option value="">Select Province</option>
                <option value="Nunavut" >Nunavut</option>
                <option value="Quebec">Quebec</option>
                <option value="Northwest Territories" >Northwest Territories</option>
                <option value="Ontario">Ontario</option>
                <option value="British Columbia">British Columbia</option>
                <option value="Alberta">Alberta</option>
                <option value="Saskatchewan" >Saskatchewan</option>
                <option value="Manitoba">Manitoba</option>
                <option value="Yukon" >Yukon</option>
                <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                <option value="New Brunswick">New Brunswick</option>
                <option value="Nova Scotia" >Nova Scotia</option>
                <option value="Prince Edward Island">Prince Edward Island</option>
            </select>
            @if($errors->has('province'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('province')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="shipping_charge" class="control-label">Shipping Charge</label>
            <input type="text" id="shipping_charge" name="shipping_charge" required class="form-control {{$errors->has('shipping_charge') ? 'is-invalid' : ''}}" value="{{old('shipping_charge')}}">
            @if($errors->has('shipping_charge'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('shipping_charge')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="tax" class="control-label">Tax</label>
            <input type="text" id="tax" name="tax" required class="form-control {{$errors->has('tax') ? 'is-invalid' : ''}}" value="{{old('tax')}}">
            @if($errors->has('tax'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('tax')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection


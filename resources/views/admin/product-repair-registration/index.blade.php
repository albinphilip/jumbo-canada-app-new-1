@extends('admin.layouts.datatable')
@section('title','Product-repair-registrations')
@section('tableTitle','Product-repair-registrations')
{{--@section('createRoute')--}}
{{--    {{route('product.create')}}--}}
{{--@endsection--}}
@section('tableHead')
    <th>#</th>
    <th>RMA Number</th>
    <th>Serial Number</th>
    <th>Safety Code</th>
    <th>Product Code</th>
    <th>Date of Purchase</th>
    <th>Purchase place</th>
    <th>Issue</th>
    <th>Status</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Phone</th>
    <th>Email</th>
    <th>Province</th>
    <th>City</th>
    <th>Address</th>
    <th>Zip code</th>
    <th>Attachment</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($products->sortByDesc('id') as $product)
        <tr>
            <td></td>
            <td>PR-{{$product->rma_num}}</td>
            <td>{{$product->serial_number}}</td>
            <td>{{$product->safety_code}}<br/>
                @if(preg_match('#^(PT20ULT90[1-9]{1,1}[0-9]{1,1}|PT20ULT9[1-9]{1,1}[0-9]{1,1}[0-9]{1,1}|PT20ULT10[0-4]{1,1}[0-9]{1,1}[0-9]{1,1}|PT20ULT105[00]{2}|PT21ULT[0-9]{2,})$#i',
                         $product->safety_code)===0)
                    <small class="badge badge-warning"> Potentially wrong PT Number </small>
                @endif
                @foreach($duplicates->sortByDesc('id') as $duplicate)
                    @if($product->safety_code == $duplicate->safety_code)
                        <small class="badge badge-danger">Duplicate PT Code</small>
                    @endif
                @endforeach
            </td>
            <td>{{$product->product_code}}</td>
            <td>{{$product->purchase_date}}</td>
            <td>{{$product->store->city}}<br/>{{$product->store->address}}</td>
            <td>{{$product->issue}}</td>
            <td><form action="{{route('product-repair-registration.store')}}" method="POST" id="form{{$product->id}}">
                    @method('PATCH')
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="hidden" value="{{$product->id}}" name="id">
                        <select id="status{{$product->id}}" name="status" required
                                class="form-control {{$errors->has('status') ? 'is-invalid' : ''}}" onchange="changestatus({{$product->id}})">
                            <option value="" {{$product->status == '' ? 'selected' : ''}}  @if($product->status != '') hidden @endif>Select</option>
                            <option value="Repairing" {{$product->status == 'Repairing' ? 'selected' : ''}}
                            @if($product->status == 'Rejected' || $product->status == 'Repaired') hidden @endif>Repairing</option>
                            <option value="Repaired" {{$product->status == 'Repaired' ? 'selected' : ''}}
                            @if($product->status == 'Rejected') hidden @endif>Repaired</option>
                            <option value="Rejected" {{$product->status == 'Rejected' ? 'selected' : ''}}
                                @if($product->status == 'Repairing' || $product->status == 'Repaired') hidden @endif>
                                Rejected</option>
                        </select>
                        @if($errors->has('status'))
                            <span class="help-block error invalid-feedback">
                            <strong>{{$errors->first('status')}}</strong>
                         </span>
                        @endif
                    </div>
                </form></td>
            <td>{{$product->first_name}}</td>
            <td>{{$product->last_name}}</td>
            <td>{{$product->phone}}</td>
            <td>{{$product->email}}</td>
            <td>{{$product->provenance}}</td>
            <td>{{$product->city}}</td>
            <td>{{$product->address}}</td>
            <td>{{$product->zipcode}}</td>
            <td>@if($product->attached_file!='')<a target="_blank" href="{{asset($product->attached_file)}}"><i class="fa fa-paperclip"></i> </a> @else No attachment @endif</td>
            <td>
                <a href="{{route('product-repair-registration.edit',$product->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
    <!-- rejection Modal -->
    <div class="modal" id="rejectionModal" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal" data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Rejection Reason</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('repair.reject')}}" method="post" name="discount" id="approvalForm">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6" hidden>
                                    <input type="text" name="rej_repair_id" id="rej_repair_id" value=""
                                           class="form-control">
                                    <span class="text-danger">
                                        <small><strong id="rej_repair_id"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-12">
                                    <textarea  name="reason" value="" id="reason" class="form-control"></textarea>
                                    <span class="text-danger">
                                        <small><strong id="reason-error"></strong></small>
                                </span>
                                </div>
                            </div>
                            <div class="actions">
                                <ul class="list-inline pt-2">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-primary" onclick="reasonSubmit();">Submit</a>
                                    </li>
                                    <li style="display:none;" id="rej-response"><i class="fa fa-cog fa-spin"></i>Please
                                        Wait
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('evenMoreScripts')
    <script type="text/javascript">

        function  changestatus(id) {
            var status = $('#status'+id).val();
            if(status == 'Rejected')
            {
                $('#rej_repair_id').val(id);
                $('#rejectionModal').modal('show');

            }
            else{
                var url='/admin/product-repair-registration';
                console.log(url);
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"POST",status: status,id:id},
                    success: function (result) {
                        fireTost('Status updated');
                        setInterval('location.reload()', 2500);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to update status");
                    }
                });
            }

        }
        function reasonSubmit() {
            repair_id = $('#rej_repair_id').val();
            reason = $('#reason').val();

            if (!reason) $('#reason-error').html('Write reason');
            else {
                $('#rej-response').css('display', 'block');
                url = '{{route('repair.reject')}}';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}', _method: "post", id: repair_id, reason: reason},
                    success: function (result) {
                        if (result == 'success') {
                            $('#rejectionModel').modal('hide');
                            fireTost('Repair Request rejected');
                            setInterval('location.reload()', 2000);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to reject request");
                        $('#response').css('display', 'none');
                        //$('#buybackform')[0].reset();

                    }
                });
            }

        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }

    </script>
@endsection



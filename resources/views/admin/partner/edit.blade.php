@extends('admin.layouts.form')
@section('title','Edit Partner Registration')
@section('actionUrl')
    {{route('partner.update',$partner)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Update')
@section('indexRoute')
    {{route('partner.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <input type="text" id="name" name="name" required class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{$partner->name}}">
            @if($errors->has('name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="address" class="control-label">Address</label>
            <textarea id="address" name="address" required
                      class="form-control  {{$errors->has('address') ? 'is-invalid' : ''}}">{{$partner->address}}
            </textarea>
            @if($errors->has('address'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('address')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label class="control-label" for="province">Province</label>
            <select id="province" name="province" class="form-control" required>
                <option value="Nunavut" {{old('province',$partner->province) == 'Nunavut' ? 'selected' : ''}}>Nunavut</option>
                <option value="Quebec" {{old('province',$partner->province) == 'Quebec' ? 'selected' : ''}}>Quebec</option>
                <option value="Northwest Territories" {{old('province',$partner->province) == 'Northwest Territories' ? 'selected' : ''}}>Northwest Territories</option>
                <option value="Ontario" {{old('province',$partner->province) == 'Ontario' ? 'selected' : ''}}>Ontario</option>
                <option value="British Columbia" {{old('province',$partner->province) == 'British Columbia' ? 'selected' : ''}}>British Columbia</option>
                <option value="Alberta" {{old('province',$partner->province) == 'Alberta' ? 'selected' : ''}}>Alberta</option>
                <option value="Saskatchewan" {{old('province',$partner->province) == 'Saskatchewan' ? 'selected' : ''}}>Saskatchewan</option>
                <option value="Manitoba" {{old('province',$partner->province) == 'Manitoba' ? 'selected' : ''}}>Manitoba</option>
                <option value="Yukon" {{old('province',$partner->province) == 'Yukon' ? 'selected' : ''}}>Yukon</option>
                <option value="Newfoundland and Labrador" {{old('province',$partner->province) == 'Newfoundland and Labrador' ? 'selected' : ''}}>Newfoundland and Labrador</option>
                <option value="New Brunswick" {{old('province',$partner->province) == 'New Brunswick' ? 'selected' : ''}}>New Brunswick</option>
                <option value="Nova Scotia" {{old('province',$partner->province) == 'Nova Scotia' ? 'selected' : ''}}>Nova Scotia</option>
                <option value="Prince Edward Island" {{old('province',$partner->province) == 'Prince Edward Island' ? 'selected' : ''}}>Prince Edward Island</option>
            </select>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="city" class="control-label">City</label>
            <input type="text" id="city" name="city" required class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}" value="{{$partner->city}}">
            @if($errors->has('city'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('city')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="Phone" class="control-label">Contact Number</label>
            <input type="text" id="Phone" name="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask required class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" value="{{$partner->phone}}">
            @if($errors->has('phone'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('phone')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="text" id="email" name="email" required class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{$partner->email}}">
            @if($errors->has('email'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('email')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection

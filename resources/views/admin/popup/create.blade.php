@extends('admin.layouts.form')
@section('title','Add/Edit Popup')
@section('actionUrl')
    {{route('popup.store')}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add Popup')
@section('indexRoute')
    {{route('popup.index')}}
@endsection
@section('formBody')
    <div class="col-6">
        <div class="form-group">
            <label for="title" class="control-label">Title</label>
            <input type="text" id="title" name="title"  class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}"
                   @if($popup!=null) value="{{$popup->title}}" @else value = "{{old('title')}}" @endif>
            @if($errors->has('title'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('title')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="url" class="control-label">URL</label>
            <input type="text" id="url" name="url"  class="form-control {{$errors->has('url') ? 'is-invalid' : ''}}"
                   @if($popup!=null) value="{{$popup->url}}" @else value = "{{old('url')}}" @endif>
            @if($errors->has('url'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('url')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="body" class="control-label">Content</label>
            <textarea id="body" name="body"
                      class="form-control textarea {{$errors->has('body') ? 'is-invalid' : ''}}" >
                @if($popup!=null) {{$popup->body}} @else {{old('content')}} @endif
            </textarea>
            @if($errors->has('body'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('body')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="image"> image <small>(Maximum 500Kb)</small></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" id="image" name="image" value="{{old('image')}}" required>
                    <label class="custom-file-label" for="image">Choose file</label>
                </div>
            </div>
            @if($errors->has('image'))
                <span class="help-block error" style="color:#c9352a">
                    <strong>{{$errors->first('image')}}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            <label for="expiry_date" class="control-label">Expiry Date <small></small></label>
            <input type="date" id="expiry_date" name="expiry_date" required class="form-control {{$errors->has('expiry_date') ? 'is-invalid' : ''}}" required
                   @if($popup!=null) value="{{date('Y-m-d',strtotime($popup->expiry_date))}}" @else value = "{{old('expiry_date')}}" @endif>
            @if($errors->has('expiry_date'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('expiry_date')}}</strong>
                </span>
            @endif
        </div>
    </div>

@endsection
@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection


@extends('admin.layouts.app')
@section('title','Update Product')
@section('additionalStyles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @yield('evenMoreStyles')
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <div>
                            <ul class="nav nav-pills" id="myTab">
                                <li class="nav-item"><a class="nav-link active" href="#general"
                                                        data-toggle="tab">General</a></li>
                                <li class="nav-item"><a class="nav-link" href="#specification" data-toggle="tab">Specification</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#image" data-toggle="tab">Images</a></li>
                                <li class="nav-item"><a class="nav-link" href="#video" data-toggle="tab">Video</a></li>
                                <li class="nav-item"><a class="nav-link" href="#varient" data-toggle="tab">Variant</a></li>
                                <li class="nav-item"><a class="nav-link" href="#metadata" data-toggle="tab">Meta Data</a>
                                <li class="nav-item"><a class="nav-link" href="#shipment" data-toggle="tab">For Shipment</a>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" form="update-form">
                                Update
                            </button>
                        </div>
                    </div>
                </div>

                <form method="POST" action="{{route('product.update',$product)}}" enctype="multipart/form-data"
                      id="update-form">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <!-- /.card-header -->
                    <div class="card-body">
                        <!-- this is needed in edit form -->

                        <div class="tab-content">
                            <div id="general" class="tab-pane active">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="brand" class="control-label">Brand Name<small
                                                    style="color:red;">*</small></label>
                                            <select name="brand_id" class="form-control" required id="brand_id">
                                                <option value="">{{$product->brand->name}}</option>
                                                @foreach($brands->sortBy('name') as $brand)
                                                    <option
                                                        value="{{$brand->id}}" {{$product->brand_id == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('brand_id'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('brand_id')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="category" class="control-label">Product Category<small
                                                    style="color:red;">*</small></label>
                                            <select name="category_id" class="form-control" required id="category_id" onchange="change_category();">
                                                <option value="">Select</option>
                                                @foreach($categories->sortBy('name') as $category)
                                                    <option
                                                        value="{{$category->id}}" {{$product->category_id == $category->id ? 'selected' : ''}}>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('name'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('name')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Product Name<small
                                                    style="color:red;">*</small></label>
                                            <input type="text" id="name" name="name" required
                                                   class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                                                   value="{{$product->name}}">
                                            @if($errors->has('name'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('name')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="offer" class="control-label">Offer % <small>(If the product is
                                                    sold on offer price)</small></label>
                                            <input type="text" id="offer" name="offer"
                                                   class="form-control {{$errors->has('offer') ? 'is-invalid' : ''}}"
                                                   value="{{$product->offer}}">
                                            @if($errors->has('offer'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('offer')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="offer_expire" class="control-label">Offer Price Expires
                                                on</label>
                                            <input type="date" id="offer_expire" name="offer_expire"
                                                   class="form-control {{$errors->has('offer_expire') ? 'is-invalid' : ''}}"
                                                   value="{{date("Y-m-d",strtotime($product->offer_expire))}}">
                                            @if($errors->has('offer_expire'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('offer_expire')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="model" class="control-label">Product Model</label>
                                            <input type="text" id="model" name="model"
                                                   class="form-control {{$errors->has('model') ? 'is-invalid' : ''}}"
                                                   value="{{$product->model}}">
                                            @if($errors->has('model'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('model')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="upc" class="control-label">UPC</label>
                                            <input type="text" id="upc" name="upc"
                                                   class="form-control {{$errors->has('upc') ? 'is-invalid' : ''}}"
                                                   value="{{$product->upc}}">
                                            @if($errors->has('upc'))
                                                <span class="help-block error invalid-feedback">
                                                     <strong>{{$errors->first('upc')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="weight" class="control-label">Product Weight <small>in
                                                    Kg</small></label>
                                            <input type="text" id="weight" name="weight"
                                                   class="form-control {{$errors->has('weight') ? 'is-invalid' : ''}}"
                                                   value="{{$product->weight}}">
                                            @if($errors->has('weight'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('weight')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="dimension" class="control-label">Product Dimension <small>(lXbXh
                                                    cm)</small></label>
                                            <input type="text" id="dimension" name="dimension"
                                                   class="form-control {{$errors->has('dimension') ? 'is-invalid' : ''}}"
                                                   value="{{$product->dimension}}">
                                            @if($errors->has('dimension'))
                                                <span class="help-block error invalid-feedback">
                                                     <strong>{{$errors->first('dimension')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="control-label" for="status">Display Status</label>
                                            <select id="status" name="status" class="form-control" required>
                                                <option
                                                    value="Published" {{$product->status=='Published' ? 'selected' : ''}}>
                                                    Published
                                                </option>
                                                <option value="Hidden" {{$product->status=='Hidden' ? 'selected' : ''}}>
                                                    Hidden
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="description" class="control-label">Description <small>(Do not
                                                    capitalise entire sentence)</small><small
                                                    style="color:red;">*</small></label>
                                            <textarea id="description" name="description"
                                                      class="form-control textarea {{$errors->has('description') ? 'is-invalid' : ''}}">{{$product->description}}
                                            </textarea>
                                            @if($errors->has('description'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('description')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12" hidden>
                                        <div class="form-group">
                                            <label for="keywords" class="control-label">Keywords <small>(Separate each
                                                    by comma)</small></label>
                                            <textarea id="keywords" name="keywords"
                                                      class="form-control  {{$errors->has('keywords') ? 'is-invalid' : ''}}">{{$product->keywords}}
                                             </textarea>
                                            @if($errors->has('keywords'))
                                                <span class="help-block error invalid-feedback">
                                                <strong>{{$errors->first('keywords')}}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- row-->
                            </div>
                            <!-- specification -->
                            <div id="specification" class="tab-pane">
                                <div id="mxr"  @if(str_contains($product->category->name,'Mixer') == false) hidden @endif>
                                    <div class="row" id="spec_rows">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="specification" class="control-label">Specification <small>(Height
                                                        / Weight / ..)</small></label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="value" class="control-label">Value <small>(30 / 40 /
                                                        ...)</small></label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label for="unit" class="control-label">Unit <small>(Kg / Cm / ...)</small></label>
                                            </div>
                                        </div>
                                        @php($i = 37)
                                    @php($data  = array('No.Of Jars','Super Extractor Juicer','Chutney Jar Size',
                                           'Medium Jar Size','Large Jar Size',
                                           "Grind N' Store Containers",'Warranty','Voltage, Wattage',
                                           'Chef Jar','Coconut Scraper','Local customization','Sound score',
                                            'Performance Score','Design Score','Return Ratio','Durability Score Jars',
                                            'Sellers Warranty Motor','Sellers Warranty Jars','Sellers Warranty Parts',
                                            'Certification','Motor Type','Mixer Shape','Colour','OLP','Manufactured By','Brand name',
                                            'Price','Remarks','Seller Rating','Packaging','Jar Lid Type','Body made by','Flow Braker',
                                            'Features','Customer rating','Customized'))
                                    @foreach($product->productSpecifications->sortBy('id') as $specification)
                                            <!--SPEC-->
                                            @if(in_array($specification->specification,$data))
                                                @if($specification->specification == 'No.Of Jars')
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="No.Of Jars" name="specification[1][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[1][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                                <option value="2" {{$specification->value == '2' ? 'selected' : ''}}>2</option>
                                                                <option value="3" {{$specification->value == '3' ? 'selected' : ''}}>3</option>
                                                                <option value="4" {{$specification->value == '4' ? 'selected' : ''}}>4</option>
                                                                <option value="5" {{$specification->value == '5' ? 'selected' : ''}}>5</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[1][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Super Extractor Juicer')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Super Extractor Juicer" name="specification[2][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[2][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Included" {{$specification->value == 'Included' ? 'selected' : ''}}>Included</option>
                                                                <option value="Not Included" {{$specification->value == 'Not Included' ? 'selected' : ''}}>Not Included</option>
                                                                <option value="Optional" {{$specification->value == 'Optional' ? 'selected' : ''}}>Optional</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[2][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Capacity of Chutney Jar' || $specification->specification == 'Chutney Jar Size')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Chutney Jar Size" name="specification[3][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[3][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="0.1" {{$specification->value == '0.1' ? 'selected' : ''}}>0.1</option>
                                                                <option value="0.2" {{$specification->value == '0.2' ? 'selected' : ''}}>0.2</option>
                                                                <option value="0.3" {{$specification->value == '0.3' ? 'selected' : ''}}>0.3</option>
                                                                <option value="0.4" {{$specification->value == '0.4' ? 'selected' : ''}}>0.4</option>
                                                                <option value="0.5" {{$specification->value == '0.5' ? 'selected' : ''}}>0.5</option>
                                                                <option value="0.6" {{$specification->value == '0.6' ? 'selected' : ''}}>0.6</option>
                                                                <option value="0.7" {{$specification->value == '0.7' ? 'selected' : ''}}>0.7</option>
                                                                <option value="0.8" {{$specification->value == '0.8' ? 'selected' : ''}}>0.8</option>
                                                                <option value="0.9" {{$specification->value == '0.9' ? 'selected' : ''}}>0.9</option>
                                                                <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[3][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Capacity of Middle Jar' || $specification->specification == 'Medium Jar Size')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Medium Jar Size" name="specification[4][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[4][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="0.5" {{$specification->value == '0.5' ? 'selected' : ''}}>0.5</option>
                                                                <option value="0.75" {{$specification->value == '0.75' ? 'selected' : ''}}>0.75</option>
                                                                <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                                <option value="1.25" {{$specification->value == '1.25' ? 'selected' : ''}}>1.25</option>
                                                                <option value="1.5" {{$specification->value == '1.5' ? 'selected' : ''}}>1.5</option>
                                                                <option value="1.75" {{$specification->value == '1.75' ? 'selected' : ''}}>1.75</option>
                                                                <option value="2" {{$specification->value == '2' ? 'selected' : ''}}>2</option>
                                                                <option value="2.25" {{$specification->value == '2.25' ? 'selected' : ''}}>2.25</option>
                                                                <option value="2.5" {{$specification->value == '2.5' ? 'selected' : ''}}>2.5</option>
                                                                <option value="2.75" {{$specification->value == '2.75' ? 'selected' : ''}}>2.75</option>
                                                                <option value="3" {{$specification->value == '3' ? 'selected' : ''}}>3</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[4][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                    <!--SPEC-->
                                                @elseif($specification->specification == 'Capacity of Liquidizing Jar' || $specification->specification == 'Large Jar Size')
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Large Jar Size" name="specification[5][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[5][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                                <option value="1.25" {{$specification->value == '1.25' ? 'selected' : ''}}>1.25</option>
                                                                <option value="1.5" {{$specification->value == '1.5' ? 'selected' : ''}}>1.5</option>
                                                                <option value="1.75" {{$specification->value == '1.75' ? 'selected' : ''}}>1.75</option>
                                                                <option value="2" {{$specification->value == '2' ? 'selected' : ''}}>2</option>
                                                                <option value="2.25" {{$specification->value == '2.25' ? 'selected' : ''}}>2.25</option>
                                                                <option value="2.5" {{$specification->value == '2.5' ? 'selected' : ''}}>2.5</option>
                                                                <option value="2.75" {{$specification->value == '2.75' ? 'selected' : ''}}>2.75</option>
                                                                <option value="3" {{$specification->value == '3' ? 'selected' : ''}}>3</option>
                                                                <option value="3.25" {{$specification->value == '3.25' ? 'selected' : ''}}>3.25</option>
                                                                <option value="3.5" {{$specification->value == '3.5' ? 'selected' : ''}}>3.5</option>
                                                                <option value="3.75" {{$specification->value == '3.75' ? 'selected' : ''}}>3.75</option>
                                                                <option value="4" {{$specification->value == '4' ? 'selected' : ''}}>4</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[5][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == "Grind N' Store Containers")
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Grind N' Store Containers" name="specification[6][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[6][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}}>Yes</option>
                                                                <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                                <option value="Optional" {{$specification->value == 'Optional' ? 'selected' : ''}}>Optional</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[6][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Warranty')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Warranty" name="specification[7][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[7][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="1 Year" {{$specification->value == '1 Year' ? 'selected' : ''}}>1 Year</option>
                                                                <option value="2 Year" {{$specification->value == '2 Year' ? 'selected' : ''}}>2 Year</option>
                                                                <option value="3 Year" {{$specification->value == '3 Year' ? 'selected' : ''}}>3 Year</option>
                                                                <option value="4 Year" {{$specification->value == '4 Year' ? 'selected' : ''}}>4 Year</option>
                                                                <option value="5 Year" {{$specification->value == '5 Year' ? 'selected' : ''}}>5 Year</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[7][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Voltage, Wattage')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Voltage, Wattage" name="specification[8][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[8][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[8][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Chef Jar')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Chef Jar" name="specification[9][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[9][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}}>Yes</option>
                                                                <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[9][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Coconut Scraper')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Coconut Scraper" name="specification[10][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[10][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}}>Yes</option>
                                                                <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                                <option value="Optional" {{$specification->value == 'Optional' ? 'selected' : ''}}>Optional</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[10][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Local customization')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Local customization" name="specification[11][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[11][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}} >Yes</option>
                                                                <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[11][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Sound score')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Sound score" name="specification[12][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[12][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[12][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Performance Score')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Performance Score" name="specification[13][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[13][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[13][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Design Score')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Design Score" name="specification[14][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[14][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[14][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Return Ratio')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Return Ratio" name="specification[15][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[15][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[15][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Durability Score Jars')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Durability Score Jars" name="specification[16][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[16][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[16][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Sellers Warranty Motor')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Sellers Warranty Motor" name="specification[17][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[17][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[17][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Sellers Warranty Jars')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Sellers Warranty Jars" name="specification[18][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[18][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[18][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Sellers Warranty Parts')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Sellers Warranty Parts" name="specification[19][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[19][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[19][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Certification')
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Certification" name="specification[20][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[20][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="NON-UL" {{$specification->value ==  "NON-UL" ? 'selected' : ''}}>NON-UL</option>
                                                                <option value="UL Motor" {{$specification->value == "UL Motor" ? 'selected' : ''}}>UL Motor</option>
                                                                <option value="SGS" {{$specification->value == "SGS" ? 'selected' : ''}}>SGS</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[20][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPEC-->
                                                @elseif($specification->specification == 'Motor Type')
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Motor Type" name="specification[21][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[21][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Turbo"  {{$specification->value == 'Turbo' ? 'selected' : ''}}>Turbo</option>
                                                                <option value="Aria Cool Tec Motor"  {{$specification->value == 'Aria Cool Tec Motor' ? 'selected' : ''}}>Aria Cool Tec Motor</option>
                                                                <option value="Universal 550 Watt High Power Motor"
                                                                    {{$specification->value == 'Universal 550 Watt High Power Motor' ? 'selected' : ''}}>Universal 550 Watt High Power Motor</option>
                                                                <option value="Universal 600 Watt High Power Motor"
                                                                    {{$specification->value == 'Universal 600 Watt High Power Motor' ? 'selected' : ''}}>Universal 600 Watt High Power Motor</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[21][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Mixer Shape')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Mixer Shape" name="specification[22][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[22][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="A Shape" {{$specification->value == 'A Shape' ? 'selected' : ''}}>A Shape</option>
                                                                <option value="L Shape" {{$specification->value == 'L Shape' ? 'selected' : ''}}>L Shape</option>
                                                                <option value="Cube Shape" {{$specification->value == 'Cube Shape' ? 'selected' : ''}}>Cube Shape</option>
                                                                <option value="Bottle Shape" {{$specification->value == 'Bottle Shape' ? 'selected' : ''}}>Bottle Shape</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[22][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Colour')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Colour" name="specification[23][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[23][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="White" {{$specification->value == 'White' ? 'selected' : ''}}>White</option>
                                                                <option value="Ocean Green" {{$specification->value == 'Ocean Green' ? 'selected' : ''}}>Ocean Green</option>
                                                                <option value="Blue" {{$specification->value == 'Blue' ? 'selected' : ''}}>Blue</option>
                                                                <option value="Red" {{$specification->value == 'Red' ? 'selected' : ''}}>Red</option>
                                                                <option value="Black" {{$specification->value == 'Black' ? 'selected' : ''}}>Black</option>
                                                                <option value="Metallic Silver" {{$specification->value == 'Metallic Silver' ? 'selected' : ''}}>Metallic Silver</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[23][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'OLP')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="OLP" name="specification[24][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[24][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}}>Yes</option>
                                                                <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[24][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Manufactured By')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Manufactured By" name="specification[25][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[25][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Maya Appliances" {{$specification->value == 'Maya Appliances' ? 'selected' : ''}} >Maya Appliances</option>
                                                                <option value="Others" {{$specification->value == 'Others' ? 'selected' : ''}}>Others</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[25][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Brand name')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Brand name" name="specification[26][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <select name="valu[26][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                                <option value="">Select</option>
                                                                <option value="Preethi" {{$specification->value == 'Preethi' ? 'selected' : ''}}>Preethi</option>
                                                                <option value="Butterfly" {{$specification->value == 'Butterfly' ? 'selected' : ''}}>Butterfly</option>
                                                                <option value="Vidiem" {{$specification->value == 'Vidiem' ? 'selected' : ''}}>Vidiem</option>
                                                                <option value="Others" {{$specification->value == 'Others' ? 'selected' : ''}}>Others</option>
                                                            </select>
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[26][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Price')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Price" name="specification[27][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[27][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[27][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                @elseif($specification->specification == 'Remarks')
                                                <!--SPEC-->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="specification" class="form-control"
                                                                   value="Remarks" name="specification[28][]" readonly>
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input type="text" id="value" name="valu[28][]"
                                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->value}}">
                                                            @if($errors->has('value'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input type="text" id="unit"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}" name="unit[28][]">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <!--SPC END-->
                                                <!--SPEC-->
                                            @elseif($specification->specification == 'Seller Rating')
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Seller Rating" name="specification[29][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[29][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                            <option value="2" {{$specification->value == '2' ? 'selected' : ''}}>2</option>
                                                            <option value="3" {{$specification->value == '3' ? 'selected' : ''}}>3</option>
                                                            <option value="4" {{$specification->value == '4' ? 'selected' : ''}}>4</option>
                                                            <option value="5" {{$specification->value == '5' ? 'selected' : ''}}>5</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[29][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->
                                            @elseif($specification->specification == 'Packaging')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Packaging" name="specification[30][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                    <!-- <input type="text" id="value" name="valu[30][]"
                                                               class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                               value="{{old('value')}}"> -->
                                                        <select name="valu[30][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                            <option value="2" {{$specification->value == '2' ? 'selected' : ''}}>2</option>
                                                            <option value="3" {{$specification->value == '3' ? 'selected' : ''}}>3</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[30][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->

                                        @elseif($specification->specification == 'Jar Lid Type')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Jar Lid Type" name="specification[31][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[31][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="Clear Dome" {{$specification->value == 'Clear Dome' ? 'selected' : ''}}>
                                                                Clear Dome</option>
                                                            <option value="Flat Cover with Lock" {{$specification->value == 'Flat Cover with Lock' ? 'selected' : ''}}>
                                                                Flat Cover with Lock</option>
                                                            <option value="Flat Dome" {{$specification->value == 'Flat Dome' ? 'selected' : ''}}>
                                                                Flat Dome</option>
                                                            <option value="Flat" {{$specification->value == 'Flat' ? 'selected' : ''}}>
                                                                Flat</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[31][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->
                                        @elseif($specification->specification == 'Body made by')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Body made by" name="specification[32][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[32][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="ABS Body" {{$specification->value == 'ABS Body' ? 'selected' : ''}}>ABS Body</option>
                                                            <option value="SS" {{$specification->value == 'SS' ? 'selected' : ''}}>SS</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[32][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->

                                        @elseif($specification->specification == 'Flow Braker')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Flow Braker" name="specification[33][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[33][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}}>Yes</option>
                                                            <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[33][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->

                                        @elseif($specification->specification == 'Features')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Features" name="specification[34][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[34][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="Overload and Voltage Fluctation Cut Off" {{$specification->value == 'Overload and Voltage Fluctation Cut Off' ? 'selected' : ''}}>
                                                                Overload and Voltage Fluctation Cut Off</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[34][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->

                                        @elseif($specification->specification == 'Customer rating')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Customer rating" name="specification[35][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[35][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="1" {{$specification->value == '1' ? 'selected' : ''}}>1</option>
                                                            <option value="2" {{$specification->value == '2' ? 'selected' : ''}}>2</option>
                                                            <option value="3" {{$specification->value == '3' ? 'selected' : ''}}>3</option>
                                                            <option value="4" {{$specification->value == '4' ? 'selected' : ''}}>4</option>
                                                            <option value="5" {{$specification->value == '5' ? 'selected' : ''}}>5</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[35][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->


                                                <!--SPC END-->
                                            @elseif($specification->specification == 'Customized')
                                            <!--SPEC-->
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input type="text" id="specification" class="form-control"
                                                               value="Customized" name="specification[36][]" readonly>
                                                        @if($errors->has('specification'))
                                                            <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('specification')}}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="valu[36][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                            <option value="">Select</option>
                                                            <option value="Yes" {{$specification->value == 'Yes' ? 'selected' : ''}} >Yes</option>
                                                            <option value="No" {{$specification->value == 'No' ? 'selected' : ''}}>No</option>
                                                        </select>
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('value')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="text" id="unit"
                                                               class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->unit}}" name="unit[36][]">
                                                        @if($errors->has('unit'))
                                                            <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('unit')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--SPC END-->
                                                @elseif($specification->specification == 'Wattage' || $specification->specification == 'Juicer Jar')
                                                    <div hidden></div>
                                            @else
                                            <!-- already existing specifications -->
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <input data-task="{{$specification->id}}" type="text" id="specification"
                                                                   name="specification[{{$i}}][]"
                                                                   class="form-control {{$errors->has('specification') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->specification}}">
                                                            @if($errors->has('specification'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('specification')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <input data-task="{{$specification->id}}" type="text" id="value"
                                                               name="valu[{{$i}}][]"
                                                               class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                               value="{{$specification->value}}">
                                                        @if($errors->has('value'))
                                                            <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <input data-task="{{$specification->id}}" type="text" id="unit"
                                                                   name="unit[{{$i}}][]"
                                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                                   value="{{$specification->unit}}">
                                                            @if($errors->has('unit'))
                                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                <div class="col-1">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary"
                                                            onclick="delete_individual({{$specification->id}},'specification')">
                                                        <i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                                @php($i++)
                                            @endif
                                            @if (($key = array_search($specification->specification, $data)) !== false)
                                                    @unset($data[$key])
                                            @endif

                                        @endif
                                    @endforeach

                                    </div><!--row-->
                                </div>
                                <div id="other" @if(str_contains($product->category->name, 'Mixer') == true) hidden @endif>
                                    <div class="col-12 text-right">
                                        <a class="btn btn-default btn-sm m-2" id="addspc">Add new specification</a>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="specification" class="control-label">Specification <small>(Height
                                                        / Weight / ..)</small></label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label for="value" class="control-label">Value <small>(30 / 40 /
                                                        ...)</small></label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label for="unit" class="control-label">Unit <small>(Kg / Cm / ...)</small></label>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <label for="action" class="control-label">Action</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" id="spec_row">
                                        @php($i=1)
                                        @foreach($product->productSpecifications->sortBy('id') as $specification)
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <input data-task="{{$specification->id}}" type="text" id="specification"
                                                           name="specification[{{$i}}][]"
                                                           class="form-control {{$errors->has('specification') ? 'is-invalid' : ''}}"
                                                           value="{{$specification->specification}}">
                                                    @if($errors->has('specification'))
                                                        <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('specification')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <input data-task="{{$specification->id}}" type="text" id="value"
                                                           name="valu[{{$i}}][]"
                                                           class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                           value="{{$specification->value}}">
                                                    @if($errors->has('value'))
                                                        <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input data-task="{{$specification->id}}" type="text" id="unit"
                                                           name="unit[{{$i}}][]"
                                                           class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                           value="{{$specification->unit}}">
                                                    @if($errors->has('unit'))
                                                        <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-1">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary"
                                                            onclick="delete_individual({{$specification->id}},'specification')">
                                                        <i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                            @php($i++)
                                        @endforeach
                                        <input type="hidden" id="spc_inc" value="{{$i}}">
                                    </div>
                                </div>

                            </div><!-- specfication end -->

                            <!-- image -->
                            <div id="image" class="tab-pane">
                            @if(session()->has('image_error'))
                                <p style="color:red">{{session()->get('image_error')}}</p>
                                @php(session()->forget('image_error'))
                            @endif
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addimage">Add new image</a>
                                </div>
                                <div class="row">
                                    <div class="col-7">
                                        <div class="form-group">
                                            <label for="image" class="control-label">Image<small> (Not less than 812X590
                                                    px , Maximum 500Kb)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="sort_order" class="control-label">Sort Order</label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="action" class="control-label">Action</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="image_row">
                                    @php($j=1)
                                    @foreach($product->productImages->sortBy('sor_order') as $image)

                                        <div class="col-7">
                                            <div class="form-group">
                                                <input type="text" id="image_path" name="image_path[{{$j}}][]"
                                                       value="{{$image->image_path}}" hidden>
                                                <img src="{{asset($image->thumbnail_path)}}" width="100">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="number" id="sort_order" name="sort_order[{{$j}}][]" min="0"
                                                       class="form-control {{$errors->has('sort_order') ? 'is-invalid' : ''}}"
                                                       value="{{$image->sort_order}}">
                                                @if($errors->has('sort_order'))
                                                    <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('sort_order')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary"
                                                        onclick="delete_individual({{$image->id}},'image')"><i
                                                        class="fa fa-trash"></i></button>

                                            </div>
                                        </div>
                                        @php($j++)
                                    @endforeach
                                    <input type="hidden" id="img_inc" value="{{$j}}">
                                </div>
                            </div><!-- image end-->

                            <div id="video" class="tab-pane">
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addvideo">Add new Video</a>
                                </div>
                                <div class="row" id="video_row">
                                    @php($l=1)
                                    @foreach($product->productVideos->sortBy('sor_order') as $video)

                                        <div class="col-7">
                                            <div class="form-group">
                                                <input type="text" id="url" name="url[{{$l}}][]"
                                                       value="https://www.youtube.com/watch?v={{$video->url}}" hidden>
                                                <iframe width="300" height="115"
                                                        src="https://www.youtube.com/embed/{{$video->url}}"
                                                        frameborder="0"
                                                        allow="clipboard-write; encrypted-media; picture-in-picture"
                                                        allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="number" id="v_sort_order" name="v_sort_order[{{$l}}][]" min="0"
                                                       class="form-control {{$errors->has('v_sort_order') ? 'is-invalid' : ''}}"
                                                       value="{{$video->sort_order}}">
                                                @if($errors->has('v_sort_order'))
                                                    <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('v_sort_order')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary"
                                                        onclick="delete_individual({{$video->id}},'video')"><i
                                                        class="fa fa-trash"></i></button>

                                            </div>
                                        </div>
                                        @php($l++)
                                    @endforeach
                                    <input type="hidden" id="video_inc" value="{{$l}}">
                                </div>
                            </div><!-- video end-->

                            <!-- varient -->
                            <div id="varient" class="tab-pane">
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addvariant">Add new variant</a>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="Variant" class="control-label">Variance on <small>(color / Sie /
                                                    ..)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="value" class="control-label">Variant Value <small>(Red / 45 /
                                                    ..)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="Price" class="control-label">Price of this variant
                                                <small>(C$)</small> </label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="Quantity" class="control-label">Stock</label>
                                        </div>
                                    </div>

                                    <div class="col-1">
                                        <div class="form-group">
                                            <label for="action" class="control-label">Action</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="variant_row">

                                    @php($k=1)
                                    @foreach($product->productVariants->sortBy('id') as $variant)
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="text" id="varient" name="variant[{{$k}}][]"
                                                       class="form-control {{$errors->has('varient') ? 'is-invalid' : ''}}"
                                                       value="{{$variant->variant}}">
                                                @if($errors->has('varient'))
                                                    <span class="help-block error invalid-feedback">
                                                <strong>{{$errors->first('varient')}}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="text" id="value" name="val[{{$k}}][]"
                                                       class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                       value="{{$variant->value}}">
                                                @if($errors->has('value'))
                                                    <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('value')}}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="text" id="price" name="price[{{$k}}][]"
                                                       class="form-control {{$errors->has('price') ? 'is-invalid' : ''}}"
                                                       value="{{$variant->price}}">
                                                @if($errors->has('price'))
                                                    <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('price')}}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <input type="number" id="quantity" name="quantity[{{$k}}][]"
                                                       class="form-control {{$errors->has('quantity') ? 'is-invalid' : ''}}"
                                                       value="{{$variant->quantity}}">
                                                @if($errors->has('quantity'))
                                                    <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('quantity')}}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary"
                                                        onclick="delete_individual({{$variant->id}},'variant')"><i
                                                        class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        @php($k++)
                                    @endforeach
                                    <input type="hidden" id="var_inc" value="{{$k}}">
                                </div>
                            </div>

                            <!-- variant end -->
                            <!-- metadata -->
                            <div class="active tab-pane" id="metadata">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="title" class="control-label">Title</label>
                                            <input type="text" id="title" name="title" maxlength="60"
                                                   class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}"
                                                   @if($product->productMetadata != null) value="{{$product->productMetadata->title}}"
                                                    @else value="{{old('title')}}" @endif>
                                            @if($errors->has('title'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('title')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div><span>(</span><span id="c_count"></span><span>/60)</span></div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="meta_description" class="control-label">Meta Description <small>(Do
                                                    not capitalise entire sentence)</small></label>
                                            <textarea id="meta_description" name="meta_description" maxlength="160"
                                                      class="form-control  {{$errors->has('meta_description') ? 'is-invalid' : ''}}">@if($product->productMetadata != null){{$product->productMetadata->description}} @else {{old('meta_description')}}@endif</textarea>
                                            @if($errors->has('meta_description'))
                                                <span class="help-block error invalid-feedback">
                                                         <strong>{{$errors->first('meta_description')}}</strong>
                                                     </span>
                                            @endif
                                        </div>
                                        <div><span>(</span><span id="current_count"></span><span>/160)</span></div>
                                    </div>
                                    <div class="col-12" >
                                        <div class="form-group">
                                            <label for="keywords" class="control-label">Keywords <small>(Separate each
                                                    by comma)</small></label>
                                            <textarea id="keywords" name="keywords"
                                                      class="form-control  {{$errors->has('keywords') ? 'is-invalid' : ''}}">@if($product->productMetadata != ''){{$product->productMetadata->keywords}}@endif
                                        </textarea>

                                            @if($errors->has('keywords'))
                                                <span class="help-block error invalid-feedback">
                                                <strong>{{$errors->first('keywords')}}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- row-->
                            </div>
                            <!-- metadata end -->

                            <!-- shippment -->
                            <div class="tab-pane" id="shipment">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="weight" class="control-label"> Weight <small>in
                                                    Kg</small></label>
                                            <input type="text" id="weight" name="shipment_weight"
                                                   class="form-control {{$errors->has('weight') ? 'is-invalid' : ''}}"
                                                   @if($product->productShipment !='') value="{{$product->productShipment->shipment_weight}}" @endif>
                                            @if($errors->has('weight'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('weight')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Height <small>in
                                                    inch</small></label>
                                            <input type="text" id="height" name="height"
                                                   class="form-control {{$errors->has('height') ? 'is-invalid' : ''}}"
                                                   @if($product->productShipment !='' && $product->productShipment->height != '') value="{{($product->productShipment->height)/2.54}}" @endif>
                                            @if($errors->has('height'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('height')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Length <small>in
                                                    inch</small></label>
                                            <input type="text" id="length" name="length"
                                                   class="form-control {{$errors->has('length') ? 'is-invalid' : ''}}"
                                                   @if($product->productShipment !='' && $product->productShipment->length != '') value="{{($product->productShipment->length)/2.54}}" @endif>
                                            @if($errors->has('length'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('length')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Width <small>in
                                                    inch</small></label>
                                            <input type="text" id="breadth" name="breadth"
                                                   class="form-control {{$errors->has('breadth') ? 'is-invalid' : ''}}"
                                                   @if($product->productShipment !='' && $product->productShipment->breadth != '') value="{{($product->productShipment->breadth)/2.54}}" @endif>

                                            @if($errors->has('breadth'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('breadth')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <!-- shipment end -->
                        </div>

                    </div>
                    <!-- /.card-body -->
                        </div>
            </form>
            <!-- /.card -->
        </div>
    </div>

@endsection
@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                console.log('tab');
                sessionStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = sessionStorage.getItem('activeTab');
            console.log(activeTab);
            if (activeTab) {
                $('#myTab a').removeClass('active');
                $('#myTab a[href="' + activeTab + '"]').addClass('active');
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
            category = $('#category_id option:selected').text();
            console.log(category);
            if(category.includes("Mixer") == true ){
                console.log('vv');
                $('#mxr').removeAttr('hidden');
                $('#mxr').find('input,textarea,select').removeAttr('disabled');
                $('#other').attr('hidden','');
                $('#other').find('input,textarea,select').attr({'disabled':'disabled'});
            }
            else{
                $('#mxr').attr('hidden','');
                $('#mxr').find('input,textarea,select').attr({'disabled':'disabled'});
                $('#other').removeAttr('hidden');
                $('#other').find('input,textarea,select').removeAttr('disabled');
            }

            characterCount = $('#meta_description').val().length;
            $('#current_count').text(characterCount);
            titleCount = $('#title').val().length;
            $('#c_count').text(titleCount);
        });
    </script>
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        $(function () {
            // Summernote
            $('.textarea').summernote()
            $('.textarea').css('font-family', 'Cario')
        })
    </script>
    <script>
        var i = $('#spc_inc').val();
        var j = $('#img_inc').val();
        var k = $('#var_inc').val();
        var l = $("#video_inc").val();
        $("#addspc").on("click", function () {

            $("#spec_row").prepend('<div class="col-4"><div class="form-group"><input type="text" name="specification[' + i + '][]" class="form-control"/></div></div>' +
                '<div class="col-4"><div class="form-group"><input type="text" name="valu[' + i + '][]" class="form-control"/></div></div>' +
                '<div class="col-3"><div class="form-group"><input type="text" name="unit[' + i + '][]" class="form-control"/></div></div>');
            i++;

        });
        $("#addimage").on("click", function () {

            $("#image_row").prepend('<div class="col-6"><div class="form-group"><div class="input-group"><div class="custom-file">' +
                '<input type="file" class="custom-file-input" id="image_path[' + j + ']" name="image_path[' + j + '][]" onchange="showfilename(j)">' +
                '</div><label class="custom-file-label" for="image_path" id="label' + j + '">Choose file</label></div></div></div>' +
                '<div class="col-5"><div class="form-group"><input type="number" name="sort_order[' + j + '][]" class="form-control" min="0"/></div></div>');
            j++;

        });
        $("#addvideo").on("click", function () {

            $("#video_row").append('<div class="col-6"><div class="form-group">' +
                '<input placeholder="Enter Youtube Url (Full Url)" type="url" class="form-control"  name="url[' + l + '][]" id="url[' + l + ']"> ' +
                '</div></div>' +
                '<div class="col-5"><div class="form-group"><input type="number" name="v_sort_order[' + l + '][]" class="form-control" min="0"/></div></div>');
            l++;

        });
        $("#addvariant").on("click", function () {

            $("#variant_row").prepend('<div class="col-3"><div class="form-group"><input type="text" name="variant[' + k + '][]" class="form-control"/></div></div>' +
                '<div class="col-3"><div class="form-group"><input type="text" name="val[' + k + '][]" class="form-control"/></div></div>' +
                '<div class="col-3"><div class="form-group"><input type="number" name="price[' + k + '][]" class="form-control"/></div></div>' +
                '<div class="col-2"><div class="form-group"><input type="number" name="quantity[' + k + '][]" class="form-control"/></div></div>');
            k++;


        });

    </script>
    <script>
        function delete_individual(id, spec) {
            console.log(spec);
            if (confirm("Are you sure you want to delete?")) {
                if (spec == 'specification')
                    url = "/admin/delete_spec";
                else if (spec == 'image')
                    url = "/admin/delete_image";
                else if (spec == 'variant')
                    url = "/admin/delete_variant";
                else if (spec == 'video')
                    url = "/admin/delete_video";
                $.ajax({
                    url: url,
                    data: {
                        '_method': 'DELETE',
                        'id': id,
                    },
                    success: function (response) {
                        console.log(response)
                        if (response == 'success') {
                            console.log(response);
                            fireTost('Deleted Successfully');
                            setInterval('location.reload()', 1000);
                        }
                    },

                })
            }
        }

        //to tost a message
        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }

    </script>
    <script>
        function showfilename(j) {
            console.log(j - 1);
            j = j - 1;
            var fileName = document.getElementById('image_path[' + j + ']').files[0].name;
            console.log(fileName);
            //replace the "Choose a file" label
            $('#label' + j).text(fileName);

        }
    </script>
    <script>
        function change_category(){
            category = $('#category_id option:selected').text();
            console.log(category);
            if(category.includes("Mixer") == true ){
                console.log('vv');
                $('#mxr').removeAttr('hidden');
                $('#mxr').find('input,textarea,select').removeAttr('disabled');
                $('#other').attr('hidden','');
                $('#other').find('input,textarea,select').attr({'disabled':'disabled'});
            }
            else{
                $('#mxr').attr('hidden','');
                $('#mxr').find('input,textarea,select').attr({'disabled':'disabled'});
                $('#other').removeAttr('hidden');
                $('#other').find('input,textarea,select').removeAttr('disabled');
            }
            return true;
        }
    </script>
    <script>
        $('#meta_description').keyup(function() {
            console.log($(this).val().length);
            characterCount = $(this).val().length;
            $('#current_count').text(characterCount);
        });
        $('#title').keyup(function() {
            console.log($(this).val().length);
            characterCount = $(this).val().length;
            $('#c_count').text(characterCount);
        });
    </script>
@endsection

@extends('admin.layouts.datatable')
@section('title','Product Returns')
@section('tableTitle','Product Returns')
@section('evenMoreStyles')
    <style>
        #tracking-modal .modal-dialog {
            max-width: 700px;
            border-radius: 0;
        }
        #tracking-modal .modal-dialog .modal-content {
            border-radius: 0;
        }
        #tracking-modal .modal-dialog .modal-content .modal-header {
            padding: 0;
            border: 0;
        }
        #tracking-modal .modal-dialog .modal-content .modal-header button {
            padding: 5px;
            margin: 0 0 0 auto;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body h4 {
            text-transform: capitalize;
            font-weight: 600;
            font-size: 23px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body h5 {
            font-weight: 600;
            font-size: 18px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day {
            padding-top: 25px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day h6 {
            font-weight: 600;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location {
            list-style: none;
            padding: 0;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li {
            margin-bottom: 15px;
            display: flex;
            width: 100%;
            flex-wrap: wrap;
            align-items: center;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .time {
            display: inline-block;
            width: 125px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location {
            width: calc(100% - 125px);
            display: inline-block;
            position: relative;
            padding-left: 10px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location:before {
            position: absolute;
            left: 0;
            top: 50%;
            height: 100%;
            background-color: #869791;
            width: 1px;
            content: "";
            transform: translateY(-50%);
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location h6 {
            margin-bottom: 0;
            font-size: 15px;
        }
        #tracking-modal .modal-dialog .modal-content .modal-body .day ul.time-location li .location span {
            margin-bottom: 0;
            font-style: italic;
            font-size: 14px;
        }
    </style>
@endsection
@section('tableHead')
    <th>#</th>
    <th>RMA</th>
    <th>Product Detail</th>
    <th>Fault</th>
    <th>Customer Detail</th>
@endsection
@section('tableBody')
    @foreach($returns->sortByDesc('id') as $return)
        <tr>
            <td></td>
            <td>
                {{$return->return_id }}<br/>
                @if($return->status=='Rejected')
                    <span class="badge badge-danger">Rejected</span><br/>
                    <small>{{$return->reject_reason}}</small>
                @elseif($return->status=='Provisionally Approved')
                    <span class="badge badge-primary">Provisionally Approved</span><br/>
                    @if($return->product_registration->store->address=='Ultralinks Online Store')
                        <a href="" id="track{{$return->return_id}}" class="btn btn-success btn-sm"
                           onclick="return track({{$return->tracking_pin}});" type="button">Track </a>
                    @endif
                @elseif($return->status=='Replaced')
                    <span class="badge badge-success">Replaced</span><br/>
                    <small>{{$return->reject_reason}}</small>
                    @if($return->shipping_partner!='')
                        <small>Shipped via {{$return->shipping_partner}} ({{$return->shippment_number}})
                            on {{date('M d Y',strtotime($return->shipping_date))}}</small>
                    @endif
                @else
                    <button data-return-id="{{$return->return_id }}" class="approve btn btn-sm btn-primary mt-2">Approve
                        <i class="fa fa-check"></i></button>
                    <br/>
                    <button data-toggle="modal" data-target="#rejectionModal" data-return-id="{{$return->return_id }}"
                            class="reject btn btn-sm btn-danger mt-2">Reject <i
                            class="fa fa-times"></i></button>
                    <br/>
                @endif
            </td>
            <td>
                Product: {{$return->product_registration->product->name}}<br/>
                Reg Id: {{$return->reg_num}}<br/>
                Reg Date: {{date('M d, Y', strtotime($return->product_registration->created_at))}}<br/>
                Purchase Date: {{date('M d, Y', strtotime($return->product_registration->purchase_date))}}<br/>
                Purchased From: {{$return->product_registration->store->address}}<br/>
                Serial No: {{$return->product_registration->serial_number}}<br/>
                Product Code: {{$return->product_registration->product_code}}<br/>
                Safety Code: {{$return->product_registration->safety_code}}<br/>
            </td>
            <td>
                {{$return->dead}} <br/>{{$return->faulty}}<br/> Opened ? {{$return->is_open}} <br/>
                <a href="{{asset($return->image1)}}" target="_blank"><img class="mt-2" src="{{asset($return->image1)}}"
                                                                          width="100"></a>
            </td>
            <td>
                Name: {{$return->product_registration->first_name}} {{$return->product_registration->last_name}} <br/>
                Phone: {{$return->product_registration->phone}}<br/>
                Email: <a target="_blank"
                          href="mailto:{{$return->product_registration->email}}"> {{$return->product_registration->email}}</a><br/>
                Address: {{$return->product_registration->address}}, {{$return->product_registration->city}}
                , {{$return->product_registration->province}}, {{$return->product_registration->zipcode}}
            </td>
        </tr>
    @endforeach
    <!-- rejection Modal -->
    <div class="modal fade" id="rejectionModal" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal" data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Rejection Reason</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('discount.reject')}}" method="post" name="discount" id="approvalForm">
                            @csrf
                            <div class="row">
                                <input type="hidden" name="return_id" id="reject_id">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input name="reason" id="reason" class="form-control" required
                                               placeholder="Give the exact reason ">
                                        <span class="text-danger">
                                        <small><strong id="reason-error"></strong></small>
                                </span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-danger" onclick="reasonSubmit();">Reject</button>
                                </div>
                                <div class="col-sm-6" style="display:none;" id="rej-response"><i
                                        class="fa fa-cog fa-spin"></i>Please
                                    Wait
                                </div>
                            </div>
                            <div class="actions">
                                <ul class="list-inline pt-2">

                                    <li style="display:none;" id="rej-response"><i class="fa fa-cog fa-spin"></i>Please
                                        Wait
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- tracking modal -->
    <div class="modal" id="tracking-modal" tabindex="-1" role="dialog"
         aria-labelledby="tracking-modal" data-backdrop="false"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!--                <h5 class="modal-title">Add Addresses</h5>-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <h4>shipped with Canada Post</h4>
                        <h5>Tracking ID: <span id="track_id"></span></h5>
                        <div class="day">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('evenMoreScripts')
    <script>
        $(".approve").click(function () {
            var return_id = $(this).data('return-id')
            //show please wait
            $(this).html('<i class="fa fa-cog fa-spin"></i> Please wait');
            //$('#rej-response').css('display', 'block');
            url = '{{route('return.approve')}}';
            $.ajax({
                type: "POST",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}', _method: "post", return_id: return_id},

                success: function (result) {
                    console.log(result)
                    if (result == 'success') {
                        $('#rejectionModal').modal('hide');
                        fireTost('Return request provisionally approved');
                        setInterval('location.reload()', 2000);
                    }
                    else if (result == 'dimension') {
                        $('#rejectionModal').modal('hide');
                        alert('Please update product weight and dimension before approving the request');
                        setInterval('location.reload()', 2000);

                    }
                    else{
                        $('.approve').html('Approve <i class="fa fa-check"></i> ');
                        alert('Failed to approve request');

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $('.approve').html('Approve <i class="fa fa-check"></i> ');
                    alert("Failed to approve request");
                    //alert(thrownError);
                }
            });
        })



        $('#rejectionModal').on("show.bs.modal", function (e) {

            $("#reject_id").val($(e.relatedTarget).data('return-id'));
            console.log($(e.relatedTarget).data('return-id'))
        });


        function reasonSubmit() {
            return_id = $('#reject_id').val();
            reason = $('#reason').val();
            if (!reason) $('#reason-error').html('Please give the reason');
            else {
                $('#rej-response').css('display', 'block');
                url = '{{route('return.reject')}}';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}', _method: "post", return_id: return_id, reason: reason},
                    success: function (result) {
                        console.log(result)
                        if (result == 'success') {
                            $('#rejectionModal').modal('hide');
                            fireTost('Return Request rejected');
                            setInterval('location.reload()', 2000);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to reject request");
                        $('#response').css('display', 'none');
                    }
                });
            }

        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
    </script>
    <script>
        function track(pin) {
            $("#track_id").text(pin);
            url = '/admin/order/tracking/'+pin
            $.ajax({
                type: "GET",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"get",id:pin},
                success: function (result) {
                    if(result.msg == 'success')
                    {
                        if(!result.datas[0])
                            $('.day').html('<p> Tracking History Not Available</p>');
                        else {
                            $('.day').html('');
                            for (i = 0; i < result.datas.length; i++) {
                                c_date =  result.datas[i]["date"][0];
                                d_date = c_date.split('-');
                                new_date = new Date(d_date[0],d_date[1]-1,d_date[2]);
                                $('.day').append('<h6>' +new_date.toDateString() + '</h6>' +
                                    '<ul class="time-location">' +
                                    '<li>' +
                                    '<div class="time">' + result.datas[i]["time"][0] + '</div>' +
                                    '<div class="location">' +
                                    '<h6>' + result.datas[i]["description"][0] + '</h6>' +
                                    '<span>' + result.datas[i]["site"][0] + '</span>' +
                                    '</div>' +
                                    '</li>' +
                                    '</ul>');
                            }
                        }
                        $('#tracking-modal').modal('show');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Failed to get tracking details");
                }
            });
            return false;
        }
    </script>
@endsection

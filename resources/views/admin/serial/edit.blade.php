@extends('admin.layouts.form')
@section('title','Edit Serial Number')
@section('actionUrl')
    {{route('serial.update',$product)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Update')
@section('indexRoute')
    {{route('serial.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="product_id" class="control-label">Product Name</label>
            <select name="product_id" class="form-control" required readonly="readonly">
                    <option value="{{$product->product_id}}">{{$product->product->name}}</option>
            </select>
            @if($errors->has('product_id'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('product_id')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="serial_number" class="control-label">Serial Number</label>
            <input type="text" id="serial_number" name="serial_number" required class="form-control {{$errors->has('serial_number') ? 'is-invalid' : ''}}" value="{{$product->serial_number}}">
            @if($errors->has('serial_number'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('serial_number')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="safety_code" class="control-label">Safety Code</label>
            <input type="text" id="safety_code" name="safety_code" required class="form-control {{$errors->has('safety_code') ? 'is-invalid' : ''}}" value="{{$product->safety_code}}">
            @if($errors->has('safety_code'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('safety_code')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="product_code" class="control-label">Product Code</label>
            <input type="text" id="product_code" name="product_code" required class="form-control {{$errors->has('product_code') ? 'is-invalid' : ''}}" value="{{$product->product_code}}">
            @if($errors->has('product_code'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('product_code')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection


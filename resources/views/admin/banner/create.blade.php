@extends('admin.layouts.form')
@section('title','Add/Edit Product Banner')
@section('actionUrl')
    {{route('banner.store')}}
@endsection
<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add')
@section('indexRoute')
    {{route('banner.index')}}
@endsection
@section('formBody')

    <div class="col-6">
        <div class="form-group">
            <label for="image">Banner Image <small>(Not less than 1280X319 px , Maximum 500Kb)</small></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('image') ? 'is-invalid' : ''}}" id="image" name="image" value="{{old('image')}}" required>
                    <label class="custom-file-label" for="image">Choose file</label>
                </div>
            </div>
            @if($errors->has('image'))
                <span class="help-block error" style="color:#c9352a">
                    <strong> {{$errors->first('image')}}
                    </strong>
                </span>
            @endif
        </div>
    </div>
@endsection

@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection
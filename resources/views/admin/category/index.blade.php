@extends('admin.layouts.datatable')
@section('title','Product Categories')
@section('tableTitle','Product Categories')
@section('createRoute')
    {{route('category.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Icon</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($categories->sortByDesc('id') as $category)
        <tr>
            <td></td>
            <td>{{$category->name}}</td>
            <td><img src="{{asset($category->icon)}}" width="100px" class="img-thumbnail"></td>
            <td>
                <form action="{{route('category.destroy',$category->id)}}" method="POST" id="delete-form-{{$category->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$category->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('category.edit',$category->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection

@extends('admin.layouts.form')
@section('title','Edit Flat Shipping Rate')
@section('actionUrl')
    {{route('shippingdiscount.update',$code)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Update')
@section('indexRoute')
    {{route('shippingdiscount.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="code" class="control-label">Code</label>
            <input type="text" id="code" name="code" required class="form-control {{$errors->has('code') ? 'is-invalid' : ''}}" value="{{$code->code}}" readonly>
            @if($errors->has('code'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('code')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="expires_on" class="control-label">Expires On</label>
            <input type="date" id="expires_on" name="expires_on" required class="form-control {{$errors->has('expires_on') ? 'is-invalid' : ''}}" value="{{date('Y-m-d', strtotime($code->expires_on))}}">
            @if($errors->has('expires_on'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('expires_on')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="applicable_to" class="control-label">Applicable To</label>
            <select id="applicable_to" name="applicable_to" required class="form-control {{$errors->has('applicable_to') ? 'is-invalid' : ''}}">
                <option value="">Select</option>
                <option value="All" {{$code->applicable_to == 'All' ? 'selected' : ''}}>All</option>
                <option value="New Customers" {{$code->applicable_to == 'New Customers' ? 'selected' : ''}}>New Customers</option>
                <option value="Existing Customers" {{$code->applicable_to == 'Existing Customers' ? 'selected' : ''}}>Existing Customers</option>
            </select>
            @if($errors->has('applicable_to'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('applicable_to')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="value" class="control-label">Flat Shipping Rate</label>
            <input type="number" id="value" name="value" required class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}" value="{{$code->value}}">
            @if($errors->has('value'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('value')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="category" class="control-label">Product Category</label>
            <select name="category" class="form-control" id="category">
                <option value="">Select</option>
                <option value="All" {{$code->category == 'All' ? 'selected' :''}}>All</option>
                @foreach($categories->sortBy('name') as $category)
                    <option value="{{$category->name}}" {{$category->name == $code->category ? 'selected' : '' }}>
                        {{$category->name}}</option>
                @endforeach
            </select>
            @if($errors->has('name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="applicable_product" class="control-label">Applicable Product <small> (If discount applicable to a specific product)</small></label>
            <select name="applicable_product" class="select2bs4 form-control" id="product">
                <option value="">Select</option>
            @foreach($products->sortBy('name') as $product)
                    <option value="{{$product->id}}" {{$product->id == $code->applicable_product ? 'selected' : '' }}>
                        {{$product->name}}</option>
                @endforeach
            </select>
            @if($errors->has('applicable_product'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('applicable_product')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="status" class="control-label">Status</label>
            <select id="status" name="status" required class="form-control {{$errors->has('status') ? 'is-invalid' : ''}}">
                <option value="">Select</option>
                <option value="Published" {{$code->status == 'Published' ? 'selected' : ''}}>Published</option>
                <option value="Hidden" {{$code->status == 'Hidden' ? 'selected' : ''}}>Hidden</option>
            </select>
            @if($errors->has('status'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('status')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        $(function () {
                //Initialize Select2 Elements
                $('.select2').select2();

                //Initialize Select2 Elements
                $('.select2bs4').select2({
                    theme: 'bootstrap4'
                })
            });
        $('#product').on('change',function(){
            console.log('pp');
            if($(this).val() != '') {
                $('#category').attr({'disabled': 'disabled'});
                $('#category').val('');
            }
            else{
                console.log('kkk');
                $('#category').removeAttr('disabled');

            }

        });
        $(document).ready(function (){
            if($('#product').val() != ''){
                $('#category').attr({'disabled': 'disabled'});
                $('#category').val('');
            }

        });
    </script>
@endsection

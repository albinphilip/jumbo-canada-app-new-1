@extends('admin.layouts.form')
@section('title','Add new Product brand')
@section('actionUrl')
    {{route('brand.store')}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add Brand')
@section('indexRoute')
    {{route('brand.index')}}
@endsection
@section('formBody')
    <div class="col-6">
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <input type="text" id="name" name="name" required class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{old('name')}}">
            @if($errors->has('name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="banner_image">Banner Image <small>(Not less than 1280X319 px , Maximum 500Kb)</small></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('banner_image') ? 'is-invalid' : ''}}" id="banner_image" name="banner_image" value="{{old('banner_image')}}">
                    <label class="custom-file-label" for="banner_image">Choose file</label>
                </div>
            </div>
            @if($errors->has('banner_image'))
                <span class="help-block error" style="color:#c9352a"> 
                    <strong>{{$errors->first('banner_image')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
